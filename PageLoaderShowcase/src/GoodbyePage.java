import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
/*
 * Created By Joseph O'Leary
 * 25/1/2017
 * 
 */

public class GoodbyePage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;

	// Standard Swing code here
	private JButton showHelloPageButton;
	private JButton showWelcomePageButton;

	public GoodbyePage(PageLoader parent) {
		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);

		// Standard Swing GUI code here
		this.setLayout(new BorderLayout());

		showHelloPageButton = new JButton("Show Hello Page");
		showHelloPageButton.addActionListener(this);
		showWelcomePageButton = new JButton("Show Welcome Page");
		showWelcomePageButton.addActionListener(this);

		JPanel buttonsPanel = new JPanel(new GridLayout(1, 2));
		buttonsPanel.add(showWelcomePageButton);
		buttonsPanel.add(showHelloPageButton);

		this.add(new JLabel("I AM THE GOODBYE PAGE"), BorderLayout.PAGE_START);
		this.add(buttonsPanel, BorderLayout.PAGE_END);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if (target == showHelloPageButton) {
			PARENT.showPage(PARENT.HELLO_PAGE);
		} else if (target == showWelcomePageButton) {
			PARENT.showPage(PARENT.WELCOME_PAGE);

		}
	}

}
