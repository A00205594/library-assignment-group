import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
/*
 * Created By Joseph O'Leary
 * 25/1/2017
 * 
 */
public class PageLoader extends JFrame{

	private static final long serialVersionUID = 1L;
	
	private JPanel mainPanel;
	private CardLayout pageSwapper;
	
	private JPanel helloPage;
	private JPanel goodbyePage;
	private JPanel welcomePage;
	
	public final String HELLO_PAGE = "A";
	public final String GOODBYE_PAGE = "B";
	public final String WELCOME_PAGE ="C";
	public final Color DEFAULT_BACKGROUND_COLOUR = Color.LIGHT_GRAY;
	
	public PageLoader() {
		pageSwapper = new CardLayout();
		mainPanel = new JPanel(pageSwapper);
		
		helloPage = new HelloPage(this);
		goodbyePage = new GoodbyePage(this);
		welcomePage = new WelcomePage(this);
		
		mainPanel.add(helloPage, HELLO_PAGE);
		mainPanel.add(goodbyePage, GOODBYE_PAGE);
		mainPanel.add(welcomePage, WELCOME_PAGE);

		
		// Going to start with the Welcome Page
		showPage(WELCOME_PAGE);
		
		this.setContentPane(mainPanel);
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public void resizeWindow(int width, int height){
		this.setSize(new Dimension(width,height));
	}
	public void showPage(String key){
		pageSwapper.show(mainPanel,key);
	}


	public static void main(String[] args) {
		new PageLoader();
	}
}
