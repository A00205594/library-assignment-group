//done by: Oliver
package data;

import java.awt.Container;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Lib17_V1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame frame = new JFrame();
		Container cont = frame.getContentPane();
		
		JPanel panel = new JPanel();
		JLabel studnameLB = new JLabel("Student name:");
		JLabel studidLB = new JLabel("Student ID:");
		JLabel timedueLB = new JLabel("Time overdue:");
		JLabel amntdueLB = new JLabel("Amount overdue:");
		JLabel emailrepLB = new JLabel("Email recipient:");
		JLabel JournaltitleLB = new JLabel("Journal Title:");
		
		JTextField studnameTF = new JTextField(5);
		JTextField studidTF = new JTextField(5);
		JTextField timedueTF = new JTextField(5);
		JTextField amntdueTF = new JTextField(5);
		JTextField emailrepTF = new JTextField(5);
		JTextField JournaltitleTF = new JTextField(5);
		
		JTextArea textarea = new JTextArea(10,10);
		textarea.setBorder(BorderFactory.createTitledBorder("Message"));
		
		JButton sendbutton = new JButton("Send");
		
		panel.add(studnameLB);
		panel.add(studnameTF);
		panel.add(studidLB);
		panel.add(studidTF);
		panel.add(timedueLB);
		panel.add(timedueTF);
		panel.add(amntdueLB);
		panel.add(amntdueTF);
		panel.add(emailrepLB);
		panel.add(emailrepTF);
		panel.add(JournaltitleLB);
		panel.add(JournaltitleTF);
		panel.add(textarea);
		panel.add(sendbutton);
		
		
		cont.add(panel);
		
		frame.setSize(340,400);
		frame.setVisible(true);
		
		

	}

}
