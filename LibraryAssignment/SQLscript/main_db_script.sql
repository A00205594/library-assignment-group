DROP DATABASE IF EXISTS AgileDatabase;
CREATE DATABASE IF NOT EXISTS AgileDatabase;
USE AgileDatabase;

##########################################################################################################

CREATE TABLE Book(
	book_id INTEGER AUTO_INCREMENT ,
	title VARCHAR(50) NOT NULL,
	ISBN VARCHAR(20) NOT NULL,
    Author VARCHAR(20) NOT NULL,
    Genre VARCHAR(20) NOT NULL,
    Year_Published INTEGER,
  	Reference_Number VARCHAR(6) NOT NULL,
	Stock_Number INTEGER,
    Description VARCHAR(250) NOT NULL,
	Publisher VARCHAR(50) NOT NULL,

	PRIMARY KEY(book_id));

INSERT INTO Book VALUES ( null, 'A Sword of Night & Day', '9781212345015','David Gemmell','Heroic Fantasy','2007','E23456','5','Skilganon the famous swordmen is recarnated','Gotham Press');
INSERT INTO Book VALUES ( null, 'White Wolf', '9221132345314','David Eddings','Fiction','2001','E54321','6','The Mythical kingdoms are under siege from many foes','Bantam Press');
INSERT INTO Book VALUES ( null, 'Feast of Crows', '1234562345015','George RR Martin','Fantasy Fiction','2005','E23123','8','The Nights Watch on the wall are under attack from widlings','Appletree Press');
INSERT INTO Book VALUES ( null, 'The Crippled God', '1221132398765','Steven Ericsson','Fantasy Fiction','2001','E54322','6','Will the crippled god bring about the end of warren magic','The Books Press');

SELECT * FROM Book;


##################################################################################################################

CREATE TABLE Journal(
	journal_id INTEGER AUTO_INCREMENT ,
	title VARCHAR(50) NOT NULL,
	ISSN VARCHAR(20) NOT NULL,
    Author VARCHAR(20) NOT NULL,
    Genre VARCHAR(20) NOT NULL,
    Year_Published INTEGER,
    Reference_Number VARCHAR(6) NOT NULL,
	Stock_Number INTEGER,
	Description VARCHAR(250) NOT NULL,
	Publisher VARCHAR(50) NOT NULL,


	PRIMARY KEY(journal_id));

INSERT INTO Journal VALUES ( null, 'A Day and a Night', '978X1234','Patrick McLoughlin','Crime','2013', 'E98765','4','Set over a day and night will the Hercules succed in saving over the world','Blackstaff Press');
INSERT INTO Journal VALUES ( null, 'A Night and a Day', '17YX1233','Peter Quinn','Fantasy','2012','E56789','3','After his failure in the previous book will Hercules find a new world to live in','Bantam Press');
INSERT INTO Journal VALUES ( null, 'A Evening and a Afternoon', '66XY6612','Jeff Blake','Romance','2011', 'E43218','9','Hercules needs to stop fighting but his enemies wont leave him alone','Appletree Press');
INSERT INTO Journal VALUES ( null, 'A Afternoon and a Evening', '29YH1233','Ana Power','Horror','2017','E81781','5','Hercules has taking up knitting.  This book is a step by step book on how to knit a cardigan','Gotham Press');

SELECT * FROM Journal;

##################################################################################################################

DROP TABLE IF EXISTS Fines;
    
  CREATE TABLE Fines (
  id INTEGER AUTO_INCREMENT,
  Email varchar(40) NOT NULL,
  Book varchar(35) NOT NULL,
  Journal varchar(35) NOT NULL,
  IssueDate date NOT NULL,
  ReturnDate date NOT NULL,
  LateFines double(9,2) NOT NULL,
  LateFineDate date NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO Fines VALUES(null,'A0012345@student.ait.ie','The Codex','null','2017-5-1','2017-5-5','0','2017-5-6');
INSERT INTO Fines VALUES(null,'A0012348@student.ait.ie','null','The Codex','2017-1-5','2017-1-2','0','2017-1-6');




select * from Fines;

update Fines set LateFines = (LateFines + 5.5) , LateFineDate = CURDATE()  where ReturnDate < CURDATE();

####################################################################################################################

DROP TABLE IF EXISTS finalReturnNotif;

create table finalReturnNotif(
finalReturnNotif_ID  INTEGER AUTO_INCREMENT PRIMARY KEY,
FirstName varchar(40) NOT NULL,
LastName varchar(40) NOT NULL,
Email varchar(20) NOT NULL,
BookID INT,
BookTitle varchar(40),
JournalID INT,
JournalTitle varchar(40),
TimeOverDue INT NOT NULL,
AmountDue float NOT NULL

);

INSERT INTO finalReturnNotif VALUES(null,'Luke','Evans','A00111111@ait.ie',1,'Catcher in the rye',null,null,40,12.20);
INSERT INTO finalReturnNotif VALUES(null,'John','Thomas','A00111112@ait.ie',null,null,7,'My Journal',30,9.20);

select * from finalReturnNotif;

######################################################################################################################
DROP TABLE IF EXISTS users;

CREATE TABLE users(
	first_name VARCHAR(25) NOT NULL,
    Last_Name VARCHAR(30) NOT NULL,
    email_Address VARCHAR(50) NOT NULL,
    Phone_Num INTEGER NOT NULL,
    Student_num VARCHAR(9),
    Pass VARCHAR(40),
    user_Type VARCHAR(10)
);

select * from users;
#############################################################################################################################

DROP TABLE IF EXISTS Requests;
CREATE TABLE Requests(
	Priority_Id INTEGER auto_increment PRIMARY KEY,
	Library_id INTEGER NOT Null ,
	title VARCHAR(50) NOT NULL,
	ISBN VARCHAR(20) NOT NULL,
    Author VARCHAR(20) NOT NULL,
    Genre VARCHAR(20) NOT NULL,
    Year_Published INTEGER,
  	Reference_Number VARCHAR(6) NOT NULL,
    Description VARCHAR(250) NOT NULL,
	Publisher VARCHAR(50) NOT NULL,
    RequestType VARCHAR(50) NOT NULL,
    StudentEmail Varchar(50) NOT NULL);

INSERT INTO Requests VALUES ( null, 1, 'A Sword of Night & Day', '9781212345015','David Gemmell','Heroic Fantasy','2007','E23456','Hello Description','Gotham Press','Book','@mail');
INSERT INTO Requests VALUES (null,  3, 'White Wolf', '9221132345314','David Eddings','Fiction','2001','E54321','Hello Description','Gotham Press','Book','@mail');
INSERT INTO Requests VALUES ( null, 2, 'Feast of Crows', '1234562345015','George RR Martin','Fantasy Fiction','2005','E23123','Hello Description','Gotham Press','Book','@mail');
INSERT INTO Requests VALUES ( null, 4, 'The Crippled God', '1221132398765','Steven Ericsson','Fantasy Fiction','2001','E54322','Hello Description','Gotham Press','Book','@mail');

SELECT * FROM Requests;


##################################################################################################################