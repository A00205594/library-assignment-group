DROP DATABASE AgileLib2;
CREATE DATABASE IF NOT EXISTS AgileLib2;
USE AgileLib2;




DROP TABLE IF EXISTS AgileLib;
   
  CREATE TABLE AgileLib (
  Art_id INTEGER AUTO_INCREMENT,
  title varchar(40) NOT NULL,
  publisher varchar(40) NOT NULL,
  category varchar(35) NOT NULL,
  ISSN int(13) NOT NULL,
  reference_number varchar (35) NOT NULL,
  number_of_journals  int(20) NOT NULL,
  PRIMARY KEY(Art_id)
);

INSERT INTO AgileLib VALUES(null,'New Journal of Physics','John Reed','science','9771144875007','E855A3648','4');




select * from AgileLib;

DROP TABLE IF EXISTS AgileLibBooks;
   
  CREATE TABLE AgileLibBooks (
  Art_id INTEGER AUTO_INCREMENT,
  title varchar(40) NOT NULL,
  publisher varchar(40) NOT NULL,
  category varchar(35) NOT NULL,
  ISSN int(16) NOT NULL,
  reference_number varchar (35) NOT NULL,
  number_of_journals  int(20) NOT NULL,
  PRIMARY KEY(Art_id)
);

INSERT INTO AgileLibBooks VALUES(null,'Prince of Thorns','Mark Lawrence','Fantasy','9771144875007','E855A3648','4');




select * from AgileLibBooks;


DROP TABLE IF EXISTS AgileLibBooks;
   
  CREATE TABLE AgileLibBooks (
  Art_id INTEGER AUTO_INCREMENT,
  title varchar(40) NOT NULL,
  publisher varchar(40) NOT NULL,
  category varchar(35) NOT NULL,
  ISSN int(13) NOT NULL,
  reference_number varchar (35) NOT NULL,
  number_of_journals  int(20) NOT NULL,
  PRIMARY KEY(Art_id)
);

INSERT INTO AgileLibBooks VALUES(null,'Prince of Thorns','Mark Lawrence','Fantasy','9771144875007','E855A3648','4');




select * from AgileLibBooks;

 
DROP TABLE IF EXISTS Fines;
   
  CREATE TABLE Fines (
  id INTEGER AUTO_INCREMENT,
  FirstName varchar(40) NOT NULL,
  LastName varchar(40) NOT NULL,
  Book varchar(35) NOT NULL,
  Journal varchar(35) NOT NULL,
  IssueDate date NOT NULL,
  ReturnDate date NOT NULL,
  LateFines float(35) NOT NULL,
  LateFineDate date NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO Fines VALUES(null,'John','Rogers','The Codex','null','2017-5-1','2017-5-5','0','2017-5-6');
INSERT INTO Fines VALUES(null,'John','Rogers','null','The Codex','2017-1-5','2017-5-5','0','2017-1-6');




select * from Fines;




 update Fines set LateFines = (LateFines + 5.50) where (ReturnDate < CURDATE());

 
create table finalReturnNotif(
finalReturnNotif_ID NOT NULL AUTO_INCREMENT PRIMARY KEY,
FirstName varchar(40) NOT NULL, 
LastName varchar(40) NOT NULL,
Email varchar(20) NOT NULL,
BookID INT,
BookTitle varchar(40),
JournalID INT, 
JournalTitle varchar(40),
TimeOverDue INT NOT NULL,
AmountDue float NOT NULL

);

INSERT INTO finalReturnNotif VALUES(null,'Luke','Evans','A00111111@ait.ie',1,'Catcher in the rye',null,null,40,12.20);
INSERT INTO finalReturnNotif VALUES(null,'John','Thomas','A00111112@ait.ie',null,null,7,'My Journal',30,9.20);

select * from finalReturnNotif







