DROP TABLE IF EXISTS users;

CREATE TABLE users(
	first_name VARCHAR(25) NOT NULL,
    Last_Name VARCHAR(30) NOT NULL,
    email_Address VARCHAR(50) NOT NULL,
    Phone_Num INTEGER NOT NULL,
    Pass VARCHAR(40),
    user_Type VARCHAR(10)
);

select * from users;