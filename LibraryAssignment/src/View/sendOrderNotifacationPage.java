package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import DatabaseAccess.BookDAO;
import DatabaseAccess.DbTable;
import DatabaseAccess.FinalReturnNotifyDAO;
import DatabaseAccess.JournalDAO;
import DatabaseAccess.UserDAO;
import DatabaseAccess.Utils;
import Model.Book;
import Model.Journal;
import Model.User;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;
import Utils.MailSender;

public class sendOrderNotifacationPage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;
	
	JButton sendbutton = new JButton("SEND");
	JButton homebutton = new JButton("HOME");
	
	
	JTextField toTextField = new JTextField(20);
	JLabel LabelTo = new JLabel("Recipient: ");
	JTextField SubjectTextField = new JTextField(20);
	JLabel LabelSubject = new JLabel("Subject: ");
	JTextField MessageTextField = new JTextField(20);
	JLabel LabelMessage = new JLabel("Message: ");
	
	public sendOrderNotifacationPage(PageLoader parent){
		this.PARENT=parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());
		
		JPanel notifpanel = new JPanel();
		notifpanel.setLayout(new FlowLayout());
		notifpanel.setBorder(BorderFactory.createTitledBorder("Send Notification for Order"));
		notifpanel.add(LabelTo);
		notifpanel.add(toTextField);
		notifpanel.add(LabelSubject);
		notifpanel.add(SubjectTextField);
		notifpanel.add(LabelMessage);
		notifpanel.add(MessageTextField);
		
		this.add(notifpanel);
		
		
		sendbutton.addActionListener(this);
		homebutton.addActionListener(this);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if(target==sendbutton){
			String to = toTextField.getText();
			String subject = SubjectTextField.getText();
			String message = MessageTextField.getText();
			MailSender.instance.send(to, subject, message);
		}
		if(target==homebutton){
			//return to home page.
			PARENT.showPage(PARENT.HOME_PAGE);
		}
		
	}
	
	} 
	
	


