package View;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import DatabaseAccess.UserDAO;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;


public class UserUpdateDetails extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;
	
	static JTextField FirstNmField = new JTextField();
	static JTextField LastNmField = new JTextField();
	static JTextField PhNoField = new JTextField();
	static JTextField StNumField = new JTextField();
	static JTextField emailField = new JTextField();
	
	JPasswordField passField = new JPasswordField();
	
	JLabel firstName = new JLabel("First Name: ");
	JLabel lastName = new JLabel("Last Name: ");
	JLabel phone = new JLabel("Phone Number: ");
	JLabel StudentNum = new JLabel("Student Number: ");
	JLabel email = new JLabel("Email: ");

	JLabel Password = new JLabel("Enter your password: "); 
	JLabel Password2 = new JLabel("Re-enter your password: ");
	
	JButton updateDet = new JButton("Update");
	JButton changeEmail = new JButton("Change Email/Password");
	JButton updateEmail = new JButton("Update Email");
	JButton back = new JButton("Return Home");
	JPanel other = new JPanel();
	public UserUpdateDetails(PageLoader parent){
		this.PARENT = parent;
		setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		
		JPanel UpdateDetails = new JPanel();
		UpdateDetails.setLayout(new FlowLayout());
		UpdateDetails.setBorder(BorderFactory.createTitledBorder("Update Basic Info"));
		
		JPanel update = new JPanel();
		update.setLayout(new GridLayout(5,2));
		
		update.add(firstName);update.add(FirstNmField);
		update.add(lastName);update.add(LastNmField);
		update.add(phone);update.add(PhNoField);
		update.add(StudentNum);update.add(StNumField);
		update.add(updateDet);update.add(changeEmail);
		
		other.setLayout(new GridLayout(3,2));
		other.add(email);other.add(emailField);
		other.add(Password);other.add(passField);
		other.add(updateEmail);other.add(back);
		
		updateEmail.addActionListener(this);
		back.addActionListener(this);
		updateDet.addActionListener(this);
		changeEmail.addActionListener(this);
		
		other.setVisible(false);
		UpdateDetails.add(update);
		UpdateDetails.add(other);
		this.add(UpdateDetails);
	}

	public void actionPerformed(ActionEvent e){
		Object target = e.getSource();
		if(target==this.updateDet){
			String fname = FirstNmField.getText();
			String lastname = LastNmField.getText();
			String phone = PhNoField.getText();
			String student = StNumField.getText();
			try {
				UserDAO.instance.UpdateCredentials(fname, lastname,phone,student);
			} catch (OliverExceptionHandler e1) {
				e1.printStackTrace();
			}
		}
		if(target==this.changeEmail){
			other.setVisible(true);
		}
		if(target==this.updateEmail){
			String email = emailField.getText();
			String password = passField.getText();
			try {
				UserDAO.instance.UpdateCredentialsEmailPass("Bob1@ait.ie","password");
			} catch (OliverExceptionHandler e1) {
				e1.printStackTrace();
			}
		}
		if(target==this.back){
			PARENT.showPage(PARENT.HOME_PAGE);
		}
	}
}
