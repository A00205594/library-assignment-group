package View;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import DatabaseAccess.UserDAO;
import Model.User;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;
import Utils.MailSender;
/*
 * GUI Built By Fergal McKiernan
 * 16/2/2017
 * 
 */
public class RegisterPage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;

	public boolean res = false;
	
	JPanel fields = new JPanel();
	
	ButtonGroup bg = new ButtonGroup();
	
	JTextField emailField = new JTextField();
	JTextField FirstNmField = new JTextField();
	JTextField LastNmField = new JTextField();
	JTextField PhNoField = new JTextField();
	JTextField StNumField = new JTextField();
	JPasswordField passField = new JPasswordField();
	JPasswordField passCheckField = new JPasswordField();
	
	JLabel email = new JLabel("Email: ");
	JLabel firstName = new JLabel("First Name: ");
	JLabel lastName = new JLabel("Last Name: ");
	JLabel phone = new JLabel("Phone Number: ");
	JLabel StudentNum = new JLabel("Student Number: ");
	JLabel Password = new JLabel("Enter your password: "); 
	JLabel Password2 = new JLabel("Re-enter your password: "); 
	JRadioButton admin = new JRadioButton("Admin user");
	JRadioButton student = new JRadioButton("Student user");
	
	JLabel errorEmail = new JLabel("*this field is required*");
	JLabel errorfirst = new JLabel("*this field is required*");
	JLabel errorlast = new JLabel("*this field is required*");
	JLabel errorphone = new JLabel("*this field is required*");
	JLabel errorstudent = new JLabel("*this field is required*");
	JLabel errorPass = new JLabel("*this field is required*");
	JLabel errorPass2 = new JLabel("*Must be the same first Entry*");
	
	JButton create = new JButton("Create Account");
	JButton clear = new JButton("Clear");
	
	public RegisterPage(PageLoader parent) {
		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());
		
		
		fields.setLayout(new GridLayout(8,3));
		fields.setBorder(BorderFactory.createTitledBorder("New User"));
		
		JPanel radioP = new JPanel();
		radioP.setLayout(new GridLayout(1,2));
		radioP.add(admin);
		radioP.add(student);
		
		bg.add(admin);
		bg.add(student);
		
		errorEmail.setVisible(false);
		errorfirst.setVisible(false);
		errorlast.setVisible(false);
		errorphone.setVisible(false);
		errorPass.setVisible(false);
		errorPass2.setVisible(false);
		errorstudent.setVisible(false);
		
		errorEmail.setForeground(Color.RED);
		errorfirst.setForeground(Color.RED);
		errorlast.setForeground(Color.RED);
		errorphone.setForeground(Color.RED);
		errorstudent.setForeground(Color.RED);
		errorPass.setForeground(Color.RED);
		errorPass2.setForeground(Color.RED);
		
		fields.add(errorEmail);fields.add(email);fields.add(emailField);
		fields.add(errorfirst);fields.add(firstName);fields.add(FirstNmField);
		fields.add(errorlast);fields.add(lastName);fields.add(LastNmField);
		fields.add(errorphone);fields.add(phone);fields.add(PhNoField);
		fields.add(errorstudent);fields.add(StudentNum);fields.add(StNumField);
		fields.add(errorPass);fields.add(Password);fields.add(passField);
		fields.add(errorPass2);fields.add(Password2);fields.add(passCheckField);
		fields.add(radioP);fields.add(create);fields.add(clear);
		
		clear.addActionListener(this);
		create.addActionListener(this);
		admin.addActionListener(this);
		student.addActionListener(this);
		
		this.add(fields);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if(target==this.clear){
			emailField.setText("");
			FirstNmField.setText("");
			LastNmField.setText("");
			PhNoField.setText("");
			StNumField.setText("");
			passField.setText("");
			passCheckField.setText("");
			
		}
			if(target==this.create){
				String first = FirstNmField.getText();
				String last = LastNmField.getText();
				String email = emailField.getText();
				String Phone = PhNoField.getText();
				String StudentNum = StNumField.getText();
				String pass = passField.getText();
				String passCheck = passCheckField.getText();
				
				if(first.equals("")&&last.equals("")&&email.equals("")&&Phone.equals("")&&StudentNum.equals("")&&pass.equals("")&&passCheck!=pass){
					errorEmail.setVisible(true);
					errorfirst.setVisible(true);
					errorlast.setVisible(true);
					errorphone.setVisible(true);
					errorstudent.setVisible(false);
					errorPass.setVisible(true);
					errorPass2.setVisible(true);
				}
				else if(first.equals("")&&last.equals("")&&email.equals("")&&Phone.equals("")&&StudentNum.equals("")&&pass.equals("")){
					errorEmail.setVisible(true);
					errorfirst.setVisible(true);
					errorlast.setVisible(true);
					errorphone.setVisible(true);
					errorstudent.setVisible(false);
					errorPass.setVisible(true);
					errorPass2.setVisible(false);
				}
				else if(first.equals("")&&last.equals("")&&email.equals("")&&Phone.equals("")&&StudentNum.equals("")){
					errorEmail.setVisible(true);
					errorfirst.setVisible(true);
					errorlast.setVisible(true);
					errorphone.setVisible(true);
					errorstudent.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(false);
				}
				else if(first.equals("")&&last.equals("")&&email.equals("")&&StudentNum.equals("")){
					errorEmail.setVisible(true);
					errorfirst.setVisible(true);
					errorlast.setVisible(true);
					errorstudent.setVisible(false);
					errorphone.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(false);
				}
				else if(first.equals("")&&last.equals("")&&StudentNum.equals("")){
					errorEmail.setVisible(false);
					errorfirst.setVisible(true);
					errorlast.setVisible(true);
					errorstudent.setVisible(false);
					errorphone.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(false);
				}
				else if(first.equals("")&&StudentNum.equals("")){
					errorEmail.setVisible(false);
					errorfirst.setVisible(true);
					errorstudent.setVisible(false);
					errorlast.setVisible(false);
					errorphone.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(false);
				}
				else if(last.equals("")){
					errorEmail.setVisible(false);
					errorfirst.setVisible(false);
					errorlast.setVisible(true);
					errorstudent.setVisible(false);
					errorphone.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(false);
				}
				else if(email.equals("")){
					errorEmail.setVisible(true);
					errorfirst.setVisible(false);
					errorlast.setVisible(false);
					errorphone.setVisible(false);
					errorstudent.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(false);
				}
				else if(Phone.equals("")){
					errorEmail.setVisible(false);
					errorfirst.setVisible(false);
					errorlast.setVisible(false);
					errorphone.setVisible(true);
					errorstudent.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(false);
				}
				else if(pass.equals("")){
					errorEmail.setVisible(false);
					errorfirst.setVisible(false);
					errorlast.setVisible(false);
					errorphone.setVisible(false);
					errorstudent.setVisible(false);
					errorPass.setVisible(true);
					errorPass2.setVisible(false);
				}
				else if(!(passCheck.equals(pass))){
					errorEmail.setVisible(false);
					errorfirst.setVisible(false);
					errorlast.setVisible(false);
					errorstudent.setVisible(false);
					errorphone.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(true);
				}
				else{
					errorEmail.setVisible(false);
					errorfirst.setVisible(false);
					errorlast.setVisible(false);
					errorphone.setVisible(false);
					errorstudent.setVisible(false);
					errorPass.setVisible(false);
					errorPass2.setVisible(false);
				
				if(admin.isSelected()){
					try{
						User user = new User(first, last, email, Phone, pass);
						res = UserDAO.instance.addUserAdmin(user);
						CheckRes(res, user);
						
						PARENT.homePage.showAdmin();
						PARENT.requestBookpage.showHistory();
						PARENT.requestJournalpage.showHistory();
						PARENT.showPage(PARENT.HOME_PAGE);
						HomePage.userMenu.setText(user.getfName());
						HomePage.fname.setText("First Name: " + user.getfName());
						HomePage.lname.setText("Last Name: " + user.getLName());
						HomePage.email.setText("Email: " + user.getEmail());
						HomePage.phone.setText("Phone: " + user.getPhoneNo());
		
						UserUpdateDetails.FirstNmField.setText(user.getfName());
						UserUpdateDetails.LastNmField.setText(user.getLName());
						UserUpdateDetails.PhNoField.setText(user.getPhoneNo());
						UserUpdateDetails.StNumField.setText(user.getStudentNum());
						UserUpdateDetails.emailField.setText(user.getEmail());
					}catch(OliverExceptionHandler e){
						JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
				else if(student.isSelected()) {
					try {
						PARENT.homePage.showStandardUser();
						User user = new User(first, last, email, Phone, StudentNum, pass);
						res = UserDAO.instance.addUserReg(user);
						CheckRes(res, user);
						PARENT.requestBookpage.showHistory();
						PARENT.requestJournalpage.showHistory();
						String sub = "Terms and Conditions";
						String msg = "In the following is the terms and conditions";

						MailSender.instance.send(PARENT.user.getEmail(), sub, msg);

						// PARENT.getJMenuBar().setVisible(true);
						PARENT.showPage(PARENT.HOME_PAGE);
						HomePage.userMenu.setText(user.getfName());
						HomePage.fname.setText("First Name: " + user.getfName());
						HomePage.lname.setText("Last Name: " + user.getLName());
						HomePage.email.setText("Email: " + user.getEmail());
						HomePage.phone.setText("Phone: " + user.getPhoneNo());

						UserUpdateDetails.FirstNmField.setText(user.getfName());
						UserUpdateDetails.LastNmField.setText(user.getLName());
						UserUpdateDetails.PhNoField.setText(user.getPhoneNo());
						UserUpdateDetails.StNumField.setText(user.getStudentNum());
						UserUpdateDetails.emailField.setText(user.getEmail());

					} catch (OliverExceptionHandler e) {

						JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Please select a user type", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		}
	}
	
	
	public void CheckRes(boolean result, User user){
		
		if(result==true){
			PARENT.showPage(PARENT.HOME_PAGE);PARENT.user = user;
		}
		else{
			JOptionPane.showMessageDialog(null, "Registration was unsuccessful!!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}

