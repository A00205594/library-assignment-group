package View;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import DatabaseAccess.UserDAO;
import Model.User;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;

/*
 * GUI Built By Fergal McKiernan
 * 16/2/2017
 * 
 */
public class LoginPage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;

	public User user = null;

	JTextField username = new JTextField(10);
	JPasswordField password = new JPasswordField(10);

	JLabel usernm = new JLabel("Username: ");
	JLabel pass = new JLabel("Password: ");

	JButton loginButton = new JButton("Login");
	JButton CreateNew = new JButton("New User");

	public LoginPage(PageLoader parent) {
		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());

		JPanel main = new JPanel();
		main.setBorder(BorderFactory.createTitledBorder("Login"));
		main.setLayout(new GridLayout(3, 2));

		main.add(usernm);
		main.add(username);
		main.add(pass);
		main.add(password);
		main.add(loginButton);
		main.add(CreateNew);

		CreateNew.addActionListener(this);
		loginButton.addActionListener(this);

		this.add(main);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if (target == this.CreateNew) {
			PARENT.showPage(PARENT.REGISTER_PAGE);
		}
		if (target == this.loginButton) {
			if (username.getText() == "" || password.getText() == "") {
				JOptionPane.showMessageDialog(null, "Please Enter details", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				try {
					
					PARENT.user = UserDAO.instance.LoginUser(username.getText(), password.getText());
					
					if (PARENT.user == null) {
						JOptionPane.showMessageDialog(null, "Invlaid login", "Error", JOptionPane.ERROR_MESSAGE);
					} else {
						if(PageLoader.userAdmin){
							PARENT.homePage.showAdmin();
						}else {
							PARENT.homePage.showStandardUser();
						}
						//PARENT.getJMenuBar().setVisible(true);
						PARENT.showPage(PARENT.HOME_PAGE);
						HomePage.userMenu.setText(PARENT.user.getfName());
						HomePage.fname.setText("First Name: " + PARENT.user.getfName());
						HomePage.lname.setText("Last Name: " + PARENT.user.getLName());
						HomePage.email.setText("Email: " + PARENT.user.getEmail());
						HomePage.phone.setText("Phone: " + PARENT.user.getPhoneNo());
						username.setText("");
						password.setText("");
						UserUpdateDetails.FirstNmField.setText(PARENT.user.getfName());
						UserUpdateDetails.LastNmField.setText(PARENT.user.getLName());
						UserUpdateDetails.PhNoField.setText(PARENT.user.getPhoneNo());
						UserUpdateDetails.StNumField.setText(PARENT.user.getStudentNum());
						UserUpdateDetails.emailField.setText(PARENT.user.getEmail());
						
					}
				} catch (OliverExceptionHandler e) {
					e.printStackTrace();
				}
			}
		}
	}

}
