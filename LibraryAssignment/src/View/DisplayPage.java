package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import Model.Book;
import Model.DisplayModel;
import Model.Journal;
import PageController.PageLoader;

public class DisplayPage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;

	private JButton placeRequest, backToSearch;
	private JLabel itemTitle, itemPublisher, itemStock, itemGenre, itemReference, itemPublisherId, itemAuthor;
	private DisplayModel item;
	private JTextArea itemDesc;
	private JLabel publishCodeIndicator;

	public DisplayPage(PageLoader parent) {
		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new BorderLayout());
		initVars();
		JPanel northernDisplay = new JPanel(new BorderLayout());
		populateNorthen(northernDisplay);

		JPanel centreDisplay = new JPanel(new BorderLayout());
		populateCenter(centreDisplay);

		JPanel southernDisplay = new JPanel(new BorderLayout());
		populateSouth(southernDisplay);

		this.add(northernDisplay, BorderLayout.PAGE_START);
		this.add(centreDisplay, BorderLayout.CENTER);
		this.add(southernDisplay, BorderLayout.PAGE_END);
	}

	private void populateNorthen(JPanel north) {
		north.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		north.add(backToSearch, BorderLayout.LINE_START);
		JPanel backSearchPanel = new JPanel(new BorderLayout());
		backSearchPanel.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		backSearchPanel.add(backToSearch, BorderLayout.PAGE_START);
		north.add(backSearchPanel, BorderLayout.LINE_START);
		JPanel gridContainer = new JPanel(new GridLayout(2, 1));
		JLabel titleIndicator = new JLabel("Title:");
		gridContainer.add(getCombinedPanel(titleIndicator, itemTitle));
		JLabel itemAuthor = new JLabel("Autor:");
		gridContainer.add(getCombinedPanel(itemAuthor, itemAuthor));
		north.add(gridContainer, BorderLayout.CENTER);
	}

	private void populateCenter(JPanel center) {
		center.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		Box container = new Box(BoxLayout.PAGE_AXIS);
		JPanel upperPanel = new JPanel(new GridLayout(1, 3));
		JPanel midPanel = new JPanel(new GridLayout(1, 2));
		JPanel descContainer = new JPanel(new BorderLayout());
		container.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		upperPanel.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		midPanel.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		descContainer.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		descContainer.setBorder(new TitledBorder("Description"));
		itemDesc.setEditable(false);
		itemDesc.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		descContainer.add(itemDesc);

		JLabel stockIndicator = new JLabel("Stock:");
		JLabel libraryRefIndicator = new JLabel("Library Reference Number: ");
		upperPanel.add(getCombinedPanel(stockIndicator, itemStock));
		upperPanel.add(getCombinedPanel(libraryRefIndicator, itemReference));
		upperPanel.add(getCombinedPanel(publishCodeIndicator, itemPublisherId));

		JLabel publisherName = new JLabel("Publisher: ");
		JLabel genres = new JLabel("Genres: ");
		midPanel.add(getCombinedPanel(publisherName, itemPublisher));
		midPanel.add(getCombinedPanel(genres, itemGenre));

		container.add(upperPanel);
		container.add(midPanel);
		container.add(descContainer);
		center.add(container);
	}

	private void populateSouth(JPanel south) {
		south.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		south.add(placeRequest);
	}

	public JPanel getCombinedPanel(JLabel indicator, JLabel value) {
		JPanel comb = new JPanel(new FlowLayout());
		comb.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		comb.add(indicator);
		comb.add(value);
		return comb;
	}

	private void initVars() {
		item = null;
		placeRequest = new JButton("Place Request");
		placeRequest.addActionListener(this);
		backToSearch = new JButton("Back");
		backToSearch.addActionListener(this);
		itemTitle = new JLabel("A");
		itemPublisher = new JLabel("");
		itemStock = new JLabel("");
		itemGenre = new JLabel("");
		itemReference = new JLabel("");
		itemPublisherId = new JLabel("");
		itemAuthor = new JLabel("");
		itemDesc = new JTextArea();
		publishCodeIndicator = new JLabel();
	}

	public void updateFields() {
		itemTitle.setText(item.getItemTitle());
		itemPublisher.setText(item.getItemPublisher());
		itemStock.setText(item.getCurrentStock());
		itemGenre.setText(item.getItemGenre());
		itemPublisherId.setText(item.getPublisherIdentifier());
		itemDesc.setText(item.getItemDescription());
		itemReference.setText(item.getLibraryIdentifier());
		itemAuthor.setText(item.getItemAuthor());
		if (item instanceof Journal) {
			publishCodeIndicator.setText("ISSN:");
		} else {
			publishCodeIndicator.setText("ISBN:");
		}

	}

	public void setItem(DisplayModel item) {
		this.item = item;
		updateFields();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if (target == placeRequest) {
			if (item instanceof Book) {
				PARENT.requestBookpage.autoBookRequest(item.getItemId());
				PARENT.showPage(PARENT.REQUEST_BOOK_PAGE);
			} else if (item instanceof Journal) {
				//make call to parent here to show request page and to populate request page fields
				PARENT.requestJournalpage.autoJournalRequest(item.getItemId());
				PARENT.showPage(PARENT.REQUEST_JOURNAL_PAGE);
			}

		} else if (target == backToSearch) {
			PARENT.showPage(PARENT.SEARCH_PAGE);
		}

	}
}
