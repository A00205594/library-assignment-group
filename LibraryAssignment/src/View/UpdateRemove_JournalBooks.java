package View;

import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import DatabaseAccess.BookDAO;
import DatabaseAccess.JournalDAO;
import DatabaseAccess.UserDAO;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;

public class UpdateRemove_JournalBooks extends JPanel implements ActionListener{
		private static final long serialVersionUID = 1L;
		private final PageLoader PARENT;

		JTextField ID = new JTextField(10);
		JTextField Title = new JTextField(10);
		JTextField ISBN_ISSN = new JTextField(10);
		JTextField Author = new JTextField(10);
		JTextField Genre = new JTextField(10);
		JTextField Year_Published = new JTextField(10);
		JTextField Reference_Number = new JTextField(10);
		JTextField Stock_Number = new JTextField(10);
		JTextField Description = new JTextField(10);
		JTextField Publisher = new JTextField(10);

		JLabel JLabel = new JLabel("Id: ");
		JLabel TLabel = new JLabel("Title: ");
		JLabel ILabel = new JLabel("ISBN/ISSN: ");
		JLabel ALabel = new JLabel("Author: ");
		JLabel genreLabel = new JLabel("Genre: ");
		JLabel yearLabel = new JLabel("Year Published: ");
		JLabel refLabel = new JLabel("Reference Number: ");
		JLabel stockLabel = new JLabel("Stock Number: ");
		JLabel pubLabel = new JLabel("Publisher: ");
		JLabel desLabel = new JLabel("Description: ");

		JButton removeJournal = new JButton("Remove Journal");
		JButton removeBook = new JButton("Remove Book");
		JButton updateJournal = new JButton("Update Journal");
		JButton updateBook = new JButton("Update Book");

		
		public UpdateRemove_JournalBooks(PageLoader parent){
			this.PARENT=parent;
			this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
			this.setLayout(new FlowLayout());
			
			JPanel borderPanel = new JPanel();
			borderPanel.setLayout(new BorderLayout());
			borderPanel.setBorder(BorderFactory.createTitledBorder("Book/Journal Details"));
			
			JPanel requestP1 = new JPanel();
			requestP1.setLayout(new BorderLayout());
			
			JPanel requestP = new JPanel();
			requestP.setLayout(new GridLayout(10,2));
			
			requestP.add(JLabel);requestP.add(ID);
			requestP.add(TLabel);requestP.add(Title);
			requestP.add(ILabel);requestP.add(ISBN_ISSN);
			requestP.add(ALabel);requestP.add(Author);
			requestP.add(genreLabel);requestP.add(Genre);
			requestP.add(yearLabel);requestP.add(Year_Published);
			requestP.add(refLabel);requestP.add(Reference_Number);
			requestP.add(stockLabel);requestP.add(Stock_Number);
			requestP.add(pubLabel);requestP.add(Publisher);
			requestP.add(desLabel);requestP.add(Description);
			
			JPanel listPanel = new JPanel();
			listPanel.setLayout(new GridLayout(2,2));
			listPanel.add(removeJournal);listPanel.add(removeBook);
			listPanel.add(updateJournal);listPanel.add(updateBook);
		
			requestP1.add(requestP, BorderLayout.NORTH);
			requestP1.add(listPanel, BorderLayout.WEST);	
			
			borderPanel.add(requestP1);
		
			removeJournal.addActionListener(this);
			removeBook.addActionListener(this);
			updateJournal.addActionListener(this);
			updateBook.addActionListener(this);
			
			this.add(borderPanel);
			
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Object target = arg0.getSource();
			if(target == removeJournal){
				try {
					JournalDAO.instance.Remove_Journal(Title.getText());
					JournalDAO.instance.Remove_Journal2(ISBN_ISSN.getText());
					JournalDAO.instance.Remove_Journal3(Reference_Number.getText());
				} catch (OliverExceptionHandler e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
			
			if(target== removeBook){
				
				try {
					

					BookDAO.instance.Remove_Book(Title.getText());
					BookDAO.instance.Remove_Book2(ISBN_ISSN.getText());
					BookDAO.instance.Remove_Book3(Reference_Number.getText());

				} catch (OliverExceptionHandler e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(target==updateJournal){
				try {
					int stock_number = (Integer.parseInt(Stock_Number.getText()));
					int year_published = (Integer.parseInt(Year_Published.getText()));

					int id = (Integer.parseInt(ID.getText()));
					
					JournalDAO.instance.ModifyJournalTitle(id, Title.getText());
					JournalDAO.instance.ModifyJournalISSN(id, ISBN_ISSN.getText());
					JournalDAO.instance.ModifyJournalAuthor(id, Author.getText());
					JournalDAO.instance.ModifyJournalGenre(id, Genre.getText());
					JournalDAO.instance.ModifyJournalYear_Published(id, year_published);
					JournalDAO.instance.ModifyJournalReference_Number(id, Reference_Number.getText());
					JournalDAO.instance.ModifyJournalStock_Number(id, stock_number);
					JournalDAO.instance.ModifyDescription(id, Description.getText());
					JournalDAO.instance.ModifyPublisher(id, Publisher.getText());
				} catch (OliverExceptionHandler e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			if(target==updateBook){
				try {
					int stock_number = (Integer.parseInt(Stock_Number.getText()));
					int year_published = (Integer.parseInt(Year_Published.getText()));
					int id = (Integer.parseInt(ID.getText()));


					BookDAO.instance.ModifyTitle(id, Title.getText());
					BookDAO.instance.ModifyISBN(id, ISBN_ISSN.getText());
					BookDAO.instance.ModifyAuthor(id, Author.getText());
					BookDAO.instance.ModifyGenre(id, Genre.getText());
					BookDAO.instance.ModifyYear_Published(id, year_published);
					BookDAO.instance.ModifyReference_Number(id, Reference_Number.getText());
					BookDAO.instance.ModifyStock_Number(id, stock_number);
					BookDAO.instance.ModifyDescription(id, Description.getText());
					BookDAO.instance.ModifyPublisher(id, Publisher.getText());
			
				} catch (OliverExceptionHandler e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
}
