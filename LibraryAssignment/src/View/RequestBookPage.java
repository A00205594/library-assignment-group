package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import DatabaseAccess.BookDAO;
import DatabaseAccess.DbTable;
import DatabaseAccess.Utils;
import Model.Book;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;

public class RequestBookPage extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;
	
	///BookCart bookCart1 = new BookCart();
	
	
	Book testBook1 = new Book("A Sword of Night & Day", "David Gemmell", "A Sword of Night & Day", 5,
			"9781212345015", "E23456", "A Sword of Night & Day", "Heroic Fantasy", "2007");

	
	List<Book> bookCart = new ArrayList<Book>();				//book cart

	JTextArea CartList = new JTextArea(10, 20);
	
	JTextField Title = new JTextField(10);
	JTextField Author = new JTextField(10);
	JLabel TLabel = new JLabel("Title: ");
	JLabel ALabel = new JLabel("Author: ");
	JTextField Description = new JTextField(10);
	JLabel DLabel = new JLabel("Description: ");
	JTextField Year = new JTextField(10);
	JLabel YLabel = new JLabel("Year: ");
	
	private static DbTable BooksTable = new DbTable();
	private JTable TableofBooks=new JTable(BooksTable);
	private static DbTable HistoryTable = new DbTable();
	private JTable TableHistory=new JTable(HistoryTable);
	
	JLabel idLabel = new JLabel("Add/Remove from Cart ID: ");
	JTextField id = new JTextField(10);
	JTextField ID = new JTextField(10);
	
	JButton Deleterequest = new JButton("Delete From History");
	JButton AddRequest = new JButton("Add To Cart");
	JButton RemoveRequest = new JButton("Remove From Cart");
	JButton request = new JButton("Place Request");
	JButton cancel = new JButton("Cancel");

	private Statement stmt = null;
	private String query= "Book";
	private String historyQuery;
	
	public RequestBookPage(PageLoader parent){
		initiate_db_conn();
		this.PARENT=parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());
		
		JPanel requestP1 = new JPanel();
		requestP1.setLayout(new BorderLayout());
		requestP1.setBorder(BorderFactory.createTitledBorder("Place Book Request"));
		
		JPanel requestP2 = new JPanel();
		requestP2.setLayout(new GridLayout(3,2));
		requestP2.setBorder(BorderFactory.createTitledBorder("Edit Cart/History"));
		requestP2.add(idLabel);requestP2.add(ID);
		requestP2.add(AddRequest);requestP2.add(RemoveRequest);
		requestP2.add(Deleterequest);requestP2.add(id);
		
		JPanel requestP3 = new JPanel();
		requestP3.setLayout(new FlowLayout());
		requestP3.setBorder(BorderFactory.createTitledBorder("Add/Remove From Cart"));
		JScrollPane scroll2 = new JScrollPane(CartList);
		scroll2.setBorder(BorderFactory.createTitledBorder("Cart"));
		requestP3.add(scroll2);
		TableofBooks.setPreferredScrollableViewportSize(new Dimension(200, 100));
		JScrollPane scroll3 = new JScrollPane(TableofBooks);
		scroll3.setBorder(BorderFactory.createTitledBorder("Books"));
		scroll3.setPreferredSize(new Dimension(1000, 150));
		requestP3.add(requestP2);
		
		JPanel listPanel = new JPanel();
		listPanel.setLayout(new FlowLayout());
		TableHistory.setPreferredScrollableViewportSize(new Dimension(1000, 200));
		JScrollPane scroll = new JScrollPane(TableHistory);
		TableHistory.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scroll.setPreferredSize(new Dimension(400,200));
		listPanel.setBorder(BorderFactory.createTitledBorder("Request History"));
		listPanel.add(scroll);
		requestP3.add(listPanel);
		
		JPanel ButPanel = new JPanel();
		ButPanel.setLayout(new GridLayout(1,2));
		ButPanel.add(request);
		ButPanel.add(cancel);
		
		requestP1.add(requestP3, BorderLayout.CENTER);
		requestP1.add(scroll3, BorderLayout.NORTH);
		requestP1.add(ButPanel, BorderLayout.SOUTH);

		Deleterequest.addActionListener(this);
		RemoveRequest.addActionListener(this);
		AddRequest.addActionListener(this);
		request.addActionListener(this);
		cancel.addActionListener(this);
		this.add(requestP1);
		
		BooksTable.refreshTableDB(stmt, query);
	}
	
	public void showHistory(){
		historyQuery = "SELECT * FROM Requests WHERE StudentEmail= '"+PARENT.user.getEmail()+"' and RequestType='Book';";
		HistoryTable.refreshTableDB2(stmt, historyQuery);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if(target == request){
			for(int i=0; i<bookCart.size();i++){
				Book rbook = bookCart.get(i);
				try {
					BookDAO.instance.AddBookRequest(rbook.getId(),rbook.getTitle(),rbook.getISBN(),rbook.getAuthor(),rbook.getGenres(),
							rbook.getReleaseDate(), rbook.getLibraryReferenceNumber(), rbook.getDescription(), rbook.getPublisher(),PARENT.user.getEmail());
				} catch (OliverExceptionHandler e) {
					e.printStackTrace();
				}
			}
			showHistory();
		}
		if(target== cancel){
			PARENT.showPage(PARENT.HOME_PAGE);
		}
		if(target==AddRequest)
		{																				//Add a book to cart
			try 
			{
				Book rbook = BookDAO.instance.GetRequestedBook(Integer.parseInt(ID.getText()));
				
				CartList.append("Item: "+rbook.getId()+", "+rbook.getTitle()+"\n");
				bookCart.add(rbook);
				/*
				Book rbook = BookDAO.instance.GetRequestedBook(Integer.parseInt(ID.getText()));
				CartList.append("Item Index: "+bookCart1.BookCart.size() +", "+rbook.getTitle()+"\n");
				bookCart.add(rbook);
				if(bookCart1.addToCart(rbook))
				{
					CartList.append("Item Index: "+bookCart1.BookCart.size() +", "+rbook.getTitle()+"\n");
				}
				*/
			}
			catch (OliverExceptionHandler e) 
			{
				e.printStackTrace();
			}
		}
		if(target==Deleterequest){
			//remove from request History using ID param
			try {
			String DeleteQuery = "DELETE FROM requests WHERE Priority_Id = "+id.getText()+" and StudentEmail = '"+PARENT.user.getEmail()+"' ;";
				stmt.executeUpdate(DeleteQuery);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			HistoryTable.refreshTableDB2(stmt, historyQuery);
		}
		if(target==RemoveRequest)
		{
			/*																		//remove a book from cart
			int indexOfBookToBeDeleted =Integer.parseInt(ID.getText());

			if(bookCart1.removeBookFromCartByID(indexOfBookToBeDeleted))
			{
				//remove book from testBox
			}
			*/
			
		
			bookCart.remove((Integer.parseInt(ID.getText())-1));
			CartList.setText("");
			for(int i=0; i<bookCart.size();i++)
			{
				Book res = bookCart.get(i);
				CartList.append("Item: "+res.getId()+", "+res.getTitle()+"\n");
				
			}
			
		}
	}
	public void autoBookRequest(int itemId){
		ID.setText(itemId+"");
	}
	public void initiate_db_conn()
	{
		try
		{
			Connection con = Utils.getConnection();
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
	
}