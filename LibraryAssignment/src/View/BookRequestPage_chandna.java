package View;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import DatabaseAccess.DbTable;
import PageController.PageLoader;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import java.sql.*;

@SuppressWarnings("serial")
public class BookRequestPage_chandna extends JInternalFrame implements ActionListener
{	
	private final PageLoader PARENT;
	String cmd = null;
	

	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;
	
	private JScrollPane dbContentsPanel;

	private Border lineBorder;


	private static DbTable TableModel = new DbTable();

	private JTable TableofDBContents=new JTable(TableModel);
	private JTextField textField;
	private JTextField textField_1;

	private String query= "Requests";

	public BookRequestPage_chandna( PageLoader parent)
	{	
		this.PARENT=parent;
		setBorder(new LineBorder(null));
		
		setEnabled(true);

		//initiate_db_conn();
		
		content=getContentPane();
		content.setLayout(null);
		content.setBackground(Color.lightGray);
		lineBorder = BorderFactory.createEtchedBorder(15, Color.blue, Color.black);


		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel=new JScrollPane(TableofDBContents,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel.setBackground(Color.lightGray);
		dbContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder,"Database Content"));
		dbContentsPanel.setSize(509, 300);
		dbContentsPanel.setLocation(10, 11);
		content.add(dbContentsPanel);
		
		textField = new JTextField();
		textField.setBounds(555, 52, 82, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblEnetrBookId = new JLabel("Enetr Book ID and click to add / remove from cart:");
		lblEnetrBookId.setBounds(555, 27, 319, 14);
		getContentPane().add(lblEnetrBookId);
		
		JButton btnNewButton = new JButton("Add to cart");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(647, 51, 108, 23);
		getContentPane().add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane((Component) null, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(BorderFactory.createTitledBorder(lineBorder,"Book Cart"));
		scrollPane.setBackground(Color.LIGHT_GRAY);
		scrollPane.setBounds(555, 97, 331, 214);
		getContentPane().add(scrollPane);
		
		JButton btnNewButton_1 = new JButton("Place Request for Books in Cart");
		btnNewButton_1.setBounds(202, 348, 232, 23);
		getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Cancel");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton_2.setBounds(495, 348, 245, 23);
		getContentPane().add(btnNewButton_2);
		
		JScrollPane scrollPane_1 = new JScrollPane((Component) null, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setBorder(BorderFactory.createTitledBorder(lineBorder,"Book Request History"));
		scrollPane_1.setBackground(Color.LIGHT_GRAY);
		scrollPane_1.setBounds(47, 416, 447, 214);
		getContentPane().add(scrollPane_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(526, 481, 158, 20);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblEnterBookId = new JLabel("Delete book from history by ID");
		lblEnterBookId.setBounds(516, 456, 345, 14);
		getContentPane().add(lblEnterBookId);
		
		JButton btnSubmit = new JButton("SUBMIT");
		btnSubmit.setBounds(712, 480, 143, 23);
		getContentPane().add(btnSubmit);
		
		JButton btnRemoveFromCart = new JButton("Remove from Cart");
		btnRemoveFromCart.setBounds(765, 51, 121, 23);
		getContentPane().add(btnRemoveFromCart);
		
	

		//TableModel.refreshTableDB(stmt, query);
	}

	public void initiate_db_conn()
	{
		try
		{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			String url="jdbc:mysql://localhost:3306/AgileDatabase";
			
			con = DriverManager.getConnection(url, "root", "admin");
			
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}

	public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		try
		{					
			rs= stmt.executeQuery(cmd); 
		}
		catch(Exception e1)
		{e1.printStackTrace();
		}
		


	}
}
