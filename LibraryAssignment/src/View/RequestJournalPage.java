package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import DatabaseAccess.DbTable;
import DatabaseAccess.JournalDAO;
import DatabaseAccess.Utils;
import Model.Journal;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;

public class RequestJournalPage extends JPanel implements ActionListener{
		private static final long serialVersionUID = 1L;
		private final PageLoader PARENT;
		
		List<Journal> JournalCart = new ArrayList<Journal>();	
		
		JTextArea CartList = new JTextArea(10, 20);
		
		JTextField Title = new JTextField(10);
		JTextField Author = new JTextField(10);
		JLabel TLabel = new JLabel("Title: ");
		JLabel ALabel = new JLabel("Author: ");
		JTextField Description = new JTextField(10);
		JLabel SLabel = new JLabel("Subject: ");
		JTextField Year = new JTextField(10);
		JLabel YLabel = new JLabel("Year: ");
		
		private static DbTable JournalsTable = new DbTable();
		private JTable TableofJournals=new JTable(JournalsTable);
		private static DbTable HistoryTable = new DbTable();
		private JTable TableHistory=new JTable(HistoryTable);
		
		JLabel idLabel = new JLabel("Add/Remove from Cart ID: ");
		JTextField id = new JTextField(10);
		JTextField ID = new JTextField(10);
		
		
		JButton Deleterequest = new JButton("Delete From History");
		JButton AddRequest = new JButton("Add To Cart");
		JButton RemoveRequest = new JButton("Remove From Cart");
		JButton request = new JButton("Place Request");
		JButton cancel = new JButton("Cancel");
		
		private Statement stmt = null;
		private String query= "Journal";
		private String historyQuery;
		
		public RequestJournalPage(PageLoader parent){
			initiate_db_conn();
			this.PARENT=parent;
			this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
			this.setLayout(new FlowLayout());
			
			JPanel requestP1 = new JPanel();
			requestP1.setLayout(new BorderLayout());
			requestP1.setBorder(BorderFactory.createTitledBorder("Place Journal Request"));
			
			JPanel requestP2 = new JPanel();
			requestP2.setLayout(new GridLayout(3,2));
			requestP2.setBorder(BorderFactory.createTitledBorder("Edit Cart/History"));
			requestP2.add(idLabel);requestP2.add(ID);
			requestP2.add(AddRequest);requestP2.add(RemoveRequest);
			requestP2.add(Deleterequest);requestP2.add(id);
			
			JPanel requestP3 = new JPanel();
			requestP3.setLayout(new FlowLayout());
			requestP3.setBorder(BorderFactory.createTitledBorder("Add/Remove From Cart"));
			JScrollPane scroll2 = new JScrollPane(CartList);
			scroll2.setBorder(BorderFactory.createTitledBorder("Cart"));
			requestP3.add(scroll2);
			TableofJournals.setPreferredScrollableViewportSize(new Dimension(200, 100));
			JScrollPane scroll3 = new JScrollPane(TableofJournals);
			scroll3.setBorder(BorderFactory.createTitledBorder("Journals"));
			requestP3.add(requestP2);
			
			JPanel listPanel = new JPanel();
			listPanel.setLayout(new FlowLayout());
			TableHistory.setPreferredScrollableViewportSize(new Dimension(1000, 200));
			JScrollPane scroll = new JScrollPane(TableHistory);
			TableHistory.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			scroll.setPreferredSize(new Dimension(400,200));
			listPanel.setBorder(BorderFactory.createTitledBorder("Request History"));
			listPanel.add(scroll);
			requestP3.add(listPanel);
			
			JPanel ButPanel = new JPanel();
			ButPanel.setLayout(new GridLayout(1,2));
			ButPanel.add(request);
			ButPanel.add(cancel);
			
			requestP1.add(requestP3, BorderLayout.CENTER);
			requestP1.add(scroll3, BorderLayout.NORTH);
			requestP1.add(ButPanel, BorderLayout.SOUTH);

			Deleterequest.addActionListener(this);
			RemoveRequest.addActionListener(this);
			AddRequest.addActionListener(this);
			request.addActionListener(this);
			cancel.addActionListener(this);
			this.add(requestP1);
			
			JournalsTable.refreshTableDB(stmt, query);
		}
		
		public void showHistory(){
			historyQuery = "SELECT * FROM Requests WHERE StudentEmail= '"+PARENT.user.getEmail()+"' and RequestType='Journal';";
			HistoryTable.refreshTableDB2(stmt, historyQuery);
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Object target = arg0.getSource();
			if(target == request){
				for(int i=0; i<JournalCart.size();i++){
					Journal rjournal= JournalCart.get(i);
					try {
						JournalDAO.instance.AddJournalRequest(rjournal.getId(),rjournal.getTitle(),rjournal.getISSN(),rjournal.getAuthor(),rjournal.getGenres(),
								rjournal.getReleaseDate(), rjournal.getLibraryReferenceNumber(), rjournal.getDescription(), rjournal.getPublisher(),"student@mail");
					} catch (OliverExceptionHandler e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				showHistory();
				
			}
			if(target== cancel){
				PARENT.showPage(PARENT.HOME_PAGE);
			}
			if(target==AddRequest){
				//add request to cart
				try {
					Journal rjournal = JournalDAO.instance.GetRequestedJournal(Integer.parseInt(ID.getText()));
					CartList.append("Item: "+rjournal.getId()+", "+rjournal.getTitle()+"\n");
					JournalCart.add(rjournal);
				} catch (OliverExceptionHandler e) {
					e.printStackTrace();
				}
			}
			if(target==Deleterequest){
				//remove from request History using ID param
				try {
					String DeleteQuery = "DELETE FROM requests WHERE Priority_Id = "+id.getText()+" and StudentEmail = '"+PARENT.user.getEmail()+"' ;";
						stmt.executeUpdate(DeleteQuery);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					HistoryTable.refreshTableDB2(stmt, historyQuery);
			}
			if(target==RemoveRequest){
				//remove request from cart
				JournalCart.remove((Integer.parseInt(ID.getText())-1));
				CartList.setText("");
				for(int i=0; i<JournalCart.size();i++){
					Journal res = JournalCart.get(i);
					CartList.append("Item: "+res.getId()+", "+res.getTitle()+"\n");
				}
				
			}
		}
		public void autoJournalRequest(int itemId){
			ID.setText(itemId+"");
		}
		
		public void initiate_db_conn()
		{
			try
			{
				Connection con = Utils.getConnection();
				//Create a generic statement which is passed to the TestInternalFrame1
				stmt = con.createStatement();
			}
			catch(Exception e)
			{
				System.out.println("Error: Failed to connect to database\n"+e.getMessage());
			}
		}
}
