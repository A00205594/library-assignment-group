package View;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import DatabaseAccess.UserDAO;
import PageController.PageLoader;

/*
 * Created by Fergal
 * This has to be modified to our satisfaction
 */
public class HomePage extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	private final PageLoader PARENT;
	
	JMenuBar menu = new JMenuBar();
	JMenuItem searchMenu = new JMenuItem("Search");
	static  JMenu userMenu = new JMenu();
	//---------User menu------------------------------------
	JMenuItem update = new JMenuItem("Update Details");
	JMenuItem Home = new JMenuItem("Home Page");
	JMenuItem logout = new JMenuItem("Logout");
	
	//----------Books Menu-----------------------------------
	JMenuItem RequestBookPage = new JMenuItem("Request Book");
	JMenuItem ViewBookReq = new JMenuItem("View Book Request");
	JMenuItem UpdateBooks = new JMenuItem("Update/Remove Books");
	JMenuItem OrderNotification = new JMenuItem("Send Order Notification");
	
	//----------Journal Menu-----------------------------------
	JMenuItem RequestJournalPage = new JMenuItem("Request Journal");
	JMenuItem ViewJournalReq = new JMenuItem("View Journal Request");
	JMenuItem UpdateJournals = new JMenuItem("Update/Remove Journals");
	JMenuItem AddJournalsAddBook = new JMenuItem("Add Journals/Book");
	
	//----------Fines--Menu--------------------------------------------
	JMenuItem Fines = new JMenuItem("Add Fines");
	JMenuItem SendEmail = new JMenuItem("Send Email Noti.");
	JMenuItem duedate = new JMenuItem("Due Date");
	
	JMenu Books = new JMenu("Books");
	JMenu Journals = new JMenu("Journals");
	JMenu fines = new JMenu("Fines");
	
	//Main Panel contents
	static JLabel fname = new JLabel("First Name: ");
	static JLabel lname = new JLabel("Last Name: ");
	static JLabel email = new JLabel("Email: ");
	static JLabel phone = new JLabel("Phone: ");
	
	public HomePage(PageLoader parent){
		this.PARENT = parent;
		setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		
		userMenu.add(update);
		userMenu.add(Home);
		userMenu.add(logout);
		
		
		fines.add(Fines);
		fines.add(SendEmail);
		fines.add(duedate);
		searchMenu.setMinimumSize(new Dimension(60,20));
		searchMenu.setMaximumSize(new Dimension(60,20));
		searchMenu.setPreferredSize(new Dimension(60,20));
		/*
		menu.add(userMenu);
		menu.add(Books);
		menu.add(Journals);
		menu.add(fines);
		menu.add(searchMenu);
		*/
		
		
		logout.addActionListener(this);
		Home.addActionListener(this);
		update.addActionListener(this);
		RequestBookPage.addActionListener(this);
		searchMenu.addActionListener(this);
		ViewBookReq.addActionListener(this);
		UpdateBooks.addActionListener(this);
		RequestJournalPage.addActionListener(this);
		ViewJournalReq.addActionListener(this);
		UpdateJournals.addActionListener(this);
		AddJournalsAddBook.addActionListener(this);
		Fines.addActionListener(this);
		SendEmail.addActionListener(this);
		duedate.addActionListener(this);
		
		JPanel main = new JPanel();
		main.setLayout(new GridLayout(4,1));
		main.setBorder(BorderFactory.createTitledBorder("My Details"));
		main.add(fname);
		main.add(lname);
		main.add(email);
		main.add(phone);
		
		//PARENT.setJMenuBar(menu);
		this.add(main);
	}
	
	private void clearMenu(){
		menu.removeAll();
		Books.removeAll();
		Journals.removeAll();
	}
	public 	void showStandardUser(){
		clearMenu();
		Books.add(RequestBookPage);
		Journals.add(RequestJournalPage);
		menu.add(userMenu);
		menu.add(Books);
		menu.add(Journals);
		menu.add(searchMenu);
		PARENT.setJMenuBar(menu);
		if(!PARENT.getJMenuBar().isVisible()){
			PARENT.getJMenuBar().setVisible(true);
		}
	}
	public void showAdmin(){
		clearMenu();
		Books.add(RequestBookPage);
		Books.add(ViewBookReq);
		Books.add(UpdateBooks);
		Books.add(OrderNotification);
		
		Journals.add(RequestJournalPage);
		Journals.add(ViewJournalReq);
		Journals.add(UpdateJournals);
		Journals.add(AddJournalsAddBook);
		menu.add(userMenu);
		menu.add(Books);
		menu.add(Journals);
		menu.add(fines);
		menu.add(searchMenu);
		PARENT.setJMenuBar(menu);
		if(!PARENT.getJMenuBar().isVisible()){
			PARENT.getJMenuBar().setVisible(true);
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();
		
		if(target==this.logout){
			PARENT.user = UserDAO.instance.Logout();
			userMenu.setText("");
			PARENT.showPage(PARENT.LOGIN_PAGE);
			if(PARENT.getJMenuBar().isVisible()){
				PARENT.getJMenuBar().setVisible(false);
			}
		}
		if(target==this.Home){
			PARENT.showPage(PARENT.HOME_PAGE);
		}
		if(target==this.update){
			PARENT.showPage(PARENT.USER_UPDATE_DETAILS);
		}
		if(target==this.RequestBookPage){
			PARENT.showPage(PARENT.REQUEST_BOOK_PAGE);
		}
		if(target==this.searchMenu){
			PARENT.showPage(PARENT.SEARCH_PAGE);
		}
		
		if(target==this.AddJournalsAddBook){
			PARENT.showPage(PARENT.ADD_JOURNAL);
		}
		
		if(target==this.Fines){
			PARENT.showPage(PARENT.ADD_FINES);
		}
		
		if(target==this.ViewBookReq){
			PARENT.showPage(PARENT.VIEW_BOOK_REQUEST_PAGE);
		}
		if(target==this.UpdateBooks){
			PARENT.showPage(PARENT.UPDATE_REMOVE_JOURNAL);	
		}
		if(target==this.RequestJournalPage){
			PARENT.showPage(PARENT.REQUEST_JOURNAL_PAGE);
		}
		if(target==this.ViewJournalReq){
			PARENT.showPage(PARENT.VIEW_JOURNAL_REQUEST_PAGE);
		}
		if(target==this.SendEmail){
			PARENT.showPage(PARENT.SEND_FINAL_EMAIL_PAGE);
		}
		if(target==this.duedate){
			PARENT.showPage(PARENT.DUE_DATE);
		}
		if(target==this.UpdateJournals){
			PARENT.showPage(PARENT.UPDATE_REMOVE_JOURNAL);	

		}
	}
}
