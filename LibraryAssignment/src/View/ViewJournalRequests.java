package View;

import java.awt.BorderLayout;
import Utils.MailSender;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import DatabaseAccess.DbTable;
import DatabaseAccess.Utils;
import PageController.PageLoader;

public class ViewJournalRequests extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;
	
	private static DbTable RequestsTable = new DbTable();
	private JTable TableofDBContents=new JTable(RequestsTable);
	
	JButton process = new JButton("Send Notification");
	JButton home = new JButton("Home");
	
	private Statement stmt = null;
	private String query= "Requests";
	
	JTextField ToTxt = new JTextField(10);
	JLabel ToLabel = new JLabel("Student Email Address: ");
	JTextField SubTxt = new JTextField(10);
	JLabel SubLabel = new JLabel("Subject: ");
	JTextField MsgTxt = new JTextField(10);
	JLabel MsgLabel = new JLabel("Message: ");
	
	public ViewJournalRequests(PageLoader parent){
		initiate_db_conn();
		this.PARENT=parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());
		
		JPanel viewrequest = new JPanel();
		viewrequest.setLayout(new BorderLayout());
		viewrequest.setBorder(BorderFactory.createTitledBorder("View Journal Requests"));
		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(1,2));
		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(400, 400));
		JScrollPane scroll = new JScrollPane(TableofDBContents);
		scroll.setSize(700, 300);
		
		JPanel params = new JPanel();
		params.setLayout(new FlowLayout());
		params.setBorder(BorderFactory.createTitledBorder("Process Details"));
		params.add(ToLabel);params.add(ToTxt);
		params.add(SubLabel);params.add(SubTxt);
		params.add(MsgLabel);params.add(MsgTxt);
		
		buttons.add(process);buttons.add(home);
		viewrequest.add(scroll, BorderLayout.NORTH);
		viewrequest.add(params, BorderLayout.CENTER);
		viewrequest.add(buttons, BorderLayout.SOUTH);
		
		process.addActionListener(this);
		home.addActionListener(this);
		this.add(viewrequest);
		
		RequestsTable.refreshTableDB(stmt, query);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if(target==process){
			String to = ToTxt.getText();
			String subject = SubTxt.getText();
			String message = MsgTxt.getText();
			MailSender.instance.send(to, subject, message);
		}
		if(target==home){
			PARENT.showPage(PARENT.HOME_PAGE);
		}
		
	}
	public void initiate_db_conn()
	{
		try
		{
			Connection con = Utils.getConnection();
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
}
