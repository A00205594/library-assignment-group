package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import DatabaseAccess.BookDAO;
import DatabaseAccess.JournalDAO;
import PageController.PageLoader;
import Tests.RuairiTestStringHan;

public class AddJournalAddBookPage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;

	//==============================================
	//				Journal
	//==============================================
	JTextField id;
	JTextField title;
	JTextField issn;
	JTextField author;
	JTextField genre;
	JTextField yearPub;
	JTextField refNum;
	JTextField stockNum;
	JTextField des;
	JTextField publish;

	JLabel labelId = new JLabel("ID: ");
	JLabel labelTitle = new JLabel("Title: ");
	JLabel labelIssn = new JLabel("ISSN: ");
	JLabel labelAuthor = new JLabel("Author: ");
	JLabel labelGenre = new JLabel("Genre: ");
	JLabel labelYear = new JLabel("Year Published: ");
	JLabel labelRef = new JLabel("Reference Number: ");
	JLabel labelStock = new JLabel("Stock Number: ");
	JLabel labelDes = new JLabel("Description: ");
	JLabel labelPublish = new JLabel("Publisher: ");
	
	//=================================================
	//					Book
	//=================================================
	
	JTextField bookId;
	JTextField bookTitle;
	JTextField bookIsbn;
	JTextField bookAuthor;
	JTextField bookGenre;
	JTextField bookYearPub;
	JTextField bookRefNum;
	JTextField bookStockNum;
	JTextField bookDes;
	JTextField bookPublish;

	JLabel bookLabelId = new JLabel("ID: ");
	JLabel bookLabelTitle = new JLabel("Title: ");
	JLabel bookLabelIssn = new JLabel("ISBN: ");
	JLabel bookLabelAuthor = new JLabel("Author: ");
	JLabel bookLabelGenre = new JLabel("Genre: ");
	JLabel bookLabelYear = new JLabel("Year Published: ");
	JLabel bookLabelRef = new JLabel("Reference Number: ");
	JLabel bookLabelStock = new JLabel("Stock Number: ");
	JLabel bookLabelDes = new JLabel("Description: ");
	JLabel bookLabelPublish = new JLabel("Publisher: ");
	
	//===============================================
	//===============================================
	//				Journal Buttons
	//===============================================
	//===============================================
	
	JButton updateJournal = new JButton("Add Journal");
	JButton clearFields = new JButton("Clear Feilds");

	//==============================================
	//==============================================
	//				Book Buttons
	//==============================================
	//==============================================
	
	JButton updateBook = new JButton("Add Book");
	JButton clearBookFields = new JButton("Clear Feilds");
	
	//==============================================
	//=============================================
	
	public AddJournalAddBookPage(PageLoader parent) {
		this.PARENT=parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());
		
	//==============================================
	//				Journal Fields
	//==============================================	
		
		id = new JTextField(10);
		title = new JTextField(10);
		issn = new JTextField(10);
		 author = new JTextField(10);
		genre = new JTextField(10);
		yearPub = new JTextField(10);
		refNum = new JTextField(10);
		stockNum = new JTextField(10);
		des = new JTextField(10);
		publish = new JTextField(10);
		
	//=================================================
	//					Book Fields
	//=================================================		
		

		bookId = new JTextField(10);
		bookTitle = new JTextField(10);
		bookIsbn = new JTextField(10);
		bookAuthor = new JTextField(10);
		bookGenre = new JTextField(10);
		bookYearPub = new JTextField(10);
		bookRefNum = new JTextField(10);
		bookStockNum = new JTextField(10);
		bookDes = new JTextField(10);
		bookPublish = new JTextField(10);
	
	//==============================================
	//				Journal Panel
	//==============================================		
		JPanel addP1 = new JPanel();
		addP1.setLayout(new BorderLayout());
		
		JPanel addP = new JPanel();
		addP.setLayout(new GridLayout(10,2));
		
		addP.add(labelId);addP.add(id);
		addP.add(labelTitle);addP.add(title);
		addP.add(labelIssn);addP.add(issn);
		addP.add(labelAuthor);addP.add(author);
		addP.add(labelGenre);addP.add(genre);
		addP.add(labelYear);addP.add(yearPub);
		addP.add(labelRef);addP.add(refNum);
		addP.add(labelStock);addP.add(stockNum);
		addP.add(labelDes);addP.add(des);
		addP.add(labelPublish);addP.add(publish);
		
		
		
		JPanel listPanel = new JPanel();
		listPanel.setLayout(new GridLayout(1,1));
		listPanel.add(updateJournal);listPanel.add(clearFields);
	
		addP1.add(addP, BorderLayout.NORTH);
		addP1.add(listPanel, BorderLayout.WEST);			
	
		updateJournal.addActionListener(this);
		clearFields.addActionListener(this);
		
		this.add(addP1);
		
	//==============================================
	//				Book Panel
	//==============================================
		
		JPanel bookAddP1 = new JPanel();
		bookAddP1.setLayout(new BorderLayout());
		
		JPanel BookAddP = new JPanel();
		BookAddP.setLayout(new GridLayout(10,2));
		
		BookAddP.add(bookLabelId);BookAddP.add(bookId);
		BookAddP.add(bookLabelTitle);BookAddP.add(bookTitle);
		BookAddP.add(bookLabelIssn);BookAddP.add(bookIsbn);
		BookAddP.add(bookLabelAuthor);BookAddP.add(bookAuthor);
		BookAddP.add(bookLabelGenre);BookAddP.add(bookGenre);
		BookAddP.add(bookLabelYear);BookAddP.add(bookYearPub);
		BookAddP.add(bookLabelRef);BookAddP.add(bookRefNum);
		BookAddP.add(bookLabelStock);BookAddP.add(bookStockNum);
		BookAddP.add(bookLabelDes);BookAddP.add(bookDes);
		BookAddP.add(bookLabelPublish);BookAddP.add(bookPublish);
		
		
		
		JPanel bookListPanel = new JPanel();
		bookListPanel.setLayout(new GridLayout(1,1));
		bookListPanel.add(updateBook);bookListPanel.add(clearBookFields);
	
		bookAddP1.add(BookAddP, BorderLayout.NORTH);
		bookAddP1.add(bookListPanel, BorderLayout.WEST);			
	
		
		updateBook.addActionListener(this);
		clearBookFields.addActionListener(this);
		
		this.add(bookAddP1);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();

		if(target==updateJournal){
	 
			
			try {
				int ids = (Integer.parseInt(id.getText()));
				String titles = title.getText();
				String issns = issn.getText();
				String authors = author.getText();
				String genres = genre.getText();
				int yearPubs = (Integer.parseInt(yearPub.getText()));
				String refNums = refNum.getText();
				int stockNums = (Integer.parseInt(stockNum.getText()));
				String descript = des.getText();
				String publisher = publish.getText();
				
				
				JournalDAO.instance.addValues(ids, titles, issns, authors, genres, yearPubs, refNums,stockNums,descript,publisher);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RuairiTestStringHan e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		if(target==clearFields){
			
			id.setText("");
			title.setText("");
			issn.setText("");
			 author.setText("");
			genre.setText("");
			yearPub.setText("");
			refNum.setText("");
			stockNum.setText("");
			des.setText("");
			publish.setText("");
		}
		

		
		
		if(target==updateBook){
			 
			try {
				
				int ids = (Integer.parseInt(bookId.getText()));
				String titles = bookTitle.getText();
				String issns = bookIsbn.getText();
				String authors = bookAuthor.getText();
				String genres = bookGenre.getText();
				int yearPubs = (Integer.parseInt(bookYearPub.getText()));
				String refNums = bookRefNum.getText();
				int stockNums = (Integer.parseInt(bookStockNum.getText()));
				String descript = bookDes.getText();
				String publisher = bookPublish.getText();
				
				BookDAO.instance.addValuesBook(ids, titles, issns, authors, genres, yearPubs, refNums,stockNums,descript,publisher);
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RuairiTestStringHan e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		if(target==clearBookFields){
			
			bookId.setText("");
			bookTitle.setText("");
			bookIsbn.setText("");
			bookAuthor.setText("");
			bookGenre.setText("");
			bookYearPub.setText("");
			bookRefNum.setText("");
			bookStockNum.setText("");
			bookDes.setText("");
			bookPublish.setText("");
		}
	}
		

	
}