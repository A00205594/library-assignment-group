package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import DatabaseAccess.BookDAO;
import DatabaseAccess.FinalReturnNotifyDAO;
import DatabaseAccess.JournalDAO;
import DatabaseAccess.UserDAO;
import Model.Book;
import Model.Journal;
import Model.User;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;
/*
 * GUI Built By Oliver O'Connor
 * modified on 25/02/2017
 * 
 * 
 */
public class SendFinalEmailPage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;

	JButton genmsgbutton = new JButton("Send Notif");
	JButton checkbutton = new JButton("Check DB");
	
	JTextField firnameTF = new JTextField(5);
	JTextField lasnameTF = new JTextField(5);
	JTextField BookIDTF = new JTextField(5);
	JTextField BookTitleTF = new JTextField(5);
	JTextField JournalIDTF = new JTextField(5);
	JTextField JournalTitleTF = new JTextField(5);
	JTextField emailrepTF = new JTextField(5);
	JTextField TimeOverdueTF = new JTextField(5);
	JTextField AmountDueTF = new JTextField(5);
	
	
	
	
	public SendFinalEmailPage(PageLoader parent) {
		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());
		
		JPanel TopPanel = new JPanel();
		
	
		JLabel firnameLB = new JLabel("First name:");
		JLabel lasnameLB = new JLabel("Last name:");
		JLabel BookIDLB = new JLabel("Book ID:");
		JLabel BookTitleLB = new JLabel("Book Title");
		JLabel JournalIDLB = new JLabel("Journal ID:");
		JLabel JournalTitleLB = new JLabel("Journal Title:");
		JLabel emailrepLB = new JLabel("Email:");
		JLabel TimeOverdueLB = new JLabel("Time overdue:");
		JLabel AmountDueLB = new JLabel("Amount due:");
		
		
		JLabel bookjournLB = new JLabel("Book/Journal");
		JCheckBox check = new JCheckBox();
		
		TopPanel .setLayout(new GridLayout(9,2));
		
		TopPanel.add(firnameLB);	TopPanel.add(firnameTF);
		
		TopPanel.add(lasnameLB);	TopPanel.add(lasnameTF);
		
		TopPanel.add(emailrepLB);	TopPanel.add(emailrepTF);
		
		TopPanel.add(BookIDLB);		TopPanel.add(BookIDTF);
		
		TopPanel.add(BookTitleLB);	TopPanel.add(BookTitleTF);
		
		TopPanel.add(JournalIDLB);	TopPanel.add(JournalIDTF);
		
		TopPanel.add(JournalTitleLB); TopPanel.add(JournalTitleTF);
		
		TopPanel.add(TimeOverdueLB);  TopPanel.add(TimeOverdueTF);	
		
		TopPanel.add(AmountDueLB);	TopPanel.add(AmountDueTF);
		
		
		JPanel BottomPanel = new JPanel();
			
		BottomPanel.add(genmsgbutton);
		BottomPanel.add(checkbutton);
		
		JPanel doublePanel = new JPanel();
		doublePanel.setLayout(new BorderLayout());
		doublePanel.add(TopPanel, BorderLayout.NORTH);
		doublePanel.add(BottomPanel, BorderLayout.SOUTH);
		
		this.add(doublePanel);
		genmsgbutton.addActionListener(this);
		checkbutton.addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		Object target = arg0.getSource();
		
		String fname ="";
		String lastname="";
		String email="";
		int bookid=0;
		String booktitle="";
		int journid=0;
		String journtitle="";
		int timeoverdue=0;
		float amountdue=0;
		
		// this method adds a new final return notification by using a DAO class
		if(target == genmsgbutton)
		{
			fname = firnameTF.getText(); 
		    lastname = lasnameTF.getText(); 
			bookid = Integer.parseInt(BookIDTF.getText()); 
			booktitle = BookTitleTF.getText();  
			journid = Integer.parseInt(JournalIDTF.getText());  
			journtitle = JournalTitleTF.getText(); 
			email = emailrepTF.getText();  
			timeoverdue = Integer.parseInt(TimeOverdueTF.getText()); 
			amountdue = Float.parseFloat(AmountDueTF.getText()); 
			
		 FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(0,fname,lastname,email,bookid,
				 booktitle,journid,journtitle,timeoverdue,amountdue);
		}
		
		// this button first checks the database for valid user,book and journal details
		if(target==checkbutton){
			
			String firnameTFval = firnameTF.getText();
			int bookIDTFval = Integer.parseInt(BookIDTF.getText());
			int JournalIDTFval = Integer.parseInt(JournalIDTF.getText());
			
			try {
				User user = UserDAO.instance.GetRequestedUser(firnameTFval);
				Book book = BookDAO.instance.GetRequestedBook(bookIDTFval);
				Journal journ = JournalDAO.instance.GetRequestedJournal(JournalIDTFval);
				
				lasnameTF.setText(user.getLName());
				BookTitleTF.setText(book.getTitle());
				JournalTitleTF.setText(journ.getTitle());
				emailrepTF.setText(user.getEmail());
				
				
				
			} catch (OliverExceptionHandler e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	} // end method
	
	

}
