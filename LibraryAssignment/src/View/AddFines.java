package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import DatabaseAccess.BookDAO;
import DatabaseAccess.DbTable;
import DatabaseAccess.FinesDAO;
import DatabaseAccess.JournalDAO;
import DatabaseAccess.Utils;
import PageController.PageLoader;
import Tests.RuairiTestStringHan;

public class AddFines extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;
	
	private static DbTable fineTable = new DbTable();
	private JTable TableofDBContents;
	
	
	JButton home = new JButton("Home");
	
	JScrollPane scroll;
	 
	private Statement stmt = null;
	private String query= "Fines";

	JTextField fines;
	JTextField email;

	JLabel emailLabel = new JLabel("Email: ");
	JLabel finesLabel = new JLabel("Add Fine: ");

	JButton addFines = new JButton("Add Fines");
	JButton clearFields = new JButton("Clear Feilds");

	// ==============================================
	// =============================================

	public AddFines(PageLoader parent) {
		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());
		initiate_db_conn();
		fineTable.refreshTableDB(stmt, query);
		
		TableofDBContents = new JTable(fineTable);
		scroll = new JScrollPane(TableofDBContents);
		scroll.setSize(600, 300);
		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(1200,100));
	
		
		JPanel viewrequest = new JPanel();
		viewrequest.setLayout(new BorderLayout());
		viewrequest.setBorder(BorderFactory.createTitledBorder("View Book Requests"));
		viewrequest.add(scroll);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		
		fines = new JTextField(10);
		email = new JTextField(10);
		
		JPanel addP = new JPanel();
		addP.setLayout(new GridLayout(3,2));
		
		addP.add(emailLabel);addP.add(email);
		addP.add(finesLabel);addP.add(fines);
		addP.add(addFines);addP.add(clearFields);
		
		addFines.addActionListener(this);
		clearFields.addActionListener(this);
		
		mainPanel.add(addP, BorderLayout.CENTER);
		
		viewrequest.add(mainPanel, BorderLayout.NORTH);
		this.add(viewrequest);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		
			if(target==addFines){
	 
			
			try {
				double fine = (Double.parseDouble(fines.getText()));
				String emails = email.getText();
				
			//	BookDAO.instance.addBookFines( fine, emails);
				FinesDAO.instance.addFines( fine, emails);
				
				fineTable.refreshTableDB(stmt,"Fines");
				scroll.getViewport().revalidate();
				scroll.getViewport().repaint();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RuairiTestStringHan e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			}
		if(target==clearFields){
			
			fines.setText("");
			email.setText("");
			
		}
	
	}
	
	public void initiate_db_conn()
	{
		try
		{
			Connection con = Utils.getConnection();
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
}
