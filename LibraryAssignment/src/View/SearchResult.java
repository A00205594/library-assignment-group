package View;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Model.Book;
import Model.DisplayModel;
import PageController.PageLoader;

public class SearchResult extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	public DisplayModel item;
	private JButton view;
	private final PageLoader SUPER_PARENT;
	public SearchResult(DisplayModel item,PageLoader SUPER_PARENT) {
		this.SUPER_PARENT=SUPER_PARENT;
		this.setBackground(SUPER_PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new BorderLayout());
		this.item = item;
		JPanel itemListed = new JPanel(new GridLayout(2,3));
		itemListed.setBackground(SUPER_PARENT.DEFAULT_BACKGROUND_COLOUR);
		view = new JButton("View Item");
		view.addActionListener(this);
		JLabel title = new JLabel("Title:" + item.getItemTitle());
		JLabel author = new JLabel("Author:" + item.getItemAuthor());
		JLabel publisherId;
		if (item instanceof Book) {
			publisherId = new JLabel("ISBN: " + item.getPublisherIdentifier());
		} else {
			publisherId = new JLabel("ISSN: " + item.getPublisherIdentifier());
		}

		JLabel genre = new JLabel("Genres: " + item.getItemGenre());
		JLabel release = new JLabel("Released: " + item.getItemReleaseDate());
		JLabel libraryReference = new JLabel("Library Reference:" + item.getLibraryIdentifier());

		itemListed.add(title);
		itemListed.add(author);
		itemListed.add(publisherId);
		itemListed.add(genre);
		itemListed.add(release);
		itemListed.add(libraryReference);
		
		this.add(itemListed,BorderLayout.CENTER);
		this.add(view,BorderLayout.PAGE_END);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if (target == view) {
			SUPER_PARENT.displayItemPage.setItem(item);
			SUPER_PARENT.showPage(SUPER_PARENT.DISPLAY_ITEM_PAGE);
		}
	}

}
