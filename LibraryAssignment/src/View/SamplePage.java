package View;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import PageController.PageLoader;

/*
 * Created By Joseph O'Leary
 * 25/1/2017
 * 
 */
public class SamplePage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;

	// Standard Swing code here
	private JButton showWelcomePageButton;
	private JButton showGoodbyePageButton;

	public SamplePage(PageLoader parent) {

		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);

		this.setLayout(new BorderLayout());

		showWelcomePageButton = new JButton("Show Welcome Page");
		showWelcomePageButton.addActionListener(this);
		showGoodbyePageButton = new JButton("Show Goodbye Page");
		showGoodbyePageButton.addActionListener(this);

		JPanel buttonsPanel = new JPanel(new GridLayout(1, 2));
		buttonsPanel.add(showGoodbyePageButton);
		buttonsPanel.add(showWelcomePageButton);

		this.add(new JLabel("I AM THE HELLO PAGE"), BorderLayout.PAGE_START);
		this.add(buttonsPanel, BorderLayout.PAGE_END);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		
	}

}
