package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import DatabaseAccess.BookDAO;
import DatabaseAccess.JournalDAO;
import Model.Book;
import Model.DisplayModel;
import Model.Journal;
import PageController.PageLoader;

public class SearchPage extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;
	private JButton searchButton;
	private JTextField searchField;
	private JComboBox<String> searchCategory;
	private JRadioButton books, journals, all;
	private JPanel resultsContainer;
	private JScrollPane scroller;
	public SearchPage(PageLoader parent) {
		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		initVars();
		this.setLayout(new BorderLayout());

		JPanel upperSearchPanel = new JPanel(new BorderLayout());

		populateNorthernPanel(upperSearchPanel);

		this.add(upperSearchPanel, BorderLayout.PAGE_START);
		this.add(scroller, BorderLayout.CENTER);
	}

	private void initVars() {
		searchButton = new JButton("Search");
		searchButton.addActionListener(this);
		searchField = new JTextField();
		searchCategory = new JComboBox<String>();
		searchCategory.addItem("Title");
		searchCategory.addItem("Author");
		searchCategory.addItem("Genre");
		searchCategory.addItem("Release Date");
		searchCategory.addItem("Library Reference Number");
		searchCategory.addItem("ISBN/ISSN");
		searchCategory.addItem("All");
		books = new JRadioButton("Books");
		books.setSelected(true);
		journals = new JRadioButton("Journals");
		all = new JRadioButton("All");
		resultsContainer = new JPanel();
		resultsContainer.setLayout(new GridLayout(0,1,20,10));
		resultsContainer.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		scroller = new JScrollPane(resultsContainer);
		

	}

	public void update(ArrayList<DisplayModel> items) {
		resultsContainer.removeAll();

		for (DisplayModel dip : items) {
			resultsContainer.add(new SearchResult(dip, PARENT));
		}

		scroller.getViewport().repaint();
		scroller.getViewport().revalidate();

	}

	private void populateNorthernPanel(JPanel north) {
		north.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		JPanel searchPanel = new JPanel(new GridLayout(1, 5));
		searchPanel.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);

		searchPanel.add(new JLabel("Search:"));
		searchPanel.add(searchField);
		searchPanel.add(new JLabel("Category:"));
		searchPanel.add(searchCategory);
		searchPanel.add(searchButton);
		JPanel belowSearch = new JPanel(new FlowLayout());
		belowSearch.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		ButtonGroup bg = new ButtonGroup();
		bg.add(books);
		bg.add(journals);
		bg.add(all);
		belowSearch.add(books);
		belowSearch.add(journals);
		belowSearch.add(all);
		books.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		journals.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		all.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);

		JPanel holder = new JPanel(new GridLayout(3, 1));
		holder.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		holder.add(searchPanel);
		holder.add(belowSearch);
		holder.add(new JSeparator(SwingConstants.HORIZONTAL));

		north.add(holder);
	}

	public ArrayList<DisplayModel> titleSearch(ArrayList<DisplayModel> list, String search) {
		ArrayList<DisplayModel> results = new ArrayList<DisplayModel>();
		search = search.toLowerCase();
		for (DisplayModel a : list) {
			if (a.getItemTitle().toLowerCase().contains(search)) {
				results.add(a);
			}
		}
		return results;
	}

	public ArrayList<DisplayModel> authorSearch(ArrayList<DisplayModel> list, String search) {
		ArrayList<DisplayModel> results = new ArrayList<DisplayModel>();
		search = search.toLowerCase();
		for (DisplayModel a : list) {
			if (a.getItemAuthor().toLowerCase().contains(search)) {
				results.add(a);
			}
		}
		return results;
	}

	public ArrayList<DisplayModel> genreSearch(ArrayList<DisplayModel> list, String search) {
		ArrayList<DisplayModel> results = new ArrayList<DisplayModel>();
		search = search.toLowerCase();
		for (DisplayModel a : list) {
			if (a.getItemGenre().toLowerCase().contains(search)) {
				results.add(a);
			}
		}
		return results;
	}

	public ArrayList<DisplayModel> releaseDateSearch(ArrayList<DisplayModel> list, String search) {
		ArrayList<DisplayModel> results = new ArrayList<DisplayModel>();
		search = search.toLowerCase();
		for (DisplayModel a : list) {
			if (a.getItemReleaseDate().toLowerCase().contains(search)) {
				results.add(a);
			}
		}
		return results;
	}

	public ArrayList<DisplayModel> libraryReferenceSearch(ArrayList<DisplayModel> list, String search) {
		ArrayList<DisplayModel> results = new ArrayList<DisplayModel>();
		search = search.toLowerCase();
		for (DisplayModel a : list) {
			if (a.getLibraryIdentifier().toLowerCase().contains(search)) {
				results.add(a);
			}
		}
		return results;
	}

	public ArrayList<DisplayModel> publisherIdSearch(ArrayList<DisplayModel> list, String search) {
		ArrayList<DisplayModel> results = new ArrayList<DisplayModel>();
		search = search.toLowerCase();
		for (DisplayModel a : list) {
			if (a.getPublisherIdentifier().toLowerCase().contains(search)) {
				results.add(a);
			}
		}
		return results;
	}

	public ArrayList<DisplayModel> search(ArrayList<DisplayModel> list, String searchCategory, String searchTerm) {
		ArrayList<DisplayModel> results = new ArrayList<DisplayModel>();
		switch (searchCategory) {
		case "Title":
			results.addAll(titleSearch(list, searchTerm));
			break;
		case "Author":
			results.addAll(authorSearch(list, searchTerm));
			break;
		case "Genre":
			results.addAll(genreSearch(list, searchTerm));
			break;
		case "Release Date":
			results.addAll(releaseDateSearch(list, searchTerm));
			break;
		case "Library Reference Number":
			results.addAll(libraryReferenceSearch(list, searchTerm));
			break;
		case "ISBN/ISSN":
			results.addAll(publisherIdSearch(list, searchTerm));
			break;
		case "All":
			results.addAll(titleSearch(list, searchTerm));
			results.addAll(authorSearch(list, searchTerm));
			results.addAll(genreSearch(list, searchTerm));
			results.addAll(releaseDateSearch(list, searchTerm));
			results.addAll(libraryReferenceSearch(list, searchTerm));
			results.addAll(publisherIdSearch(list, searchTerm));
			break;
		}
		return results;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		if (target == searchButton) {

			ArrayList<Book> bookList = BookDAO.instance.getAll();
			ArrayList<Journal> journalList = JournalDAO.instance.getAll();
			ArrayList<DisplayModel> searchItems = new ArrayList<DisplayModel>();

			String searchBy = (String) searchCategory.getSelectedItem();
			String searchTerm = searchField.getText();
			if (books.isSelected()) {
				searchItems.addAll(bookList);

			} else if (journals.isSelected()) {
				searchItems.addAll(journalList);

			} else {
				searchItems.addAll(bookList);
				searchItems.addAll(journalList);
			}
			ArrayList<DisplayModel> res = search(searchItems, searchBy, searchTerm);
			update(res);
		}
	}

}
