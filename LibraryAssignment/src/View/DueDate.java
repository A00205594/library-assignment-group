package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import DatabaseAccess.DbTable;
import DatabaseAccess.FinesDAO;
import DatabaseAccess.Utils;
import PageController.PageLoader;
import Tests.RuairiTestStringHan;

public class DueDate extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final PageLoader PARENT;
	
	private static DbTable fineTable = new DbTable();
	private JTable TableofDBContents;
	
	JButton home = new JButton("Home");
	
	JScrollPane scroll;
	 
	private Statement stmt = null;
	private String query= "Fines";

	JTextField date;
	JTextField date2;
	
	JLabel dueBookDate = new JLabel("Enter Book: ");
	JLabel dueJournalDate = new JLabel("Enter Journal: ");

	JButton dueDate2 = new JButton("Get Book Date");
	JButton dueDate3 = new JButton("Get Journal Date");
	

	// ==============================================
	// =============================================

	public DueDate(PageLoader parent) {
		this.PARENT = parent;
		this.setBackground(PARENT.DEFAULT_BACKGROUND_COLOUR);
		this.setLayout(new FlowLayout());
		initiate_db_conn();
		fineTable.refreshTableDB(stmt, query);
		//fineTable.refreshTableDB2(stmt, query2);
		
		TableofDBContents = new JTable(fineTable);
		scroll = new JScrollPane(TableofDBContents);
		scroll.setSize(600, 300);
		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(1200,100));
	
		
		JPanel viewrequest = new JPanel();
		viewrequest.setLayout(new BorderLayout());
		viewrequest.setBorder(BorderFactory.createTitledBorder("View Return Date"));
		viewrequest.add(scroll);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		
		date = new JTextField(10);
		date2 = new JTextField(10);
		
		
	
		JPanel addP = new JPanel();
		addP.setLayout(new GridLayout(3,3));
		
		addP.add(dueBookDate);addP.add(date);
		addP.add(dueJournalDate);addP.add(date2);
		addP.add(dueDate2);addP.add(dueDate3);
		
		
		dueDate2.addActionListener(this);
		dueDate3.addActionListener(this);
		
		
		mainPanel.add(addP, BorderLayout.CENTER);
		
		viewrequest.add(mainPanel, BorderLayout.NORTH);
		this.add(viewrequest);
		
		
	}

	

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object target = arg0.getSource();
		
			if(target==dueDate2){
	 
			
			try {
				
				query = date.getText();
				
				if(date.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null,
						    "Field cannont be empty.");
				}
				else{
			
				FinesDAO.instance.dueBookDate(query);
				
				fineTable.refreshTableDB3(stmt,query);
				scroll.getViewport().revalidate();
				scroll.getViewport().repaint();
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block 
				e.printStackTrace();
			} catch (RuairiTestStringHan e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			}
			if(target==dueDate3){
				 
				
				try {
					
					query = date2.getText();
					
					if( date2.getText().equals(""))
					{
						JOptionPane.showMessageDialog(null,
							    "Field cannont be empty.");
					}
					
					else{
					FinesDAO.instance.dueJournalDate(query);
					
					fineTable.refreshTableDB4(stmt,query);
					scroll.getViewport().revalidate();
					scroll.getViewport().repaint();
					}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block 
					e.printStackTrace();
				} catch (RuairiTestStringHan e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				}
	
	}
	
	public void initiate_db_conn()
	{
		try
		{
			Connection con = Utils.getConnection();
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
}

