package DatabaseAccess;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import Tests.RuairiTestStringHan;

public enum FinalReturnNotifyDAO {
	instance;
	
	private FinalReturnNotifyDAO(){
		
	}
	
	// create a new final msg 
	public boolean sendNewFinalReturnMessage(int id,String fname,String lasname,String email,
									int bookid,String bookTitle,int journid,String journTitle,
									int timeoverdue,float amountdue){
		
		try {
			
			Connection conn = Utils.getConnection();
			
			Statement stmt = conn.createStatement();
			
			
			String sendnewFinalReturn = "Insert into finalReturnNotif values("+id + ",'"+fname+"','"
						+lasname+"','"+email+"',"+bookid+",'"+bookTitle+"',"+journid+",'"+journTitle+"',"
					 +timeoverdue+","+amountdue+";";
			
			stmt.executeUpdate(sendnewFinalReturn);
			
			

		} catch (SQLException sqle) {
			System.err.println("Error with  creating new final msg:\n" + sqle.toString());
			return false;
		}
		return true;
		
	}
	
}
