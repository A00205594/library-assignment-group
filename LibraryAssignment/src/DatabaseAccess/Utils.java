package DatabaseAccess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.stream.Stream;

public class Utils {
	final static String PATH_TO_DATABASE_CREDENTIALS = System.getProperty("user.home") + File.separatorChar
			+ "Documents" + File.separatorChar + "DBConnection.txt";
	final static String SEP = ":";

	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:";
			String[] connectionData = readDbData();
			url += connectionData[3] + "/";
			url += connectionData[2];
			conn = DriverManager.getConnection(url, connectionData[0], connectionData[1]);

		} catch (Exception e) {
			System.err.println("Failed to connect to database.");
			e.printStackTrace();
		}
		return conn;
	}
	public static String[] readDbData() {
		File props = new File(PATH_TO_DATABASE_CREDENTIALS);
		String username, password, dbName, dbPort;
		String readableData[] = new String[4];

		if (props.isFile() && props.canRead()) {
			try {
				Stream<String> lines = Files.lines(props.toPath());
				String[] arr = lines.toArray(String[]::new);
				username = arr[0].split(SEP)[1];
				password = arr[1].split(SEP)[1];
				dbName = arr[2].split(SEP)[1];
				dbPort = arr[3].split(SEP)[1];
				lines.close();
				username = username.replace("empty", "");
				password = password.replace("empty", "");
				readableData[0] = username;
				readableData[1] = password;
				readableData[2] = dbName;
				readableData[3] = dbPort;

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return readableData;
	}
	public static void showError(Object src,Exception e){
		int line = e.getStackTrace()[0].getLineNumber();
		String className = src.getClass().getName();
		String exception = e.getClass().getCanonicalName();
		System.err.println(exception + "Occured in: " + className + " on line number: " + line);
	}
}