package DatabaseAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Model.Book;
import Model.Journal;
import Tests.OliverExceptionHandler;
import Tests.RuairiTestStringHan;

public enum JournalDAO {
	instance;

	private JournalDAO() {

	}

	//************************************************************************************
		//

	
	public boolean dueDate(String dueDate) throws RuairiTestStringHan {
		try {
			Connection conn = Utils.getConnection();
			
			 Statement stmt = conn.createStatement();
			
			if (dueDate.length() > 10) {

				throw new RuairiTestStringHan("Sorry invalid entry");
			}
			
			String returnDate = "select "+dueDate+" from Fines";

			System.out.println(returnDate);
			stmt.executeQuery(returnDate);

		} catch (SQLException sqle) {
			System.err.println("Error with  insert:\n" + sqle.toString());
			return false;
		}
		return true;
	} 
	
	


	
	public boolean addValues(int journal_id, String title, String issn, String author, String genre, int year_published,String Reference_Number, int Stock_Number, String Description, String publisher)
			throws RuairiTestStringHan {
		try {
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			if (title.length() > 30) {

				throw new RuairiTestStringHan("Sorry this Title is too long");
			}

			
			
			if (issn.length() != 13) {

				throw new RuairiTestStringHan("Sorry this issn is invalid");
			}

			if (author.length() > 30) {

				throw new RuairiTestStringHan("Sorry this author is too long");
			}

			if (genre.length() > 13) {

				throw new RuairiTestStringHan("Sorry genre is too long");
			}

			if (year_published > 2018) {

				throw new RuairiTestStringHan("Sorry this is invalid year published");
			}

			if (Reference_Number.length() > 6) {

				throw new RuairiTestStringHan("out of bounds");
			}

			if (Stock_Number < 0) {

				throw new RuairiTestStringHan("invalid stock number");
			}

			if (Description.length() > 30) {  

				throw new RuairiTestStringHan("invalid Description");
			}

			if (publisher.length() > 30) {

				throw new RuairiTestStringHan("Sorry this publisher is too long"); 
			} 

			
			String addTitle = "Insert into Journal values('" + journal_id + "','" + title + "','" + issn + "','" + author
					+ "','" + genre + "','" + year_published + "','" + Reference_Number + "','" + Stock_Number + "','"
					+ Description + "','" + publisher + "');";

			System.out.println(addTitle);
			stmt.executeUpdate(addTitle);

		} catch (SQLException sqle) {
			System.err.println("Error with  insert:\n" + sqle.toString());
			return false;
		}
		return true;
	}

//************************************************************************************
//
//									Update Journals
//
		
		// 
		//Update Title
		//
		public boolean ModifyJournalTitle(int journal_id, String Title) 
				throws OliverExceptionHandler{
			try {
				
				if (Title.length() == 0) {
					throw new OliverExceptionHandler("Invalid title, must enter some text");
				}
				
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				 			
				String ModifyJournalTitle = "UPDATE Journal SET Title= '" + Title + "' WHERE journal_id='"
						+ journal_id + "';";

				stmt.executeUpdate(ModifyJournalTitle);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}

		// 
		//Update ISSN
		//
		public boolean ModifyJournalISSN(int journal_id, String ISSN) 
				throws OliverExceptionHandler{
			try {
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				 					
				if ((ISSN.length() < 8) || (ISSN.length() > 8)) 
				{
					throw new OliverExceptionHandler("Invalid Name, must have 8 characters");
				}
				
				String ModifyJournalISSN = "UPDATE Journal SET ISSN= '" + ISSN + "' WHERE journal_id='"
						+ journal_id + "';";
				
				stmt.executeUpdate(ModifyJournalISSN);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}
		
		
		// 
		//Update Author
		//
		public boolean ModifyJournalAuthor(int journal_id, String Author) 
				throws OliverExceptionHandler{
			try {
				
				if (Author.length() == 0) {
					throw new OliverExceptionHandler("Invalid author, must enter some text");
				}
				
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				
				String ModifyJournalAuthor = "UPDATE Journal SET Author= '" + Author + "' WHERE journal_id='" + journal_id
						+ "';";

				stmt.executeUpdate(ModifyJournalAuthor);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}
		
		
		// 
		//Update Genre
		//
		public boolean ModifyJournalGenre(int journal_id, String Genre) 
				throws OliverExceptionHandler{
			try {
				
				if (Genre.length() == 0) {
					throw new OliverExceptionHandler("Invalid genre, must enter some text");
				}
				
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				
				String ModifyJournalGenre = "UPDATE Journal SET Genre= '" + Genre + "' WHERE journal_id='" + journal_id + "';";

				stmt.executeUpdate(ModifyJournalGenre);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}
		
		// 
		//Update Year_Published
		//
		public boolean ModifyJournalYear_Published(int journal_id, int year_published) 
				throws OliverExceptionHandler{
			try {
				
				if (year_published > 2017) {
					throw new OliverExceptionHandler("Invalid year, must be 2017 or below");
				}

				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();			
				String ModifyJournalYear_Published = "UPDATE Journal SET Year_Published= '" + year_published + "' WHERE journal_id='" + journal_id + "';";

				stmt.executeUpdate(ModifyJournalYear_Published);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}
		
		// 
		//Update Reference_Number
		//
		public boolean ModifyJournalReference_Number(int journal_id, String Reference_Number) 
				throws OliverExceptionHandler{
			try {
				
				if (Reference_Number.length() > 6 || Reference_Number.length() < 6) {
					throw new OliverExceptionHandler("Invalid reference number, must be 6 characters in length");
				}
				
				Connection conn = Utils.getConnection();
				Statement stmt = conn.createStatement();
				
				String ModifyJournalReference_Number = "UPDATE Journal SET Reference_Number= '" + Reference_Number + "' WHERE journal_id='" + journal_id + "';";

				stmt.executeUpdate(ModifyJournalReference_Number);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}
		
		// 
		//Update Stock_Number
		//
		public boolean ModifyJournalStock_Number(int journal_id, int Stock_Number) 
				throws OliverExceptionHandler{
			try {
				
				if (Stock_Number < 0) {
					throw new OliverExceptionHandler("Invalid stock number");
				}
				
				Connection conn = Utils.getConnection();
				Statement stmt = conn.createStatement();					
				
				String ModifyJournalStock_Number = "UPDATE Journal SET Stock_Number= '" + Stock_Number + "' WHERE journal_id='" + journal_id + "';";

				stmt.executeUpdate(ModifyJournalStock_Number);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}
		
		
		// 
		//Update Description
		//
		public boolean ModifyDescription(int journal_id, String Description) 
				throws OliverExceptionHandler{
			try {
				
				if (Description.length() == 0) {
					throw new OliverExceptionHandler("You've entered a blank description, please enter some text");
				}
				
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				
				String ModifyDescription = "UPDATE Journal SET Description= '" + Description + "' WHERE journal_id='" + journal_id + "';";

				stmt.executeUpdate(ModifyDescription);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}
		
		
		// 
		//Update Publisher
		//
		public boolean ModifyPublisher(int journal_id, String Publisher) 
				throws OliverExceptionHandler{
			try {
				
				if (Publisher.length() == 0) {
					throw new OliverExceptionHandler("You've entry for publisher is blank, please enter some text");
				}
				
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				
				String ModifyPublisher = "UPDATE Journal SET Publisher= '" + Publisher + "' WHERE journal_id='" + journal_id + "';";

				stmt.executeUpdate(ModifyPublisher);

			} catch (SQLException sqle) {
				return false;
			}
			return true;
		}

		
//End of Update Journals*****************************************************************

			

//****************************************************************************************
//
//											Remove Journals
//
		
		// 
		//Remove Journal
		//
		public boolean Remove_Journal(String title) throws OliverExceptionHandler {
			try {
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				
				String Remove_Journal = "DELETE FROM Journal WHERE title ='" + title+ "';";
				stmt.executeUpdate(Remove_Journal);
				
			} catch (SQLException sqle) {
				return false;
			}
					
			return true;
		}
		
		public boolean Remove_Journal2(String issn) throws OliverExceptionHandler {
			try {
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				
				String Remove_Journal = "DELETE FROM Journal WHERE ISSN ='" + issn+ "';";
				stmt.executeUpdate(Remove_Journal);
				
			} catch (SQLException sqle) {
				return false;
			}
					
			return true;
		}
		
		public boolean Remove_Journal3(String Reference_Number) throws OliverExceptionHandler {
			try {
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				 			
			
				String Remove_Journal = "DELETE FROM Journal WHERE Reference_Number = '" + Reference_Number+ "';";
				stmt.executeUpdate(Remove_Journal);
				
			} catch (SQLException sqle) {
				return false;
			}
					
			return true;
		}
			
//End of Remove Journals*****************************************************************

				
			
			
//Add method for request Journal
			
			public boolean AddJournalRequest(int Journal_id, String title, String ISSN, String Author, String Genre,String Year_Published, String Reference_Number, String Description, String publisher, String email) throws OliverExceptionHandler{
				try {

					Connection conn = Utils.getConnection();
					
					Statement stmt = conn.createStatement();
				
					String Request_Journal = "Insert into Requests values(null, '" + Journal_id + "','" + title + "','" + ISSN + "','" + Author + "','"
							+ Genre + "','" + Year_Published + "','" + Reference_Number + "','"
							+ Description + "','" + publisher + "', 'Journal','"+email+"');";
					System.out.println(Request_Journal);
					stmt.executeUpdate(Request_Journal);
					
				} catch (SQLException sqle) {
					System.err.println("Error with  insert:\n" + sqle.toString());
					return false;
				}
				return true;
			}
			public Journal GetRequestedJournal(int Journal_id) throws OliverExceptionHandler {
				Journal res = new Journal();
				ResultSet rs = null;
				String getJournal = "SELECT * FROM Journal WHERE journal_id = " + Journal_id + ";";
				System.out.println("id: "+Journal_id);
				if(Journal_id==0){
					return null;
				}
				try {
					
					Connection conn = Utils.getConnection();
					Statement stmt = conn.createStatement();
					 rs =stmt.executeQuery(getJournal);
					
					 while (rs.next()) {
							String title = rs.getString("title");
							String author = rs.getString("Author");
							String desc = rs.getString("Description");
							int stock = rs.getInt("Stock_Number");
							String ISBN = rs.getString("ISSN");
							String libraryRef = rs.getString("Reference_Number");
							String publisher = rs.getString("Publisher");
							String genres = rs.getString("Genre");
							String releaseDate = rs.getString("Year_Published");
							res = new Journal(title, author, desc, stock, ISBN, libraryRef, publisher, genres, releaseDate);
						}
					 if(res.getAuthor()==""){
						 return null;
					 }

				} catch (SQLException sqle) {
					System.err.println("Error with retrieving:\n" + sqle.toString());
					return null;
				}

				return res;
			}
			
			
			
	public ArrayList<Journal> getAll(){
		ArrayList<Journal> res = new ArrayList<Journal>();
		try {
			Connection conn = Utils.getConnection();
			Statement stmt = conn.createStatement();
			String query = "select * from Journal";
			ResultSet resSet = stmt.executeQuery(query);
			while (resSet.next()) {
				int id = resSet.getInt("journal_id");
				String title = resSet.getString("title");
				String author = resSet.getString("Author");
				String desc = resSet.getString("Description");
				int stock = resSet.getInt("Stock_Number");
				String ISSN = resSet.getString("ISSN");
				String libraryRef = resSet.getString("Reference_Number");
				String publisher = resSet.getString("Publisher");
				String genres = resSet.getString("Genre");
				String releaseDate = resSet.getString("Year_Published");
				Journal temp = new Journal(id,title, author, desc, stock, ISSN, libraryRef, publisher, genres, releaseDate);
				res.add(temp);
			}
			conn.close();
			conn = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
}
