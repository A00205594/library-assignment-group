package DatabaseAccess;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import java.sql.Connection;
import java.sql.Statement;

@SuppressWarnings("serial")
public class DbTable extends AbstractTableModel{
	
	private static final long serialVersionUID = 1L;
	Vector<String[]> dbData; //will hold String[] objects
	int colCount;
	String[] headers=new String[0] ;
	Statement stmt = null;
	Statement stmt2 = null;
	String[] record;
	ResultSet rs = null;
	
	public DbTable() {
		dbData=new Vector<String[]>();
	}
	public String getColumnName(int i){
		return headers[i];
	}	

	@Override
	public int getColumnCount() {
		return colCount;
	}

	@Override
	public int getRowCount() {
		return dbData.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		return ((String[])dbData.elementAt(row))[col];
	}
	public void refreshTableDB( Statement stmt1, String table )
	{
		dbData = new Vector<String[]>();
		stmt = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("Select * from "+table);
			ResultSetMetaData meta = rs.getMetaData();
		//System.out.println("here");
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop
				dbData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
					System.out.println("Error with refreshTableDB Method\n"+e.toString());
		} // end catch clause to query table
	}//end refreshTableDB method
	
	public void refreshTableDB2( Statement stmt1, String query )
	{
		dbData = new Vector<String[]>();
		stmt2 = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt2.executeQuery(query);
			ResultSetMetaData meta = rs.getMetaData();
		//System.out.println("here");
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop
				dbData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
			e.printStackTrace();
					System.out.println("Error with refreshTableDB2 Method\n"+e.toString());
		} // end catch clause to query table
	}//end refreshTableDB method
	
	public void refreshTableDB4( Statement stmt1, String journal )
	{
		dbData = new Vector<String[]>();
		stmt = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("select journal,ReturnDate from Fines where journal = '"+journal+"';");
			ResultSetMetaData meta = rs.getMetaData();
		//System.out.println("here");
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop 
				dbData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
					System.out.println("Error with refreshTableDB Method\n"+e.toString());
		} // end catch clause to query table
	}//end refreshTableDB method
	
	public void refreshTableDB3( Statement stmt1, String book )
	{
		dbData = new Vector<String[]>();
		stmt = stmt1;
		try{
			//Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("select book,ReturnDate from Fines where book = '"+book+"';");
			ResultSetMetaData meta = rs.getMetaData();
		//System.out.println("here");
			//to get the number of columns
			colCount = meta.getColumnCount(); 
			// Now must rebuild the headers array with the new column names
			headers = new String[colCount];
	
			for(int h = 0; h<colCount; h++)
			{
				headers[h] = meta.getColumnName(h+1);
			}//end for loop
		
			// fill the cache with the records from the query, ie get all the rows
		
			while(rs.next())
			{
				record = new String[colCount];
				for(int i = 0; i < colCount; i++)
				{
					record[i] = rs.getString(i+1);
				}//end for loop
				dbData.addElement(record);
			}// end while loop
			fireTableChanged(null); //Tell the listeners a new table has arrived.
		}//end try clause
		catch(Exception e) {
					System.out.println("Error with refreshTableDB Method\n"+e.toString());
		} // end catch clause to query table
	}//end refreshTableDB method
	
	public void initiate_db_conn()
	{
		try
		{
			Connection con = Utils.getConnection();
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
}
