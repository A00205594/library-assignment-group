package DatabaseAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Model.User;
import PageController.PageLoader;
import Tests.OliverExceptionHandler;

public enum UserDAO {
	instance;
	public Connection con = null;
	public Statement stmt = null;
	
	public User user;
	
//add admin user------------------------------------------------------------------------------------------------
	public boolean addUserAdmin(User user)
	throws OliverExceptionHandler{
		try{
			if((user.getfName()=="") || (user.getLName()=="") || (!(user.getEmail()).contains("@")) || ((user.getPhoneNo()).length()!=10) || ((user.getPassword()).length()<=5)){
				throw new OliverExceptionHandler("Invalid data entered");
			}
			con = Utils.getConnection();
			String addUser = "INSERT INTO users VALUES('"+user.getfName()+"','"+
					user.getLName()+"','"+user.getEmail()+"',"+Integer.parseInt(user.getPhoneNo())+",'','"+user.getPassword()+"','Admin');";
			stmt = con.createStatement();
			stmt.executeUpdate(addUser);
			
		}catch(SQLException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
//add regular user-------------------------------------------------------------------------------------
	public boolean addUserReg(User user)
			throws OliverExceptionHandler{
				try{

					if((user.getfName()=="") || (user.getLName()=="") || (!(user.getEmail()).contains("@")) || ((user.getPhoneNo()).length()!=10) || ((user.getPassword()).length()<=5)){
						throw new OliverExceptionHandler("Invalid data entered");
					}
					con = Utils.getConnection();
					String addUser = "INSERT INTO users VALUES('"+user.getfName()+"','"+
							user.getLName()+"','"+user.getEmail()+"',"+Integer.parseInt(user.getPhoneNo())+",'"+ user.getStudentNum()+"','"+user.getPassword()+"','Student');";
					stmt = con.createStatement();
					stmt.executeUpdate(addUser);
				}catch(SQLException e){
					e.printStackTrace();
					return false;
				}
				return true;
			}
//Login user-------------------------------------------------------------------------------------
	public User LoginUser(String email, String password)
			throws OliverExceptionHandler{
				try{
					if(email!=null&&password!=null){
					con = Utils.getConnection();
					String CheckUser = "Select * from users WHERE email_Address = '"+email+"' and Pass = '"+password+"';";
					stmt = con.createStatement();
					ResultSet rs = stmt.executeQuery(CheckUser);
						if(rs.next()){
							user = new User(rs.getString("first_name"),rs.getString("Last_Name"),rs.getString("email_Address"),rs.getString("Phone_Num"),rs.getString("Pass"));
							if(rs.getString("user_Type").equals("Admin")){
								PageLoader.userAdmin = true;
							}
							else{
								PageLoader.userAdmin = false;
							}
						}
					}
				}catch(SQLException e){
					
				}
				return user;
			}
//Logout user-----------------------------------------------------------------------------
	public User Logout(){
		user = null;
		return user;
	}	
//update credentials---------------------------------------------------------
	public boolean UpdateCredentials(String fname, String lname, String studNo, String phno)throws OliverExceptionHandler{
		try{
			if(user!=null){
				con = Utils.getConnection();
				String updateUser = "UPDATE users SET "+"first_name = '"+fname+
						"', Last_name = '"+lname+
						"', Phone_Num = '"+studNo+
						"', Student_num = '"+phno+
						"' where email_Address = '"+user.getEmail()+"';";
				stmt = con.createStatement();
				stmt.executeUpdate(updateUser);
			}
		}catch(SQLException e){
			return true;
		}
		return false;
	}
	
	public boolean UpdateCredentialsEmailPass(String email, String pass)throws OliverExceptionHandler{
		try{
			if(user!=null){
				if((!(email.contains("@"))) || (pass.length()<=5)){
					throw new OliverExceptionHandler("Invalid data entered");
				}
				con = Utils.getConnection();
				String updateUser = "UPDATE users SET "+
						"email_Address = '"+email+
						"', Pass = '"+pass+
						"' where first_name = '"+user.getfName()+"';";
				stmt = con.createStatement();
				stmt.executeUpdate(updateUser);
			}
		}catch(SQLException e){
			return false;
		}
		return true;
		}
	
	
	
	public User GetRequestedUser(String fname) throws OliverExceptionHandler {
		User res = new User();
		ResultSet rs = null;
		String getUser = "SELECT * FROM users WHERE first_name  = '" + fname + "';";
		
		if(fname=="")
			return null;
		try {
			
			Connection conn = Utils.getConnection();
			
			Statement stmt = conn.createStatement();
			rs =stmt.executeQuery(getUser);
			
			 while (rs.next()) {
					String fnam = rs.getString("first_name");
					String lasnam = rs.getString("Last_Name");
					String emadd = rs.getString("email_Address");
					res = new User(fnam,lasnam,emadd);
				}
			 
			 if (res.getfName()=="")
				 return null;
			 
			 if (res.getLName()=="")
				 return null;
			 
			}
			
			catch (SQLException sqle) {
				System.err.println("Error with retrieving:\n" + sqle.toString());
				return null;
		}

		return res;
	}
}
