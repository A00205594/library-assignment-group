package DatabaseAccess;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import Tests.RuairiTestStringHan;

public enum FinesDAO {
	
	instance;
	
	
	public boolean addFines(double fines, String email) throws RuairiTestStringHan {
		try {

			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();
			
			 if(email.length() > 40)
			 {
				 throw new RuairiTestStringHan("Sorry invalid entry");
			 } 

			String addBookFines = "update Fines set LateFines = (LateFines + "+fines+"), Email = '"+email+"' , LateFineDate = CURDATE()  where ReturnDate < CURDATE();";
			System.out.println(addBookFines);
			stmt.executeUpdate(addBookFines);

		} catch (SQLException sqle) {
			System.err.println("Error with  insert:\n" + sqle.toString());
			return false;
		}
		return true;
	}
	
	// View return date

		public boolean dueBookDate(String book) throws RuairiTestStringHan {
			try {
				Connection conn = Utils.getConnection();

				Statement stmt = conn.createStatement();

				if (book.length() > 10) {

					throw new RuairiTestStringHan("Sorry invalid entry");
				}

				//"select "+book+", Journal,ReturnDate from Fines;";
				String returnDate = "select book,ReturnDate from Fines where book = '"+book+"';";

				System.out.println(returnDate);
				stmt.executeQuery(returnDate);

			} catch (SQLException sqle) {
				System.err.println("Error with  insert:\n" + sqle.toString());
				return false;
			}
			return true;
		}
	//
		public boolean dueJournalDate(String journal) throws RuairiTestStringHan {
			try {
				Connection conn = Utils.getConnection();

				Statement stmt = conn.createStatement();

				if (journal.length() > 10) {

					throw new RuairiTestStringHan("Sorry invalid entry");
				}

				//"select "+book+", Journal,ReturnDate from Fines;";
				String returnDate = "select journal,ReturnDate from Fines where journal = '"+journal+"';";
				//select journal,ReturnDate from Fines where journal = the codex;

				System.out.println(returnDate);
				stmt.executeQuery(returnDate);

			} catch (SQLException sqle) {
				System.err.println("Error with  insert:\n" + sqle.toString());
				return false;
			}
			return true;
		}

}
