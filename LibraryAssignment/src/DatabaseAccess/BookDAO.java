package DatabaseAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Model.Book;
import Tests.OliverExceptionHandler;
import Tests.RuairiTestStringHan;

public enum BookDAO {
	instance;

	private BookDAO() {

	}

	public boolean dueDate(String dueDate) throws RuairiTestStringHan {
		try {
			Connection conn = Utils.getConnection();
			
			 Statement stmt = conn.createStatement();
			
			if (dueDate.length() > 10) {

				throw new RuairiTestStringHan("Sorry invalid entry");
			}
			
			String returnDate = "select "+dueDate+" from Fines";

			System.out.println(returnDate);
			stmt.executeQuery(returnDate);

		} catch (SQLException sqle) {
			System.err.println("Error with  insert:\n" + sqle.toString());
			return false;
		}
		return true;
	} 
	

	

	public boolean addValuesBook(int book_id, String title, String ISBN, String Author, String Genre,int Year_Published, String Reference_Number, int Stock_Number, String Description, String publisher)
			throws RuairiTestStringHan {

		if (title.length() > 30) {

			throw new RuairiTestStringHan("Sorry this Title is too long");
		}

		if (ISBN.length() != 13) { 

			throw new RuairiTestStringHan("Sorry this ISBN is too long");
		}

		if (Author.length() > 30) {

			throw new RuairiTestStringHan("Sorry this Author is too long");
		}

		if (Genre.length() > 20) {

			throw new RuairiTestStringHan("Sorry Genre too long");
		}

		if (Year_Published > 2019) {

			throw new RuairiTestStringHan("Sorry this year is invalid");
		}

		if (Reference_Number.length() > 15) {

			throw new RuairiTestStringHan("Sorry this Reference Number is too long");
		}

		if (Stock_Number < 0) {

			throw new RuairiTestStringHan("Stock number is out of bounds");
		}

		if (Description.length() > 30) {

			throw new RuairiTestStringHan("Sorry Description is too long");
		}

		if (publisher.length() > 30) {

			throw new RuairiTestStringHan("Sorry publisher is too long");
		}

		try {
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String addBook = "Insert into Book values('" + book_id + "','" + title + "','" + ISBN + "','" + Author + "','"
					+ Genre + "','" + Year_Published + "','" + Reference_Number + "','" + Stock_Number + "','"
					+ Description + "','" + publisher + "');";

			System.out.println(addBook);
			stmt.executeUpdate(addBook);

		} catch (SQLException sqle) {
			System.err.println("Error with  insert:\n" + sqle.toString());
			return false;
		}
		return true;
	}

// ****************************************************************************************
//
// Update Books
//

	//
	// Update Title
	//
	public boolean ModifyTitle(int book_id, String Title) throws OliverExceptionHandler {
		try {
			
			if (Title.length() == 0) {
				throw new OliverExceptionHandler("Invalid title, must enter some text");
			}
			
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String ModifyTitle = "UPDATE Book SET Title= '" + Title + "' WHERE book_id='" + book_id + "';";

			stmt.executeUpdate(ModifyTitle);

		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	//
	// Update ISBN
	//
	public boolean ModifyISBN(int book_id, String ISBN) throws OliverExceptionHandler {
		try {
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			if ((ISBN.length() < 13) || (ISBN.length() > 13)) {
				throw new OliverExceptionHandler("Invalid Name, must have 13 characters");
			}

			String ModifyISBN = "UPDATE Book SET ISBN= '" + ISBN + "' WHERE book_id='" + book_id + "';";

			stmt.executeUpdate(ModifyISBN);

		} catch (SQLException sqle) {
			return false;
		}

		return true;
	}

	//
	// Update Author
	//
	public boolean ModifyAuthor(int book_id, String Author) throws OliverExceptionHandler {
		try {
			
			if (Author.length() == 0) {
				throw new OliverExceptionHandler("Invalid author, must enter some text");
			}
			
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String ModifyAuthor = "UPDATE Book SET Author= '" + Author + "' WHERE book_id='" + book_id + "';";

			stmt.executeUpdate(ModifyAuthor);

		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	//
	// Update Genre
	//
	public boolean ModifyGenre(int book_id, String Genre) throws OliverExceptionHandler {
		try {
			
			if (Genre.length() == 0) {
				throw new OliverExceptionHandler("Invalid genre, must enter some text");
			}
			
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String ModifyGenre = "UPDATE Book SET Genre= '" + Genre + "' WHERE book_id='" + book_id + "';";

			stmt.executeUpdate(ModifyGenre);

		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	//
	// Update Year_Published
	//
	public boolean ModifyYear_Published(int book_id, int year_published) throws OliverExceptionHandler {
		try {
			
			if (year_published > 2017) {
				throw new OliverExceptionHandler("Invalid year, must be 2017 or below");
			}

			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String ModifyYear_Published = "UPDATE Book SET Year_Published= '" + year_published + "' WHERE book_id='"
					+ book_id + "';";

			stmt.executeUpdate(ModifyYear_Published);

		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	//
	// Update Reference_Number
	//
	public boolean ModifyReference_Number(int book_id, String Reference_Number) throws OliverExceptionHandler {
		try {
			
			if (Reference_Number.length() > 6 || Reference_Number.length() < 6) {
				throw new OliverExceptionHandler("Invalid reference number, must be 6 characters in length");
			}
			
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String ModifyReference_Number = "UPDATE Book SET Reference_Number= '" + Reference_Number
					+ "' WHERE book_id='" + book_id + "';";

			stmt.executeUpdate(ModifyReference_Number);

		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	//
	// Update Stock_Number
	//
	public boolean ModifyStock_Number(int book_id, int stock_Number) throws OliverExceptionHandler {
		try {
			
			if (stock_Number < 0) {
				throw new OliverExceptionHandler("Invalid stock number");
			}
			
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String ModifyStock_Number = "UPDATE Book SET Stock_Number= '" + stock_Number + "' WHERE book_id='" + book_id
					+ "';";

			stmt.executeUpdate(ModifyStock_Number);

		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	//
	// Update Description
	//
	public boolean ModifyDescription(int book_id, String Description) throws OliverExceptionHandler {
		try {
			
			if (Description.length() == 0) {
				throw new OliverExceptionHandler("You've entered a blank description, please enter some text");
			}
			
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String ModifyDescription = "UPDATE Book SET Description= '" + Description + "' WHERE book_id='" + book_id
					+ "';";

			stmt.executeUpdate(ModifyDescription);

		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	//
	// Update Publisher
	//
	public boolean ModifyPublisher(int book_id, String Publisher) throws OliverExceptionHandler {
		try {
			
			if (Publisher.length() == 0) {
				throw new OliverExceptionHandler("You've entry for publisher is blank, please enter some text");
			}
			
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String ModifyPublisher = "UPDATE Book SET Publisher= '" + Publisher + "' WHERE book_id='" + book_id + "';";

			stmt.executeUpdate(ModifyPublisher);

		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	// End of Update
	// Books*****************************************************************

//****************************************************************************************
//
//										Remove Books
//
			
		// 
		//Remove Book
		//
		public boolean Remove_Book(String title) throws OliverExceptionHandler {
			try {
				
				
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				
				String Remove_Book = "DELETE FROM Book WHERE title ='" + title + "';";
				stmt.executeUpdate(Remove_Book);
				
			} catch (SQLException sqle) {
				return false;
			}
					
			return true;
		}
		
		public boolean Remove_Book2(String isbn) throws OliverExceptionHandler {
			try {
				
				
				
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				
				String Remove_Book = "DELETE FROM Book WHERE ISBN ='" + isbn+ "';";
				stmt.executeUpdate(Remove_Book);
				
			} catch (SQLException sqle) {
				return false;
			}
					
			return true;
		}
		
		public boolean Remove_Book3(String Reference_Number) throws OliverExceptionHandler {
			try {
			
				
				Connection conn = Utils.getConnection();
				
				Statement stmt = conn.createStatement();
				 			
				String Remove_Book = "DELETE FROM Book WHERE Reference_Number = '" + Reference_Number+ "';";
				stmt.executeUpdate(Remove_Book);
				
			} catch (SQLException sqle) {
				return false;
			}
					
			return true;
		}
			
//End of Remove Books*****************************************************************


	// Add method for request book

	public boolean AddBookRequest(int book_id, String title, String ISBN, String Author, String Genre,String Year_Published, String Reference_Number, String Description, String publisher, String email)
			throws OliverExceptionHandler {
		try {
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();

			String Request_Book = "Insert into Requests values(null, '" + book_id + "','" + title + "','" + ISBN + "','" + Author + "','"
					+ Genre + "','" + Year_Published + "','" + Reference_Number + "','"
					+ Description + "','" + publisher + "', 'Book', '"+email+"');";
			System.out.println(Request_Book);
			stmt.executeUpdate(Request_Book);

		} catch (SQLException sqle) {
			System.err.println("Error with  insert:\n" + sqle.toString());
			return false;
		}
		return true;
	}
	public Book GetRequestedBook(int book_id) throws OliverExceptionHandler {
		Book res = new Book();
		ResultSet rs = null;
		String getBook = "SELECT * FROM Book WHERE book_id = " + book_id + ";";
		System.out.println("id: "+book_id);
		if(book_id==0){
			return null;
		}
		try {
			
			Connection conn = Utils.getConnection();

			Statement stmt = conn.createStatement();
			 rs =stmt.executeQuery(getBook);
			
			 while (rs.next()) 
			 {
				 	int ID = rs.getInt("book_id");
					String title = rs.getString("title");
					String author = rs.getString("Author");
					String desc = rs.getString("Description");
					int stock = rs.getInt("Stock_Number");
					String ISBN = rs.getString("ISBN");
					String libraryRef = rs.getString("Reference_Number");
					String publisher = rs.getString("Publisher");
					String genres = rs.getString("Genre");
					String releaseDate = rs.getString("Year_Published");
					res = new Book(ID, title, author, desc, stock, ISBN, libraryRef, publisher, genres, releaseDate);
				}
			 if(res.getAuthor()==""){
				 return null;
			 }

		} catch (SQLException sqle) {
			System.err.println("Error with retrieving:\n" + sqle.toString());
			return null;
		}

		return res;
	}

	
	public ArrayList<Book> getAll() {
		ArrayList<Book> res = new ArrayList<Book>();
		try {
			Connection conn = Utils.getConnection();
			Statement stmt = conn.createStatement();
			String query = "select * from Book";
			ResultSet resSet = stmt.executeQuery(query);
			while (resSet.next()) {
				int id = resSet.getInt("book_id");
				String title = resSet.getString("title");
				String author = resSet.getString("Author");
				String desc = resSet.getString("Description");
				int stock = resSet.getInt("Stock_Number");
				String ISBN = resSet.getString("ISBN");
				String libraryRef = resSet.getString("Reference_Number");
				String publisher = resSet.getString("Publisher");
				String genres = resSet.getString("Genre");
				String releaseDate = resSet.getString("Year_Published");
				Book temp = new Book(id,title, author, desc, stock, ISBN, libraryRef, publisher, genres, releaseDate);
				res.add(temp);
			}
			conn.close();
			conn = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
}
