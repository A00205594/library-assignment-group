package Tests;

import DatabaseAccess.BookDAO;
import DatabaseAccess.JournalDAO;
import junit.framework.TestCase;

public class UpdateJournalTests extends TestCase {

	
//*****************************************************************
//
//							Update Journal Tests
//

		
		// Test No: 1                                                             
		// Object: Check if journal title can be Updated                    
		// Inputs: Title = Journal around the World                                              
		// Expected Output: True                                                    
		// Author: Patrick McLoughlin                                                                                                                                              
		// ****************************************************************************
		public void testModifyJournalTitle001() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyJournalTitle(1, "Journal around the World"));
			} catch (Exception E) {
			}
		}
		
		
		// Test Number: 2
		// Test Objective: Check that exception is thrown when 0 characters are entered
		// Input(s): Title = ""
		// Expected Output(s): Exception "Invalid title, must enter some text"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void ModifyJournalTitle002() {
				
			try {
				JournalDAO.instance.ModifyJournalTitle(3,"");
				fail("Should not get here .. Exception Expected");
			}
			
			catch (OliverExceptionHandler e) {
				assertEquals("Invalid title, must enter some text", e.getMessage());
			}
		}
		

		// Test No: 3                                                               
		// Object: Check if ISSN can be updated                   
		// Inputs: ISSN = 80808o8o                                          
		// Expected Output: True                                                    
		// Author: Patrick McLoughlin                                                                                                                                             
		// ****************************************************************************
		public void testModifyISSN003() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyJournalISSN(1, "80808o8o "));
			} catch (Exception E) {
			}
		}
		
		
		// Test No: 4                                                              
		// Object: Check author's name can be updated                  
		// Inputs: author = Papy Nevin-McLoughlin                                                
		// Expected Output: True                                                    
		// Author: Patrick McLoughlin                                                                                                                                             
		// ****************************************************************************
		public void testModifyJournalAuthor004() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyJournalAuthor(1, "Papy Nevin"));
			} catch (Exception E) {
			}
		}
		
		
		// Test Number: 5
		// Test Objective: Check that exception is thrown when 0 characters are entered
		// Input(s): Author = ""
		// Expected Output(s): Exception "Invalid author, must enter some text"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void ModifyJournalAuthor005() {
				
			try {
				JournalDAO.instance.ModifyJournalAuthor(3,"");
				fail("Should not get here .. Exception Expected");
			}
			
			catch (OliverExceptionHandler e) {
				assertEquals("Invalid author, must enter some text", e.getMessage());
			}
		}
		
		
		//  Test No: 6                                                               
		// 	Object: Check if Genre can be Updated                         
		// 	Inputs: Genre = Romance                                                     
		//	Expected Output: True                                                    
		// Author: Patrick McLoughlin                                                                                                                                          
		// ****************************************************************************
		public void testModifyJournalGenre006() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyJournalGenre(1, "Romance"));
			} catch (Exception E) {
			}
		}
		
		
		// Test Number: 7
		// Test Objective: Check that exception is thrown when 0 characters are entered
		// Input(s): Genre = ""
		// Expected Output(s): Exception "Invalid genre, must enter some text"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void ModifyJournalGenre007() {
				
			try {
				JournalDAO.instance.ModifyJournalGenre(3,"");
				fail("Should not get here .. Exception Expected");
			}
			
			catch (OliverExceptionHandler e) {
				assertEquals("Invalid genre, must enter some text", e.getMessage());
			}
		}
		
		
		//	Test No: 8                                                              
		// 	Object: Check if Year_Published can be Updated                         
		// 	Inputs: Year_Published = 2017                                                    
		//	Expected Output: True                                                    
		// Author: Patrick McLoughlin                                                                                                                                               
		// ****************************************************************************
		public void testModifyJournalYear_Published008() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyJournalYear_Published(1, 2017));
			} catch (Exception E) {
			}
		}
		
		// Test Number: 9
		// Test Objective: Check that exception is when invalid year entered
		// Input(s): Year_Published = 2018
		// Expected Output(s): Exception "Invalid year, must be 2017 or below"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testModifyJournalYear_Published009() {
				
				try {
					JournalDAO.instance.ModifyJournalYear_Published(2,2018);
					fail("Should not get here .. Exception Expected");
				}
				
				catch (OliverExceptionHandler e) {
					assertEquals("Invalid year, must be 2017 or below", e.getMessage());
				}
		}
		
			
		// 	Test No: 10                                                          
		// 	Object: Check if Reference_Number can be Updated                         
		// 	Inputs: Reference_Number = Y22278                                                     
		// 	Expected Output: True                                                    
		// Author: Patrick McLoughlin                                                                                                                                              
		// ****************************************************************************
		public void testJournalReference_Number0010() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyJournalReference_Number(1, "Y22278"));
			} catch (Exception E) {
			}
		}
		
		
		// Test Number: 11
		// Test Objective: Check that exception is thrown when 5 characters are entered
		// Input(s): Reference_Number = "E1234"
		// Expected Output(s): Exception "Invalid reference number, must be 6 characters in length"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testJournalReference_Number0011() {
				
				try {
					JournalDAO.instance.ModifyJournalReference_Number(3,"E1234");
					fail("Should not get here .. Exception Expected");
				}
				
				catch (OliverExceptionHandler e) {
					assertEquals("Invalid reference number, must be 6 characters in length", e.getMessage());
				}
		}
		
		
		// Test Number: 12
		// Test Objective: Check that exception is thrown when 5 characters are entered
		// Input(s): Reference_Number = "E123456"
		// Expected Output(s): Exception "Invalid reference number, must be 6 characters in length"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testJournalReference_Number0012() {
				
				try {
					JournalDAO.instance.ModifyJournalReference_Number(3,"E123456");
					fail("Should not get here .. Exception Expected");
				}
				
				catch (OliverExceptionHandler e) {
					assertEquals("Invalid reference number, must be 6 characters in length", e.getMessage());
				}
		}
		
		
		//	Test No: 13                                                       
		// 	Object: Check if Stock_Number can be Updated                         
		// 	Inputs: Stock_Number = 7                                                  
		// 	Expected Output: True                                                    
		// Author: Patrick McLoughlin                                                                                                                                               
		// ****************************************************************************
		public void testModifyStock_Number0013() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyJournalStock_Number(2, 7));
			} catch (Exception E) {
			}
		}
		
		
		// Test Number: 14
		// Test Objective: Check that exception is thrown when a minus number is entered
		// Input(s): Stock_Number = -1
		// Expected Output(s): Exception "Invalid stock number"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testStock_NumberModify0014() {
						
			try {
				JournalDAO.instance.ModifyJournalStock_Number(2,-1);
				fail("Should not get here .. Exception Expected");
			}
			
			catch (OliverExceptionHandler e) {
				assertEquals("Invalid stock number", e.getMessage());
			}
		}
		
		
		//	Test No: 15                                                    
		// 	Object: Check if Description can be Updated                         
		// 	Inputs: Description = Hercules has taking up knitting and part-time pilates                                                 
		// 	Expected Output: True                                                    
		//  Author: Patrick McLoughlin                                                                                                                                           
		// ****************************************************************************
		public void testModifyDescription0015() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyDescription
						(4, "Hercules has taking up knitting and part-time pilates"));
			} catch (Exception E) {
			}
		}
		
		
		// Test Number: 16
		// Test Objective: Check that exception is thrown if no text entered
		// Input(s): Description = ""
		// Expected Output(s): Exception "You've entered a blank description, please enter some text"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testModifyDescription0016() {
				
				try {
					JournalDAO.instance.ModifyDescription(2,"");
					fail("Should not get here .. Exception Expected");
				}
				
				catch (OliverExceptionHandler e) {
					assertEquals("You've entered a blank description, please enter some text", e.getMessage());
				}
		}
				
		
		// Test Number: 17
		// Test Objective: Check that exception is thrown when 7 characters are entered
		// Input(s): ISSN = 1234567
		// Expected Output(s): Exception "Invalid Name, must have 8 characters"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testEditISSN0017() {
						
				try {
					JournalDAO.instance.ModifyJournalISSN(2,"1234567");
					fail("Should not get here .. Exception Expected");
				}
				
				catch (OliverExceptionHandler e) {
					assertEquals("Invalid Name, must have 8 characters", e.getMessage());
				}
		}
		
		
		// Test Number: 18
		// Test Objective: Check that exception is thrown when 9 characters are entered
		// Input(s): ISSN = 123456789
		// Expected Output(s): Exception "Invalid Name, must have 8 characters"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testEditISSN0018() {
				
				try {
					JournalDAO.instance.ModifyJournalISSN(2,"123456789");
					fail("Should not get here .. Exception Expected");
				}
				
				catch (OliverExceptionHandler e) {
					assertEquals("Invalid Name, must have 8 characters", e.getMessage());
				}
		}
		
		
		//	Test No: 19                                                           
		// 	Object: Check if Publisher can be Updated                         
		// 	Inputs: Publisher = Glasnevin Publishing"                                                 
		// 	Expected Output: True                                                    
		//  Author: Patrick McLoughlin                                                                                                                                           
		// ****************************************************************************
		public void testPublisher0019() {

			try {
				assertEquals(true, JournalDAO.instance.ModifyPublisher
						(4, "Glasnevin Publishing"));
			} catch (Exception E) {
			}
		}
		
		
		// Test Number: 20
		// Test Objective: Check that exception is thrown when 0 characters are entered
		// Input(s): Publisher = ""
		// Expected Output(s): Exception "You've entry for publisher is blank, please enter some text"
		// Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testPublisher0020() {
				
			try {
				JournalDAO.instance.ModifyPublisher(3,"");
				fail("Should not get here .. Exception Expected");
			}
			
			catch (OliverExceptionHandler e) {
				assertEquals("You've entry for publisher is blank, please enter some text", e.getMessage());
			}
		}
		
//End of Update Journal Tests*****************************************************************
	
}
