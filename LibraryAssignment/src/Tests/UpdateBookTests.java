package Tests;

import DatabaseAccess.BookDAO;
import DatabaseAccess.JournalDAO;
import junit.framework.TestCase;

public class UpdateBookTests extends TestCase {

//*****************************************************************
//
//					Update Books Tests
//
	
	
	// Test No: 1                                                             
	// Object: Check if book title can be Updated                    
	// Inputs: Title = Malazan Book of the Fallen                                                 
	// Expected Output: Malazan Book of the Fallen                                                    
	// Author: Patrick McLoughlin                                                                                                                                             
	// ****************************************************************************
	public void testModifyTitle001() {

		try {
			assertEquals(true, BookDAO.instance.ModifyTitle(1, "Malazan Book of the Fallen"));
		} catch (Exception E) {
		}
	}
	
	
	// Test Number: 2
	// Test Objective: Check that exception is thrown when 0 characters are entered
	// Input(s): Title = ""
	// Expected Output(s): Exception "Invalid title, must enter some text"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void ModifyTitle002() {
			
		try {
			BookDAO.instance.ModifyTitle(3,"");
			fail("Should not get here .. Exception Expected");
		}
		
		catch (OliverExceptionHandler e) {
			assertEquals("Invalid title, must enter some text", e.getMessage());
		}
	}

	                                                                        
	// Test No: 3                                                               
	// Object: Check if ISBN can be updated                   
	// Inputs: ISBN = 8081212345015                                           
	// Expected Output: True                                                    
	// Author: Patrick McLoughlin                                                                                                                                             
	// ****************************************************************************
	public void testModifyISBN003() {

		try {
			assertEquals(true,BookDAO.instance.ModifyISBN(1, "8081212345015"));
		} catch (Exception E) {
		}
	}

                                                                        
	// Test No: 4                                                              
	// Object: Check author's name can be updated                  
	// Inputs: author = George RR Martin                                                 
	// Expected Output: True                                                    
	// Author: Patrick McLoughlin                                                                                                                                              
	// ****************************************************************************
	public void testModifyAuthor004() {

		try {
			assertEquals(true, BookDAO.instance.ModifyAuthor(1, "George RR Martin"));
		} catch (Exception E) {
		}
	}
	
	
	// Test Number: 5
	// Test Objective: Check that exception is thrown when 0 characters are entered
	// Input(s): Author = ""
	// Expected Output(s): Exception "Invalid author, must enter some text"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void ModifyAuthor005() {
			
		try {
			BookDAO.instance.ModifyAuthor(3,"");
			fail("Should not get here .. Exception Expected");
		}
		
		catch (OliverExceptionHandler e) {
			assertEquals("Invalid author, must enter some text", e.getMessage());
		}
	}

	
	//  Test No: 6                                                             
	// 	Object: Check if Genre can be Updated                         
	// 	Inputs: Genre = Thriller                                                     
	//	Expected Output: True                                                    
	// Author: Patrick McLoughlin                                                                    	                                                                          
	// ****************************************************************************
	public void testModifyGenre006() {

		try {
			assertEquals(true, BookDAO.instance.ModifyGenre(1, "Thriller"));
		} catch (Exception E) {
		}
	}

	
	// Test Number: 7
	// Test Objective: Check that exception is thrown when 0 characters are entered
	// Input(s): Genre = ""
	// Expected Output(s): Exception "Invalid genre, must enter some text"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testModifyGenre007() {
			
		try {
			BookDAO.instance.ModifyGenre(3,"");
			fail("Should not get here .. Exception Expected");
		}
		
		catch (OliverExceptionHandler e) {
			assertEquals("Invalid genre, must enter some text", e.getMessage());
		}
	}
	
	
	//	Test No: 8                                                             
	// 	Object: Check if Year_Published can be Updated                         
	// 	Inputs: Year_Published = 2002                                                     
	//	Expected Output: True                                                    
	// Author: Patrick McLoughlin                                                                                                                                               
	// ****************************************************************************
	public void testModifyYear_Published008() {

		try {
			assertEquals(true, BookDAO.instance.ModifyYear_Published(1, 2002));
		} catch (Exception E) {
		}
	}
	
	
	// Test Number: 9
	// Test Objective: Check that exception is thrown if invalid year entered
	// Input(s): Year_Published = 2018
	// Expected Output(s): Exception "Invalid year, must be 2017 or below"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testModifyYear_Published009() {
					
		try {
			BookDAO.instance.ModifyYear_Published(2,2018);
			fail("Should not get here .. Exception Expected");
		}
		
		catch (OliverExceptionHandler e) {
			assertEquals("Invalid year, must be 2017 or below", e.getMessage());
		}
	}
	
	
	// 	Test No: 10                                                          
	// 	Object: Check if Reference_Number can be Updated                         
	// 	Inputs: Reference_Number = E43678                                                     
	// 	Expected Output: True                                                    
	//  Author: Patrick McLoughlin                                                                                                                                         
	// ****************************************************************************
	public void testReference_Number0010() {

		try {
			assertEquals(true, BookDAO.instance.ModifyReference_Number(1, "E43678"));
		} catch (Exception E) {
		}
	}
	
	// Test Number: 11
	// Test Objective: Check that exception is thrown when 5 characters are entered
	// Input(s): Reference_Number = "E1234"
	// Expected Output(s): Exception "Invalid reference number, must be 6 characters in length"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testBookReference_Number0011() {
			
			try {
				BookDAO.instance.ModifyReference_Number(3,"E1234");
				fail("Should not get here .. Exception Expected");
			}
			
			catch (OliverExceptionHandler e) {
				assertEquals("Invalid reference number, must be 6 characters in length", e.getMessage());
			}
	}
	
	
	// Test Number: 12
	// Test Objective: Check that exception is thrown when 5 characters are entered
	// Input(s): Reference_Number = "E123456"
	// Expected Output(s): Exception "Invalid reference number, must be 6 characters in length"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testBookReference_Number0012() {
			
			try {
				BookDAO.instance.ModifyReference_Number(3,"E123456");
				fail("Should not get here .. Exception Expected");
			}
			
			catch (OliverExceptionHandler e) {
				assertEquals("Invalid reference number, must be 6 characters in length", e.getMessage());
			}
	}
	
	
	// 	Test No: 13                                                             
	// 	Object: Check if Stock_Number can be Updated                         
	// 	Inputs: Stock_Number = 4                                                     
	// 	Expected Output: True                                                    
	// Author: Patrick McLoughlin                                                                                                                                           
	// ****************************************************************************
	public void testStock_Number0013() {

		try {
			assertEquals(true, BookDAO.instance.ModifyStock_Number(1, 4));
		} catch (Exception E) {
		}
	}
	
	// Test Number: 14
	// Test Objective: Check that exception is thrown when a minus number is entered
	// Input(s): Stock_Number = -1
	// Expected Output(s): Exception "Invalid stock number"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testStock_NumberModify0014() {
					
		try {
			BookDAO.instance.ModifyStock_Number(2,-1);
			fail("Should not get here .. Exception Expected");
		}
		
		catch (OliverExceptionHandler e) {
			assertEquals("Invalid stock number", e.getMessage());
		}
	}
	
	
	// 	Test No: 15                                                            
	// 	Object: Check if Description can be Updated                         
	// 	Inputs: Description = Will the crippled god bring about the end of warren magic and possibly the world                                                    
	// 	Expected Output: True                                                    
	// Author: Patrick McLoughlin                                                                                                                                           
	// ****************************************************************************
	public void testModifyDescription0015() {

		try {
			assertEquals(true, BookDAO.instance.ModifyDescription(4, "Will the crippled god bring about the end of warren magic and possibly the world"));
		} catch (Exception E) {
		}
	}
	
	
	// Test Number: 16
	// Test Objective: Check that exception is thrown if no text entered
	// Input(s): Description = ""
	// Expected Output(s): Exception "You've entered a blank description, please enter some text"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testModifyDescription0016() {
			
			try {
				BookDAO.instance.ModifyDescription(3,"");
				fail("Should not get here .. Exception Expected");
			}
			
			catch (OliverExceptionHandler e) {
				assertEquals("You've entered a blank description, please enter some text", e.getMessage());
			}
	}

	
	// Test Number: 17
	// Test Objective: Check that exception is thrown when 12 characters are entered
	// Input(s): ISBN = 123456789123
	// Expected Output(s): Exception "Invalid Name, must have 13 characters"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testEditISBN0017() {
		
		try {
			BookDAO.instance.ModifyISBN(2,"123456789123");
			fail("Should not get here .. Exception Expected");
		}
		
		catch (OliverExceptionHandler e) {
			assertEquals("Invalid Name, must have 13 characters", e.getMessage());
		}
	}
	
	
	// Test Number: 18
	// Test Objective: Check that exception is thrown when 14 characters are entered
	// Input(s): ISBN = 12345678912345
	// Expected Output(s): Exception "Invalid Name, must have 13 characters"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testEditISBN0018() {
					
		try {
			BookDAO.instance.ModifyISBN(2,"12345678912345");
			fail("Should not get here .. Exception Expected");
		}
		
		catch (OliverExceptionHandler e) {
			assertEquals("Invalid Name, must have 13 characters", e.getMessage());
		}
	}

	
	// 	Test No: 19                                                        
	// 	Object: Check if Publisher can be Updated                         
	// 	Inputs: Publisher = Dalkey Archieve Press                                                   
	// 	Expected Output: True                                                    
	// Author: Patrick McLoughlin                                                                                                                                           
	// ****************************************************************************
	public void testPublisher0019() {

		try {
			assertEquals(true, BookDAO.instance.ModifyPublisher
					(4, "Dalkey Archieve Press"));
		} catch (Exception E) {
		}
	}
	
	
	// Test Number: 20
	// Test Objective: Check that exception is thrown when 0 characters are entered
	// Input(s): Publisher = ""
	// Expected Output(s): Exception "You've entry for publisher is blank, please enter some text"
	// Author: Patrick McLoughlin                                                                                                                                  
	// ****************************************************************************
	public void testPublisher0020() {
			
		try {
			BookDAO.instance.ModifyPublisher(3,"");
			fail("Should not get here .. Exception Expected");
		}
		
		catch (OliverExceptionHandler e) {
			assertEquals("You've entry for publisher is blank, please enter some text", e.getMessage());
		}
	}
		
//End of Update Books Tests*****************************************************************

}