package Tests;

import junit.framework.TestCase;
import DatabaseAccess.BookDAO;

public class RemoveBookTests extends TestCase {

//********************************************************************************************
//
//								Remove Book Tests
//
	
		//	Test No: 1                                                       
		// 	Object: Check if Book can be Removed from the table if reference number entered                 
		// 	Inputs: Reference_Number = E23123                                                     
		// 	Expected Output: True                                                    
		//  Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testRemoveBook001() {
			try {
				assertTrue(BookDAO.instance.Remove_Book3("E23123"));
			} catch (Exception E) {
			}
		}
		
		
		//	Test No: 2                                                      
		// 	Object: Check if Book can be Removed from the table if ISBN number entered                 
		// 	Inputs: ISBN = 3234562345111                                                     
		// 	Expected Output: True                                                    
		//  Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testRemoveBook002() {
			try {
				assertTrue(BookDAO.instance.Remove_Book2("3234562345111"));
			} catch (Exception E) {
			}
		}
		 
		
		//	Test No: 3                                                     
		// 	Object: Check if Book can be Removed from the table if a Title is entered                 
		// 	Inputs: ISBN = White Wolf                                                     
		// 	Expected Output: True                                                    
		//  Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testRemoveBook003() {
			try {
				assertTrue(BookDAO.instance.Remove_Book("White Wolf"));
			} catch (Exception E) {
			}
		}
		
		
//End of Remove Book Tests*****************************************************************

}
