package Tests;

import DatabaseAccess.BookDAO;
import Model.Book;
import junit.framework.TestCase;

public class BookDAOTests extends TestCase {
	Book testSearchBook;
	@Override
	public void setUp(){
		
		testSearchBook = new Book("A Sword of Night & Day", "David Gemmell", "Hello Description", 5,
				"9781212345015", "E23456", "Gotham Press", "Heroic Fantasy", "2007");
	}
	@Override
	public void tearDown(){
		testSearchBook = null;
	}
	
//	    _____ ______          _____   _____ _    _     _______ ______  _____ _______ _____ 
//	   / ____|  ____|   /\   |  __ \ / ____| |  | |   |__   __|  ____|/ ____|__   __/ ____|
//	  | (___ | |__     /  \  | |__) | |    | |__| |      | |  | |__  | (___    | | | (___  
//	   \___ \|  __|   / /\ \ |  _  /| |    |  __  |      | |  |  __|  \___ \   | |  \___ \ 
//	   ____) | |____ / ____ \| | \ \| |____| |  | |      | |  | |____ ____) |  | |  ____) |
//	  |_____/|______/_/    \_\_|  \_\\_____|_|  |_|      |_|  |______|_____/   |_| |_____/ 
	
	/* @Author:Joseph 
	 * Test Number: 1 
	 * Test Objective: Test getAll method against expected result;
	 * Input(s): none; 
	 * Expected Output = ArrayList containing all books in database.
	 */
	                                                                                       
	public void testSearch001(){
		/*
		 * I have decided that a check against the size of the list and against
		 * the first element in the list, is sufficient rather than checking
		 * every item in the list, as that would mean every time we add an item
		 * to the database i would have to modify this test heavily.
		 */
		// size test indicates that the correct number of journals are present
		assertEquals(4, BookDAO.instance.getAll().size());
		// a test against the first element in the list proves that the process
		// used for the first one is correct.
		// as such all subsequent items in the list should be correct too as
		// they follow the same protocol.

		assertEquals(testSearchBook.getTitle(), BookDAO.instance.getAll().get(0).getTitle());
		assertEquals(testSearchBook.getAuthor(), BookDAO.instance.getAll().get(0).getAuthor());
		assertEquals(testSearchBook.getCurrentStock(), BookDAO.instance.getAll().get(0).getCurrentStock());
		assertEquals(testSearchBook.getDescription(), BookDAO.instance.getAll().get(0).getDescription());
		assertEquals(testSearchBook.getGenres(), BookDAO.instance.getAll().get(0).getGenres());
		assertEquals(testSearchBook.getISBN(), BookDAO.instance.getAll().get(0).getISBN());
		assertEquals(testSearchBook.getItemReleaseDate(), BookDAO.instance.getAll().get(0).getItemReleaseDate());
		assertEquals(testSearchBook.getPublisher(), BookDAO.instance.getAll().get(0).getPublisher());
		assertEquals(testSearchBook.getLibraryIdentifier(),BookDAO.instance.getAll().get(0).getLibraryIdentifier());
	}       
	
	
}
