package Tests;

import DatabaseAccess.BookDAO;
import DatabaseAccess.FinalReturnNotifyDAO;
import DatabaseAccess.JournalDAO;
import DatabaseAccess.UserDAO;
import Model.Book;
import Model.Journal;
import Model.User;
import junit.framework.TestCase;

public class OliverSprint1Tests extends TestCase {
	
	
	/*
	 * Test #: 1
	 * Test OBJ: Test that I can return a user last name from the database using a valid first name
	 * Input(s): firstName = "John"
	 * Expected Output(s):
	 * 
	 * */
	public void testGetUserlastName001() {
		User user;
		try {
			user = UserDAO.instance.GetRequestedUser("John");
			assertEquals("Thomas",user.getLName());
		} catch (OliverExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * Test #: 2
	 * Test OBJ: Test that I can't return a user 
	 * 					last name from the database using an invalid first name
	 * Input(s): firstName = 0
	 * Expected Output(s):
	 * 
	 * */
	public void testGetUserlastName002() {
		User user;
		try {
			user = UserDAO.instance.GetRequestedUser("John");
			assertEquals("Thomas",user.getLName());
		} catch (OliverExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * Test #: 3
	 * Test OBJ: Test that I can return a user email from the database
	 * Input(s): firstName = "John"
	 * Expected Output(s):
	 * 
	 * */
	public void testGetUserEmail001() {
		User user;
		try {
			user = UserDAO.instance.GetRequestedUser("John");
			assertEquals("jt@lib.com",user.getEmail());
		} catch (OliverExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * Test #: 4
	 * Test OBJ: Test that I can't return a user email from the database using an invalid first name
	 * Input(s): firstName = 0
	 * Expected Output(s):
	 * 
	 * */
	public void testGetUserEmail002() {
		User user;
		try {
			user = UserDAO.instance.GetRequestedUser("John");
			assertEquals("jt@lib.com",user.getEmail());
		} catch (OliverExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * Test #: 5
	 * Test OBJ: Test that I can return a book title from the database using a valid book id
	 * Input(s): BookID = 1
	 * Expected Output(s):
	 * 
	 * */
	public void testGetBookTitle001() {
		Book book;
		try {
			book = BookDAO.instance.GetRequestedBook(1);
			assertEquals("",book.getTitle());
		} catch (OliverExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * Test #: 6
	 * Test OBJ: Test that I can't return a book title from the database using an invalid book id
	 * Input(s): BookID = "John"
	 * Expected Output(s):
	 * 
	 * */
	public void testGetBookTitle002() {
		Book book;
		try {
			book = BookDAO.instance.GetRequestedBook(1);
			assertEquals("",book.getTitle());
		} catch (OliverExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * Test #: 7
	 * Test OBJ: Test that I can return a journal title from the database using a valid journal id
	 * Input(s): JournalID = 1
	 * Expected Output(s):
	 * 
	 * */
	public void testGetJournalTitle001() {
		
		Journal journal;
		try {
			journal = JournalDAO.instance.GetRequestedJournal(1);
			assertEquals("",journal.getTitle());
		} catch (OliverExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * Test #: 8
	 * Test OBJ: Test that I can't return a journal title from the database 
	 * 			 using an invalid journal id
	 * Input(s): JournalID = ="John"
	 * Expected Output(s):
	 * 
	 * */
	public void testGetJournalTitle002() {
		
		Journal journal;
		try {
			journal = JournalDAO.instance.GetRequestedJournal(1);
			assertEquals("",journal.getTitle());
		} catch (OliverExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * Test #: 9
	 * Test OBJ: Test that a time overdue value below 
	 * 				the absolute minimum range will not be added to the database
	 * Input(s): time overdue = -2147483649
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue001() {
//		 FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
//				 "jt@lib.com",1,"",1,"",-2147483649,0);

	}
	
	/*
	 * Test #: 10
	 * Test OBJ: Test that a time overdue value that is 
	 * 				the absolute minimum range will not be added to the database
	 * Input(s): time overdue = -2147483648
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue002() {
		 FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",-2147483648,0);

	}
	
	/*
	 * Test #: 11
	 * Test OBJ: Test that a time overdue value above 
	 * 				the absolute minimum range will not be added to the database
	 * Input(s): -2147483647
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue003() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",-2147483647,0);

	}
	
	/*
	 * Test #: 12
	 * Test OBJ: Test that a time overdue value below 
	 * 				the zero reference range will not be added to the database
	 * Input(s): time overdue = -1
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue004() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",-1,0);

	}
	
	/*
	 * Test #: 13
	 * Test OBJ: Test that a time overdue value that is
	 * 				the zero reference range will not be added to the database
	 * Input(s): time overdue = 0
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue005() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",0,0);

	}
	
	/*
	 * Test #: 14
	 * Test OBJ: Test that a time overdue value above
	 * 				the zero reference range will be added to the database
	 * Input(s): time overdue = 1
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue006() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",1,0);

	}
	
	/*
	 * Test #: 15
	 * Test OBJ: Test that a time overdue value below
	 * 				the absolute maximum range will be added to the database
	 * Input(s): time overdue = 2147483646
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue007() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",2147483646,0);

	}
	
	/*
	 * Test #: 16
	 * Test OBJ: Test that a time overdue value that is
	 * 				the absolute maximum range will be added to the database
	 * Input(s): time overdue = 2147483647
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue008() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",2147483647,0);

	}
	
	/*
	 * Test #: 17
	 * Test OBJ: Test that a time overdue value above
	 * 				the absolute maximum range will be added to the database
	 * Input(s): time overdue = 2147483648
	 * Expected Output(s):
	 * 
	 * */
	public void testTimeOverdueValue009() {
//		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
//				 "jt@lib.com",1,"",1,"",2147483648,0);

	}
	
	/*
	 * Test #: 18
	 * Test OBJ: Test that an amount due value below 
	 * 				the absolute minimum range will not be added to the database
	 * Input(s): amount due = 1.45E-45
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue001() {
		 FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,(float) 1.45E-45);

	}
	
	/*
	 * Test #: 19
	 * Test OBJ: Test that an amount due value that is 
	 * 				the absolute minimum range will not be added to the database
	 * Input(s): amount due = 1.46E-45
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue002() {
		 FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,(float) 1.46E-45);

	}
	
	/*
	 * Test #: 20
	 * Test OBJ: Test that an amount due value above 
	 * 				the absolute minimum range will not be added to the database
	 * Input(s): amount due = 1.44E-45
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue003() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,(float) 1.44E-45);

	}
	
	/*
	 * Test #: 21
	 * Test OBJ: Test that an amount due value below 
	 * 				the zero reference range will not be added to the database
	 * Input(s): amount due = -1
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue004() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,-1);

	}
	
	/*
	 * Test #: 22
	 * Test OBJ: Test that an amount due  value that is
	 * 				the zero reference range will not be added to the database
	 * Input(s): amount due = 0
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue005() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,0);

	}
	
	/*
	 * Test #: 23
	 * Test OBJ: Test that an amount due value above
	 * 				the zero reference range will be added to the database
	 * Input(s): amount due = 1
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue006() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,1);

	}
	
	/*
	 * Test #: 24
	 * Test OBJ: Test that an amount due value below
	 * 				the absolute maximum range will be added to the database
	 * Input(s): amount due = 3.4028234E38
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue007() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,(float) 3.4028234E38);

	}
	
	/*
	 * Test #: 25
	 * Test OBJ: Test that an amount due that is
	 * 				the absolute maximum range will be added to the database
	 * Input(s): amount due = 3.4028235E38
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue008() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,(float) 3.4028235E38);

	}
	
	/*
	 * Test #: 26
	 * Test OBJ: Test that an amount due above
	 * 				the absolute maximum range will be added to the database
	 * Input(s): amount due = 3.4028236E38
	 * Expected Output(s):
	 * 
	 * */
	public void testAmountdueValue009() {
		FinalReturnNotifyDAO.instance.sendNewFinalReturnMessage(1,"John","Thomas",
				 "jt@lib.com",1,"",1,"",25,(float) 3.4028236E38);

	}
	
	
	


}
