package Tests;

import DatabaseAccess.JournalDAO;
import Model.Journal;
import junit.framework.TestCase;

public class JournalDAOTests extends TestCase {
	private Journal testSearchJournal;
	@Override
	public void setUp() {

		testSearchJournal = new Journal("A Day and a Night", "Patrick McLoughlin", "Hello Description", 4, "978X1234",
				"E98765", "Gotham Press", "Crime", "2013");
	}

	@Override
	public void tearDown() {
		testSearchJournal = null;
	}
	
//    _____ ______          _____   _____ _    _     _______ ______  _____ _______ _____ 
//   / ____|  ____|   /\   |  __ \ / ____| |  | |   |__   __|  ____|/ ____|__   __/ ____|
//  | (___ | |__     /  \  | |__) | |    | |__| |      | |  | |__  | (___    | | | (___  
//   \___ \|  __|   / /\ \ |  _  /| |    |  __  |      | |  |  __|  \___ \   | |  \___ \ 
//   ____) | |____ / ____ \| | \ \| |____| |  | |      | |  | |____ ____) |  | |  ____) |
//  |_____/|______/_/    \_\_|  \_\\_____|_|  |_|      |_|  |______|_____/   |_| |_____/ 

	/* @Author:Joseph 
	 * Test Number: 1 
	 * Test Objective: Test getAll method against expected result;
	 * Input(s): none; 
	 * Expected Output = ArrayList containing all journals in database.
	 */
	
	public void testSearch001(){
		/*
		 * I have decided that a check against the size of the list and against the first element in the list,
		 * is sufficient rather than checking every item in the list, as that would mean every time we add an item to
		 * the database i would have to modify this test heavily.
		 */
		//size test indicates that the correct number of journals are present
		assertEquals(4,JournalDAO.instance.getAll().size());
		// a test against the first element in the list proves that the process used for the first one is correct.
		// as such all subsequent items in the list should be correct too as they follow the same protocol.
		
		assertEquals(testSearchJournal.getTitle(),JournalDAO.instance.getAll().get(0).getTitle());
		assertEquals(testSearchJournal.getAuthor(),JournalDAO.instance.getAll().get(0).getAuthor());
		assertEquals(testSearchJournal.getCurrentStock(),JournalDAO.instance.getAll().get(0).getCurrentStock());
		assertEquals(testSearchJournal.getDescription(),JournalDAO.instance.getAll().get(0).getDescription());
		assertEquals(testSearchJournal.getGenres(),JournalDAO.instance.getAll().get(0).getGenres());
		assertEquals(testSearchJournal.getISSN(),JournalDAO.instance.getAll().get(0).getISSN());
		assertEquals(testSearchJournal.getItemReleaseDate(),JournalDAO.instance.getAll().get(0).getItemReleaseDate());
		assertEquals(testSearchJournal.getPublisher(),JournalDAO.instance.getAll().get(0).getPublisher());
		assertEquals(testSearchJournal.getLibraryIdentifier(),JournalDAO.instance.getAll().get(0).getLibraryIdentifier());
	}
}
