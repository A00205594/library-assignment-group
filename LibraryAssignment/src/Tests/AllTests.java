package Tests;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(BookDAOTests.class);
		suite.addTestSuite(JournalDAOTests.class);
		suite.addTestSuite(SearchTests.class);
		suite.addTestSuite(Book_JournalRequestTests.class);
		suite.addTestSuite(LIU10_Chandna_SaveBooks.class);
		suite.addTestSuite(ModelTests.class);
		suite.addTestSuite(OliverSprint1Tests.class);
		suite.addTestSuite(RegisterUsers_LoginLogoutTests.class);
		suite.addTestSuite(RemoveBookTests.class);
		suite.addTestSuite(RemoveJournalTests.class);
		suite.addTestSuite(RuairiAddBookTest.class);
		suite.addTestSuite(RuairiAddJournalTest.class);
		suite.addTestSuite(RuairiFinesTest.class);
		suite.addTestSuite(RuairiTestReturnDate.class);
		suite.addTestSuite(UpdateBookTests.class);
		suite.addTestSuite(UpdateJournalTests.class);
		//$JUnit-END$
		return suite;
	}

}
