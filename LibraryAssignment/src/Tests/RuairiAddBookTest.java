package Tests;
//import Tests.TestStringHan;
import DatabaseAccess.BookDAO;
import junit.framework.TestCase;

public class RuairiAddBookTest extends TestCase {

	public void testInsert() {
		

		// test 1:
				// test title is within range 
		 try {
			 
		BookDAO.instance.addValuesBook(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker ");
		 } catch (RuairiTestStringHan e) {
		 assertEquals("Sorry this Title is too long", e.getMessage());
		 }
		
		// test 2:
		// test title is out of range
		
		 try {
			 BookDAO.instance.addValuesBook(23, "The MagiciansMagiciansMagicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		 } catch (RuairiTestStringHan e) {
		 assertEquals("Sorry this Title is too long", e.getMessage());
		 }

		// test 3:
			// test isbn is within range

		try {
			BookDAO.instance.addValuesBook(24, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this ISBN is too long", e.getMessage());
		}
		
		

		// test 4:
		// test isbn is out range

		try {
			BookDAO.instance.addValuesBook(25,"The Magicians","12345678910234", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this ISBN is too long", e.getMessage());
		}
		
		// test 5:
		// test Author is within range

		try {
			BookDAO.instance.addValuesBook(19,"The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this Author is too long", e.getMessage());
		}
		
		// test 6:
		// test Author is out range

	try {
		BookDAO.instance.addValuesBook(24, "The Magicians","1234567891023", "John ReedReedReedReedReedReedReed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
	} catch (RuairiTestStringHan e) {
		assertEquals("Sorry this Author is too long", e.getMessage());
	}
		
		
		

		// test 7:
		// test category is within range

		try {
			BookDAO.instance.addValuesBook(26, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry Genre too long", e.getMessage());
		}

		// test 8:
		// test category is out of range

		try {
			BookDAO.instance.addValuesBook(27, "The Magicians","1234567891023", "John Reed", "syfysyfysyfysyfysyfysyfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry Genre too long", e.getMessage());
		}

		// ==============================================================================

		// test 9:
		// test year is within range
		try {
			BookDAO.instance.addValuesBook(28, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this year is invalid", e.getMessage());
		}

		// test 10:
		// test year is out of range

		try {
			BookDAO.instance.addValuesBook(29, "The Magicians","1234567891023", "John Reed", "syfy",2019, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this year is invalid", e.getMessage());
		}

		// test 11:
		// test ref Num is within range

		try {
			BookDAO.instance.addValuesBook(30, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this Reference Number is too long", e.getMessage());
		}

		// test 12:
		// test ref Num is out of range

		try {
			BookDAO.instance.addValuesBook(37, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1E456F1E456F1E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this Reference Number is too long", e.getMessage());
		}
		
		// test 13:
				// test num of book is within range

				try {
					BookDAO.instance.addValuesBook(47, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 0,"Hello Description", "The new yorker");
				} catch (RuairiTestStringHan e) {
					assertEquals("Stock number is out of bounds", e.getMessage());
				}

		// test 14:
		// test num of book out of range

		try {
			BookDAO.instance.addValuesBook(57, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", -1,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Stock number is out of bounds", e.getMessage());
		}

		
		
		// test 15:
		// test: book description in within rage

		try {
			BookDAO.instance.addValuesBook(67, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry Description is too long", e.getMessage());
		}
		
		// test 16:
				// test: book description is out of range

				try {
					BookDAO.instance.addValuesBook(77, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello DescriptionDescriptionDescription", "The new yorker");
				} catch (RuairiTestStringHan e) {
					assertEquals("Sorry Description is too long", e.getMessage()); 
				}
				
				// test 17:
				// test: book publisher is within range

				try {
					
					BookDAO.instance.addValuesBook(87, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker");
					
				} catch (RuairiTestStringHan e) {
					assertEquals("Sorry publisher is too long", e.getMessage());
				}
				
				// test 18:
				// test: book publisher is out of range

				try {
					BookDAO.instance.addValuesBook(17, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorkeryorkeryorkeryorker");
				} catch (RuairiTestStringHan e) {
					assertEquals("Sorry publisher is too long", e.getMessage());
				}
	}
}
