package Tests;

import junit.framework.TestCase;
import DatabaseAccess.JournalDAO;

public class RuairiAddJournalTest extends TestCase {

	public void testInsert() {

		// test 1:
		// test title is within range 
		try {
			JournalDAO.instance.addValues(12, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this Title is too long", e.getMessage()); 
		}

		// test 2:
		// test title is out of range

		try {
			JournalDAO.instance.addValues(13, "New Journal of PhysicsPhysicsPhysicsPhysics", "1234567891023","John Reed", "syfy", 1954, "E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this Title is too long", e.getMessage());
		}

		// test 3:
		// test issn is within range

		try {
			JournalDAO.instance.addValues(14, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954, "E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this issn is invalid", e.getMessage());
		}

		// test 4:
		// test issn is out of range

		try {
			JournalDAO.instance.addValues(15, "New Journal of Physics", "12345678910234", "John Reed", "syfy", 1954,"E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this issn is invalid", e.getMessage());
		}

		// test 5:
		// test author is within range

		try {
			JournalDAO.instance.addValues(16, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this author is too long", e.getMessage());
		}

		// test 6:
		// test author is out of range

		try {
			JournalDAO.instance.addValues(17, "New Journal of Physics", "1234567891023","John ReedReedReedReedReedReedReed", "syfy", 1954, "E4501", 3, "Hello Description",
					"The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this author is too long", e.getMessage());
		}

		// ==============================================================================

		// test 7:
		// test genre is within range
		try {
			JournalDAO.instance.addValues(18, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry genre is too long", e.getMessage());
		}

		// test 8:
			// test genre is out range
				try {
					JournalDAO.instance.addValues(19, "New Journal of Physics", "1234567891023", "John Reed", "syfysyfysyfysyfysyfy", 1954,"E4501", 3, "Hello Description", "The New Yorker");
				} catch (RuairiTestStringHan e) {
					assertEquals("Sorry genre is too long", e.getMessage());
				}
		
		// test 9:
		// test year is within range

		try {
			JournalDAO.instance.addValues(20, "New Journal of Physics", "1234567891023", "John Reed","syfy", 1954, "E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this is invalid year published", e.getMessage());
		}

		// test 10:
		// test year is out range

		try {
			JournalDAO.instance.addValues(21, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 2019,"E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this is invalid year published", e.getMessage());
		}

		

		// test 11:
		// test ref Num is within range

		try {
			JournalDAO.instance.addValues(22, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("out of bounds", e.getMessage());
		}

		// test 12:
				// test ref Num is out of range

				try {
					JournalDAO.instance.addValues(23, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501E4501E4501E4501", 3, "Hello Description", "The New Yorker");
				} catch (RuairiTestStringHan e) {
					assertEquals("out of bounds", e.getMessage());
				}
		
				// test 13:
				// test stock is within range

				try {
					JournalDAO.instance.addValues(7, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 0, "Hello Description", "The New Yorker");
				} catch (RuairiTestStringHan e) {
					assertEquals("invalid stock number", e.getMessage());
				}		
				
				
		// test 14:
		// test stock is out of range

		try {
			JournalDAO.instance.addValues(7, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", -1, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("invalid stock number", e.getMessage());
		}

		// #############################################################################################

		// test 15:
		// test Description is within range

		try {
			JournalDAO.instance.addValues(24, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("invalid Description", e.getMessage());
		}

		// test 16:
		// test Description is out of range

		try {
			JournalDAO.instance.addValues(25, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 3, "Hello DescriptionDescriptionDescriptionDescription", "The New Yorker");
		} catch (RuairiTestStringHan e) { 
			assertEquals("invalid Description", e.getMessage());
		}
		
		// test 17:
		// test publisher is within range
		
		try {
			JournalDAO.instance.addValues(26, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 3, "Hello Description", "The New YorkerYorkerYorkerYorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this publisher is too long", e.getMessage());
		}

		// test 18:
		// test publisher is out of range

		try {
			JournalDAO.instance.addValues(27, "New Journal of Physics", "1234567891023", "John Reed", "syfy", 1954,"E4501", 3, "Hello Description", "The New Yorker");
		} catch (RuairiTestStringHan e) {
			assertEquals("Sorry this publisher is too longs", e.getMessage());
		}
	}
}