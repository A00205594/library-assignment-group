package Tests;



import Model.Journal;
import Model.JournalCart;
import junit.framework.TestCase;

public class LIU8_Chandna_SaveJournals extends TestCase {
Journal testJournal1, testJournal2, testJournal3, testJournal4, testJournal5;
	
	@Override
	public void setUp()
	{
		testJournal1 = new Journal("A Day and a Night", "Patrick McLoughlin", "Set over a day and night will the Hercules succed in saving over the world", 4,
				"978X1234", "E98765", "Blackstaff Press", "Crime", "2013");
		testJournal2 = new Journal("A Night and a Day", "Peter Quinn", "The Secret Of Mind Power", 3,
				"17YX1233", "E56789", "Bantam Press", "Heroic Fantasy", "2007");
		testJournal3 = new Journal("The Secret Of Mind Power", "David Gemmell", "The Secret Of Mind Power", 5,
				"9781212345018", "E23458", "The Secret Of Mind Power", "Heroic Fantasy", "2007");
		testJournal4 = new Journal("The Secret Of Mind Power", "David Gemmell", "The Secret Of Mind Power", 5,
				"9781212345020", "E23469", "The Secret Of Mind Power", "Heroic Fantasy", "2007");
		testJournal5 = new Journal("The Secret Of Mind Power", "David Gemmell", "The Secret Of Mind Power", 5,
				"9781212345021", "E23479", "The Secret Of Mind Power", "Heroic Fantasy", "2007");
		
	}
	
	//**********************************************************
	//Journal Cart tests
	
	//Test No:01
	// Objective: verify that the cart/list updates on wach addition            
	// Inputs: Title = SomeJournal, journalList, ID
	// Expected Output: true                                                    
	//                                                                          
	// ****************************************************************************

	public void test_Cart_Updates_For_Each_New_Journal_Added() 
	{
		JournalCart testJournalCart;
		setUp();
		try 
		{
			testJournalCart  = new JournalCart();
			testJournalCart.addToCart(testJournal1);
			
			assertEquals(false, testJournalCart.JournalCart.isEmpty());
			assertTrue(testJournalCart.JournalCart.size() > 0);
		}
		catch (Exception E) {
			fail("Error in execution");
		}
	}
	//**********************************************************
		//JournalCart tests
		
		//Test No:02
		// Objective: verify that nothing deletes when someone enters invaled ID            
		// Inputs: Title = SomeJournal, JournalList, ID
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************

	
	public void test_Cart_does_not_delete_a_Journal_with_invalid_cartItemIndex()
	{
		JournalCart testJournalCart;
		setUp();
		try 
		{
			testJournalCart  = new JournalCart();
			testJournalCart.addToCart(testJournal1);
			testJournalCart.addToCart(testJournal2);
			
			int initialCartSize = testJournalCart.JournalCart.size();			//cart has 2 items and occupies index 0 and 1
			
			testJournalCart.removeJournalFromCartByID(50);
			int finalCartSize = testJournalCart.JournalCart.size();
			
			assertEquals(finalCartSize, initialCartSize);				//none of the journals are removed from cart
		}
		catch (Exception E)
		{
			System.out.println(E.getStackTrace());
			fail("Error in execution");
		}
	}
	
	//**********************************************************
		//Journal Cart tests
		
		//Test No:03
		// Objective: verify that the cart/list has a maximum size limit          
		// Inputs: Title = SomeJournal, journalList
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************
	
	
	public void test_Cart_respects_to_maximum_items_limit()
	{
		JournalCart testJournalCart;
		setUp();
		try 
		{
			testJournalCart  = new JournalCart();
			testJournalCart.addToCart(testJournal1);
			testJournalCart.addToCart(testJournal2);
			testJournalCart.addToCart(testJournal3);
			testJournalCart.addToCart(testJournal4);
			//testJournalCart.addToCart(testJournal5);

			int initialCartSize = testJournalCart.JournalCart.size();			//cart has maximum number of items (4) and occupies index 0 and 3
			assertEquals(testJournalCart.maxItemsAllowedInCart, initialCartSize);
			
			testJournalCart.addToCart(testJournal5);
			int finalCartSize = testJournalCart.JournalCart.size();			//trying to add 5th journal to cart
			
			assertEquals(finalCartSize, initialCartSize);				//none of the items are added to the cart once the maximum size of cart is reached		
		}
		catch (Exception E)
		{
			fail("Error in execution");
		}
	}
	
	//**********************************************************
		//Journal Cart tests
		
		//Test No:04
		// Objective: verify that the Journal deletes with a valid ID            
		// Inputs: Title = SomeJournal, journalList, ID
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************
	
	public void test_Cart_deletes_a_journal_with_valid_cartItemIndex()
	{
		JournalCart testJournalCart;
		setUp();
		try 
		{
			testJournalCart  = new JournalCart();
			testJournalCart.addToCart(testJournal1);
			testJournalCart.addToCart(testJournal2);
			
			int initialCartSize = testJournalCart.JournalCart.size();			//cart has 2 items and occupies index 0 and 1
			
			testJournalCart.removeJournalFromCartByID(1);
			int finalCartSize = testJournalCart.JournalCart.size();
			
			assertEquals(finalCartSize, initialCartSize -1);			//1 is removed from cart
		}
		catch (Exception E)
		{
			fail("Error in execution");
		}
	}
	
/*
 * //**********************************************************
	//Journal Cart tests
	
	//Test No:05
	// Objective: verify that each journal has unique ISBN number           
	// Inputs: Title = SomeJournal, journalList, ISBN number
	// Expected Output: true                                                    
	//                                                                          
	// ****************************************************************************
	public void test_CartOnlyContainsBookWithUniqueISBN()
	{
		setUp();
		try 
		{
			testJournalCart  = new JournalCart();
			testJournalCart.addToCart(testJounal1);
			testJournalCart.addToCart(testJournal2);
			
			assertEquals(false, testJournal1.getISBN().equals(testJournal2.getISBN()));
			
			testJournalCart.addToCart(testJournal1);
			
			//assertEquals(true, !testJournalCart);
			
			//assert.failNotEquals("The cart/list should have atleast one entry", 0, (Int)testJournalCart.size());
		}
		catch (Exception E) {
			fail("Error in execution");
		}
	}
*/

}
