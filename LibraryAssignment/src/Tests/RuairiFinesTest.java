package Tests;
import DatabaseAccess.BookDAO;
import DatabaseAccess.FinesDAO;
import DatabaseAccess.JournalDAO;
import junit.framework.TestCase;

public class RuairiFinesTest extends TestCase{


	 
	public void testInsert() {
		
		//test 1:
		// test Journal fine and email can be entered
		try {
			assertEquals(true, FinesDAO.instance.addFines(6.50, " A0012348@student.ait.ie"));
		} catch (Exception E) {
			
		}
		
		//test 2: 
				// test Journal email and is out of range
				try {
					assertEquals(true, FinesDAO.instance.addFines(6.50, " A00123484568461315468431354531531@student.ait.ie"));
				} catch (Exception E) {
					assertEquals("Sorry invalid entry", E.getMessage()); 
					
				}
		
		
	}	
		
}
