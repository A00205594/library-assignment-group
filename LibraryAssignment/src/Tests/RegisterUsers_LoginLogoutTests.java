package Tests;

import DatabaseAccess.UserDAO;
import Model.User;
import junit.framework.TestCase;
/*
 * Fergals Tests
 */
public class RegisterUsers_LoginLogoutTests extends TestCase{
	
	User userAdmin;
	User userStudent;

//*****************************************************************
//
//						Register User admin Tests

		
	// Test No: 1                                                             
	// Object: Valid data inputted                   
	// Inputs: FName = bob, LName = gates, email = bob@ait.ie, phNum = 10 digits, password = password
	// Expected Output: true                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle001() {
		
		try {
			User user = new User("Bob","Gates","Bob@ait.ie","1234567891","Password");
			assertEquals(true, UserDAO.instance.addUserAdmin(user));
		} catch (Exception E) {
			fail("Shoud not get here");
		}
	}

	// Test No: 2                                                             
	// Object: Invalid firstname                  
	// Inputs: FName = null, LName = gates, email = bob@ait.ie, phNum = 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle002() {
		
		try {
			User user = new User("","Gates","Bob@ait.ie","1234567891","Password");
			UserDAO.instance.addUserAdmin(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 3                                                             
	// Object: Invalid lastname                   
	// Inputs: FName = bob, *LName = null*, email = bob@ait.ie, phNum = 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle003() {
		
		try {
			User user = new User("Bob","","Bob@ait.ie","1234567891","Password");
			UserDAO.instance.addUserAdmin(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 4                                                             
	// Object: Invalid email address                  
	// Inputs: FName = bob, LName = Marley, *email = bob_ait.ie*, phNum = 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle004() {
		
		try {
			User user = new User("Bob","Marley","Bob_ait.ie","1234567891","Password");
			UserDAO.instance.addUserAdmin(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 5                                                             
	// Object: Invalid empty email address                  
	// Inputs: FName = bob, LName = Marley, *email = *, phNum = 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle005() {
		
		try {
			User user = new User("Bob","Marley","","1234567891","Password");
			UserDAO.instance.addUserAdmin(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 6                                                           
	// Object: Invalid phone num <10 digits               
	// Inputs: FName = bob, LName = Marley, email = Marl@ait.ie, phNum < 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle006() {
		
		try {
			User user = new User("Bob","Marley","Marl@ait.ie","12345678","Password");
			UserDAO.instance.addUserAdmin(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 7                                                           
	// Object: Invalid phone num >10 digits               
	// Inputs: FName = bob, LName = Marley, email = Marl@ait.ie, phNum > 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle007() {
		
		try {
			User user = new User("Bob","Marley","Marl@ait.ie","123456789281","Password");
			UserDAO.instance.addUserAdmin(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 8                                                           
	// Object: Invalid password<5chars               
	// Inputs: FName = bob, LName = Marley, email = Marl@ait.ie, phNum = 10 digits, password = pass
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle008() {
		
		try {
			User user = new User("Bob","Marley","Marl@ait.ie","123456789281","Pass");
			UserDAO.instance.addUserAdmin(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 9                                                           
	// Object: Invalid password = 5               
	// Inputs: FName = bob, LName = Marley, *email = *, phNum < 10 digits, password = pass1
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle009() {
		
		try {
			User user = new User("Bob","Marley","Marl@ait.ie","123456789281","Password");
			UserDAO.instance.addUserAdmin(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	
	
	
	
//*****************************************************************
//
//							Register regular User Tests
	// Test No: 1                                                             
	// Object: Valid data inputted                   
	// Inputs: FName = bob, LName = gates, email = bob@ait.ie, phNum = 10 digits, password = password
	// Expected Output: true                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle010() {
		
		try {
			User user = new User("Bob","Gates","Bob@ait.ie","1234567891","Password");
			assertEquals(true, UserDAO.instance.addUserReg(user));
		} catch (Exception E) {
			fail("Shoud not get here");
		}
	}

	// Test No: 2                                                             
	// Object: Invalid firstname                  
	// Inputs: FName = null, LName = gates, email = bob@ait.ie, phNum = 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle011() {
		
		try {
			User user = new User("","Gates","Bob@ait.ie","1234567891","Password");
			UserDAO.instance.addUserReg(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 3                                                             
	// Object: Invalid lastname                   
	// Inputs: FName = bob, *LName = null*, email = bob@ait.ie, phNum = 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle012() {
		
		try {
			User user = new User("Bob","","Bob@ait.ie","1234567891","Password");
			UserDAO.instance.addUserReg(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 4                                                             
	// Object: Invalid email address                  
	// Inputs: FName = bob, LName = Marley, *email = bob_ait.ie*, phNum = 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle013() {
		
		try {
			User user = new User("Bob","Marley","Bob_ait.ie","1234567891","Password");
			UserDAO.instance.addUserReg(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 5                                                             
	// Object: Invalid empty email address                  
	// Inputs: FName = bob, LName = Marley, *email = *, phNum = 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle014() {
		
		try {
			User user = new User("Bob","Marley","","1234567891","Password");
			UserDAO.instance.addUserReg(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 6                                                           
	// Object: Invalid phone num <10 digits               
	// Inputs: FName = bob, LName = Marley, email = Marl@ait.ie, phNum < 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle015() {
		
		try {
			User user = new User("Bob","Marley","Marl@ait.ie","12345678","Password");
			UserDAO.instance.addUserReg(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 7                                                           
	// Object: Invalid phone num >10 digits               
	// Inputs: FName = bob, LName = Marley, email = Marl@ait.ie, phNum > 10 digits, password = password
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle016() {
		
		try {
			User user = new User("Bob","Marley","Marl@ait.ie","123456789281","Password");
			UserDAO.instance.addUserReg(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 8                                                           
	// Object: Invalid password<5chars               
	// Inputs: FName = bob, LName = Marley, email = Marl@ait.ie, phNum = 10 digits, password = pass
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle017() {
		
		try {
			User user = new User("Bob","Marley","Marl@ait.ie","123456789281","Pass");
			UserDAO.instance.addUserReg(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
	// Test No: 9                                                           
	// Object: Invalid password = 5               
	// Inputs: FName = bob, LName = Marley, *email = *, phNum < 10 digits, password = pass1
	// Expected Output: false                                                    
	//                                                                          
	// ****************************************************************************
	public void testModifyTitle018() {
		
		try {
			User user = new User("Bob","Marley","Marl@ait.ie","123456789281","Password");
			UserDAO.instance.addUserReg(user);
			fail("Should not get here .. Exception Expected");
		} catch (OliverExceptionHandler E) {
			assertEquals("Invalid data entered", E.getMessage());
		}
	}
	
//*********************************************************************************************
//
//									Login Admin User Tests
	// Test No: 19                                                           
	// Object: Valid Login admin             
	// Inputs: email = bob@ait.ie, password = password
	// Expected Output:          valid login user object returned                                           
	//                                                                          
	// ****************************************************************************
	public void testLoginAdmin019(){
		try{
			userAdmin = UserDAO.instance.LoginUser("Bob@ait.ie", "Password");
			assertEquals(true,userAdmin!=null);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	
	// Test No: 20                                                           
	// Object: Invalid Login admin             
	// Inputs: email = bob@ait.ie, invalid password = pass
	// Expected Output:    return user = null, expected false                                                 
	//                                                                          
	// ****************************************************************************
	public void testLoginAdmin020(){
		try{
			User user = UserDAO.instance.LoginUser("Bob@ait.ie", "pass");
			assertEquals(false,user!=null);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}

	// Test No: 21                                                           
	// Object: Invalid Login admin             
	// Inputs: invalid email = bobait.ie, password = Password
	// Expected Output:    return user = null, expected false                                                 
	//                                                                          
	// ****************************************************************************
	public void testLoginAdmin021(){
		try{
			User user = UserDAO.instance.LoginUser("Bobait.ie", "Password");
			assertEquals(false,user!=null);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	
//*****************************************************************************************
//
//										Login Student User Tests
	// Test No: 22                                                           
	// Object: Valid Login student             
	// Inputs: email = Mary@ait.ie, password = Password
	// Expected Output:    Valid login user object returned                                                 
	//                                                                          
	// ****************************************************************************
	public void testLoginStudent022(){
		try{
			userStudent = UserDAO.instance.LoginUser("Mary@ait.ie", "Password");
			assertEquals(true,userStudent!=null);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	
	// Test No: 23                                                           
	// Object: Invalid Login student             
	// Inputs: email = Mary@ait.ie, invalid password = pass
	// Expected Output:    return user = null, expected false                                                 
	//                                                                          
	// ****************************************************************************
	public void testLoginStudent023(){
		try{
			User user = UserDAO.instance.LoginUser("Mary@ait.ie", "pass");
			assertEquals(false,user!=null);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}

	// Test No: 24                                                           
	// Object: Invalid Login student             
	// Inputs: invalid email =Maryait.ie, password = Password
	// Expected Output:    return user = null, expected false                                                 
	//                                                                          
	// ****************************************************************************
	public void testLoginStudent024(){
		try{
			User user = UserDAO.instance.LoginUser("Maryait.ie", "Password");
			assertEquals(false,user!=null);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	
//*****************************************************************************************
//
//							Logout User Tests
	// Test No: 25                                                           
	// Object: user admin logout            
	// Inputs: null
	// Expected Output:    return user = null                                                 
	//                                                                          
	// ****************************************************************************
	public void testLogout025(){
		userAdmin=UserDAO.instance.Logout();
		assertEquals(true,userAdmin==null);
	}
	
//*****************************************************************************************
//
//								Logout User Tests
	// Test No: 26                                                           
	// Object: user admin logout            
	// Inputs: null
	// Expected Output:    return user = null                                                 
	//                                                                          
	// ****************************************************************************
	public void testLogout026(){
		userStudent=UserDAO.instance.Logout();
		assertEquals(true,userStudent==null);
	}
//*****************************************************************************************
//
//									Update User Tests
	// Test No: 27                                                           
	// Object: update user details fname            
	// Inputs: first name, last name, email, phno, studentno, password
	// Expected Output:  updated user details returned                                           
	//                                                                          
	// ****************************************************************************
	public void testUpdateUser027(){
		try{
			boolean res = UserDAO.instance.UpdateCredentials("Bobby", "Thornton","0901233485","A98765489");
			assertEquals(true,res);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	
	// Test No: 29                                                           
	// Object: update user details lname            
	// Inputs: first name, last name, phno, studentno
	// Expected Output: true                                           
	//                                                                          
	// ****************************************************************************
	public void testUpdateUser029(){
		try{
			boolean res = UserDAO.instance.UpdateCredentials("Bobby", "Thornton","0901233485","A98765489");
			assertEquals(true,res);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	
	// Test No: 30                                                           
	// Object: update user details phno            
	// Inputs: first name, last name, phno, studentno
	// Expected Output:  true                                           
	//                                                                          
	// ****************************************************************************
	public void testUpdateUser030(){
		try{
			boolean res = UserDAO.instance.UpdateCredentials("Bobby", "Thornton","0901233485","A98765489");
			assertEquals(true,res);
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	
	// Test No: 31                                                           
	// Object: update user details studentNo            
	// Inputs: first name, last name, phno, studentno
	// Expected Output:  true                                           
	//                                                                          
	// ****************************************************************************
	public void testUpdateUser031(){
		try{
			assertEquals(true,UserDAO.instance.UpdateCredentials("Bobby", "Thornton","1111111111","A98765489"));
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	
	// Test No: 32                                                           
	// Object: update user details email            
	// Inputs: email, pass
	// Expected Output:  true                                           
	//                                                                          
	// ****************************************************************************
	public void testUpdateUser032(){
		try{
			assertEquals(true,UserDAO.instance.UpdateCredentialsEmailPass("Bob1@ait.ie","password"));
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}
	// Test No: 33                                                           
	// Object: update user details email            
	// Inputs: email, pass
	// Expected Output:  true                                           
	//                                                                          
	// ****************************************************************************
	public void testUpdateUser033(){
		try{
			assertEquals(true,UserDAO.instance.UpdateCredentialsEmailPass("Bob1@ait.ie","password1"));
		}catch(OliverExceptionHandler e){
			fail("should not get here...");
		}
	}		
}

