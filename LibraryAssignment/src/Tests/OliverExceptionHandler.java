package Tests;

public class OliverExceptionHandler extends Exception {
	
	String message;
	
	public OliverExceptionHandler(String errorMessage){
		message = errorMessage;
	}
	
	public String getMessage() {
		return message;
	}

}
