package Tests;

import java.util.ArrayList;
import java.util.List;



import DatabaseAccess.BookDAO;
import Model.Book;
import Model.BookCart;
//import Model.BookCart;
import junit.framework.TestCase;
import Tests.OliverExceptionHandler;

public class LIU10_Chandna_SaveBooks extends TestCase 
{
	
	Book testBook1, testBook2, testBook3, testBook4, testBook5, testBook6;
	
	@Override
	public void setUp()
	{
		testBook1 = new Book("A Sword of Night & Day", "David Gemmell", "A Sword of Night & Day", 5,
				"9781212345015", "E23456", "A Sword of Night & Day", "Heroic Fantasy", "2007");
		testBook2 = new Book("The Secret Of Mind Power", "David Gemmell", "The Secret Of Mind Power", 5,
				"9781212345017", "E23457", "The Secret Of Mind Power", "Heroic Fantasy", "2007");
		testBook3 = new Book("The Secret Of Mind Power", "David Gemmell", "The Secret Of Mind Power", 5,
				"9781212345018", "E23458", "The Secret Of Mind Power", "Heroic Fantasy", "2007");
		testBook4 = new Book("The Secret Of Mind Power", "David Gemmell", "The Secret Of Mind Power", 5,
				"9781212345019", "E23459", "The Secret Of Mind Power", "Heroic Fantasy", "2007");
		testBook5 = new Book("The Secret Of Mind Power", "David Gemmell", "The Secret Of Mind Power", 5,
				"9781212345020", "E23460", "The Secret Of Mind Power", "Heroic Fantasy", "2007");
		testBook6 = new Book("The Secret Of Mind Power", "David Gemmell", "The Secret Of Mind Power", 5,
				"9781212345021", "E23461", "The Secret Of Mind Power", "Heroic Fantasy", "2007");
	}
	
	//**********************************************************
	//Book Cart tests
	
	//Test No:01
	// Objective: verify that the cart/list updates on wach addition            
	// Inputs: Title = SomeBook, bookList, ID
	// Expected Output: true                                                    
	//                                                                          
	// ****************************************************************************

	public void test_Cart_Updates_For_Each_New_Book_Added() 
	{
		BookCart testBookCart;
		setUp();
		try 
		{
			testBookCart  = new BookCart();
			testBookCart.addToCart(testBook1);
			
			assertEquals(false, testBookCart.BookCart.isEmpty());
			assertTrue(testBookCart.BookCart.size() > 0);
		}
		catch (Exception E) {
			fail("Error in execution");
		}
	}
	//**********************************************************
		//Book Cart tests
		
		//Test No:02
		// Objective: verify that nothing deletes when someone enters invaled ID            
		// Inputs: Title = SomeBook, bookList, ID
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************

	
	public void test_Cart_does_not_delete_a_book_with_invalid_cartItemIndex()
	{
		BookCart testBookCart;
		setUp();
		try 
		{
			testBookCart  = new BookCart();
			testBookCart.addToCart(testBook1);
			testBookCart.addToCart(testBook2);
			
			int initialCartSize = testBookCart.BookCart.size();			//cart has 2 items and occupies index 0 and 1
			
			testBookCart.removeBookFromCartByID(50);
			int finalCartSize = testBookCart.BookCart.size();
			
			assertEquals(finalCartSize, initialCartSize);				//none of the books are removed from cart
		}
		catch (Exception E)
		{
			System.out.println(E.getStackTrace());
			fail("Error in execution");
		}
	}
	
	//**********************************************************
		//Book Cart tests
		
		//Test No:03
		// Objective: verify that the cart/list has a maximum size limit          
		// Inputs: Title = SomeBook, bookList
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************
	
	
	public void test_Cart_respects_to_maximum_items_limit()
	{
		BookCart testBookCart;
		setUp();
		try 
		{
			testBookCart  = new BookCart();
			testBookCart.addToCart(testBook1);
			testBookCart.addToCart(testBook2);
			testBookCart.addToCart(testBook3);
			testBookCart.addToCart(testBook4);
			testBookCart.addToCart(testBook5);

			int initialCartSize = testBookCart.BookCart.size();			//cart has maximum number of items (5) and occupies index 0 and 4
			assertEquals(testBookCart.maxItemsAllowedInCart, initialCartSize);
			
			testBookCart.addToCart(testBook6);
			int finalCartSize = testBookCart.BookCart.size();			//trying to add 6th book to cart
			
			assertEquals(finalCartSize, initialCartSize);				//none of the items are added to the cart once the maximum size of cart is reached		
		}
		catch (Exception E)
		{
			fail("Error in execution");
		}
	}
	
	//**********************************************************
		//Book Cart tests
		
		//Test No:04
		// Objective: verify that the book deletes with a valid ID            
		// Inputs: Title = SomeBook, bookList, ID
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************
	
	public void test_Cart_deletes_a_book_with_valid_cartItemIndex()
	{
		BookCart testBookCart;
		setUp();
		try 
		{
			testBookCart  = new BookCart();
			testBookCart.addToCart(testBook1);
			testBookCart.addToCart(testBook2);
			
			int initialCartSize = testBookCart.BookCart.size();			//cart has 2 items and occupies index 0 and 1
			
			testBookCart.removeBookFromCartByID(1);
			int finalCartSize = testBookCart.BookCart.size();
			
			assertEquals(finalCartSize, initialCartSize -1);			//1 is removed from cart
		}
		catch (Exception E)
		{
			fail("Error in execution");
		}
	}
	
/*
 * //**********************************************************
	//Book Cart tests
	
	//Test No:05
	// Objective: verify that each book has unique ISBN number           
	// Inputs: Title = SomeBook, bookList, ISBN number
	// Expected Output: true                                                    
	//                                                                          
	// ****************************************************************************
	public void test_CartOnlyContainsBookWithUniqueISBN()
	{
		setUp();
		try 
		{
			testBookCart  = new BookCart();
			testBookCart.addToCart(testBook1);
			testBookCart.addToCart(testBook2);
			
			assertEquals(false, testBook1.getISBN().equals(testBook2.getISBN()));
			
			testBookCart.addToCart(testBook1);
			
			//assertEquals(true, !testBookCart);
			
			//assert.failNotEquals("The cart/list should have atleast one entry", 0, (Int)testBookCart.size());
		}
		catch (Exception E) {
			fail("Error in execution");
		}
	}
*/
}