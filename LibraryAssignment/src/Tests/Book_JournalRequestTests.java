package Tests;

import DatabaseAccess.BookDAO;
import DatabaseAccess.JournalDAO;
import Model.Book;
import Model.Journal;
import junit.framework.TestCase;
import Tests.OliverExceptionHandler;

public class Book_JournalRequestTests extends TestCase{
	//**********************************************************
		//Book Request tests
		
		//Test No:01
		// Objective: Verify user can place Book request by title  and no author   with email               
		// Inputs: Title = SomeBook
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************

//		public void testRequest001(){
//			try {
//				assertEquals(true, BookDAO.instance.AddBookRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//			} catch (OliverExceptionHandler E) {
//				fail("Error in execution");
//			}
//		}
//		
		//Test No:02
			// Objective: Verify user can place Book request by Author and no title along with email               
			// Inputs: Author = SomeAuthor
			// Expected Output: true                                                    
			//                                                                          
			// ****************************************************************************
//
//			public void testRequest002(){
//				try {
//					assertEquals(true, BookDAO.instance.AddBookRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//				} catch (OliverExceptionHandler E) {
//					fail("Error in execution");
//				}
//			}
//			
//			//Test No:03
//			// Objective: Verify user can place Book request with Description provided either title is entered                
//			// Inputs: Description = book about Java
//			// Expected Output: true                                                    
//			//                                                                          
//			// ****************************************************************************
//
//			public void testRequest003(){
//				try {
//					assertEquals(true, BookDAO.instance.AddBookRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//				} catch (OliverExceptionHandler E) {
//					fail("Error in execution");
//				}
//			}
//			
//			//Test No:04
//			// Objective: Verify user can place Book request with Year provided title or author is entered and email               
//			// Inputs: Description = book about Java
//			// Expected Output: true                                                    
//			//                                                                          
//			// ****************************************************************************
//
//			public void testRequest004(){
//				try {
//					assertEquals(true, BookDAO.instance.AddBookRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//				} catch (OliverExceptionHandler E) {
//					fail("Error in execution");
//				}
//			}
//			
//			//Test No:05
//			// Objective: Verify user can place Book request with all parameters entered                
//			// Inputs: title, author, description, year, email
//			// Expected Output: true                                                    
//			//                                                                          
//			// ****************************************************************************
//
//			public void testRequest005(){
//				try {
//					assertEquals(true, BookDAO.instance.AddBookRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//				} catch (OliverExceptionHandler E) {
//					fail("Error in execution");
//				}
//			}
//			
//			//Test No:06
//			// Objective: Verify user can't place Book request without Title/author                
//			// Inputs: title, author, description, year, emailid
//			// Expected Output: true                                                    
//			//                                                                          
//			// ****************************************************************************
//
//			public void testRequest006(){
//				try {
//					BookDAO.instance.AddBookRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker ");
//					fail("Error in execution");
//				} catch (OliverExceptionHandler E) {
//					assertEquals("Invalid input",E.getMessage());
//				}
//			}
//			
//	//**********************************************************
//	//Journal Request tests
//			
//			//Test No:01
//			// Objective: Verify user can place Journal request by title  and no author   with email               
//			// Inputs: Title = SomeBook
//			// Expected Output: true                                                    
//			//                                                                          
//			// ****************************************************************************
//
//			public void testRequest007(){
//				try {
//					assertEquals(true, JournalDAO.instance.AddJournalRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//				} catch (OliverExceptionHandler E) {
//					fail("Error in execution");
//				}
//			}
//			
//			//Test No:02
//				// Objective: Verify user can place Journal request by Author and no title along with email               
//				// Inputs: Author = SomeAuthor
//				// Expected Output: true                                                    
//				//                                                                          
//				// ****************************************************************************
//
//				public void testRequest008(){
//					try {
//						assertEquals(true, JournalDAO.instance.AddJournalRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//					} catch (OliverExceptionHandler E) {
//						fail("Error in execution");
//					}
//				}
//				
//				//Test No:03
//				// Objective: Verify user can place Journal request with Description provided either title is entered                
//				// Inputs: Description = book about Java
//				// Expected Output: true                                                    
//				//                                                                          
//				// ****************************************************************************
//
//				public void testRequest009(){
//					try {
//						assertEquals(true, JournalDAO.instance.AddJournalRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//					} catch (OliverExceptionHandler E) {
//						fail("Error in execution");
//					}
//				}
//				
//				//Test No:04
//				// Objective: Verify user can place Journal request with Year provided title or author is entered and email               
//				// Inputs: Description = book about Java
//				// Expected Output: true                                                    
//				//                                                                          
//				// ****************************************************************************
//
//				public void testRequest010(){
//					try {
//						assertEquals(true, JournalDAO.instance.AddJournalRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//					} catch (OliverExceptionHandler E) {
//						fail("Error in execution");
//					}
//				}
//				
//				//Test No:05
//				// Objective: Verify user can place Journal request with all parameters entered                
//				// Inputs: title, author, description, year, email
//				// Expected Output: true                                                    
//				//                                                                          
//				// ****************************************************************************
//
//				public void testRequest011(){
//					try {
//						assertEquals(true, JournalDAO.instance.AddJournalRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker "));
//					} catch (OliverExceptionHandler E) {
//						fail("Error in execution");
//					}
//				}
//				
//				//Test No:06
//				// Objective: Verify user can't place Journal request without Title/author                
//				// Inputs: title, author, description, year, emailid
//				// Expected Output: true                                                    
//				//                                                                          
//				// ****************************************************************************
//
//				public void testRequest012(){
//					try {
//						JournalDAO.instance.AddJournalRequest(32, "The Magicians","1234567891023", "John Reed", "syfy",2015, "E456F1", 4,"Hello Description", "The new yorker ");
//						fail("Error in execution");
//					} catch (OliverExceptionHandler E) {
//						assertEquals("Invalid input",E.getMessage());
//					}
//				}
	
	//*******************************************New Tests for changed approach**********************************************
	
	//Test No:01
		// Objective: Verify user can request and obtain book by ID                
		// Inputs: book id = 1
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************

		public void testRequest001(){
			try {
				Book rbook = BookDAO.instance.GetRequestedBook(1);
				assertEquals(true, rbook!=null);
			} catch (OliverExceptionHandler E) {
				fail("Error book is null");
			}
		}
	//Test No:02
	// Objective: Verify user place a request by obtaining book details using id                
	// Inputs: book id, student email for id
	// Expected Output: true                                                    
	//                                                                          
	// ****************************************************************************

	public void testRequest002(){
		try {
			Book rbook = BookDAO.instance.GetRequestedBook(1);
			assertEquals(true,BookDAO.instance.AddBookRequest(rbook.getId(),rbook.getTitle(),rbook.getISBN(),rbook.getAuthor(),rbook.getGenres(),
					rbook.getReleaseDate(), rbook.getLibraryReferenceNumber(), rbook.getDescription(), rbook.getPublisher(),"student@mail"));
		} catch (OliverExceptionHandler E) {
			fail("Error in execution");
		}
	}
	
	//Test No:03
	// Objective: Verify user cant request a book by incorrect ID                
	// Inputs: book id = 0
	// Expected Output: true                                                    
	//                                                                          
	// ****************************************************************************

	public void testRequest003(){
		try {
			Book rbook = BookDAO.instance.GetRequestedBook(0);
			assertEquals(true,rbook==null);
		} catch (OliverExceptionHandler E) {
			fail("Error book is not null");
		}
	}
	//Test No:04
		// Objective: Verify user cant request a non existing book by ID                
		// Inputs: book id = 0
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************

		public void testRequest004(){
			try {
				Book rbook = BookDAO.instance.GetRequestedBook(6);
				assertEquals(true,rbook==null);
			} catch (OliverExceptionHandler E) {
				fail("Error book is not null");
			}
		}
		//*********************************************New Journal Tests**********************************************
		//Test No:05
		// Objective: Verify user can request and obtain book by ID                
		// Inputs: book id = 1
		// Expected Output: true                                                    
		//                                                                          
		// ****************************************************************************

		public void testRequest005(){
			try {
				Journal rjournal = JournalDAO.instance.GetRequestedJournal(1);
				assertEquals(true, rjournal!=null);
			} catch (OliverExceptionHandler E) {
				fail("Error book is null");
			}
		}
			//Test No:06
			// Objective: Verify user place a request by obtaining Journal details using id                
			// Inputs: book id, student email for id
			// Expected Output: true                                                    
			//                                                                          
			// ****************************************************************************

			public void testRequest006(){
				try {
					Journal rjournal = JournalDAO.instance.GetRequestedJournal(1);
					assertEquals(true,JournalDAO.instance.AddJournalRequest(rjournal.getId(),rjournal.getTitle(),rjournal.getISSN(),rjournal.getAuthor(),rjournal.getGenres(),
							rjournal.getReleaseDate(), rjournal.getLibraryReferenceNumber(), rjournal.getDescription(), rjournal.getPublisher(),"student@mail"));
				} catch (OliverExceptionHandler E) {
					fail("Error in execution");
				}
			}
			
			//Test No:07
			// Objective: Verify user cant request a Journal by incorrect ID                
			// Inputs: book id = 0
			// Expected Output: true                                                    
			//                                                                          
			// ****************************************************************************

			public void testRequest007(){
				try {
					Journal rjournal = JournalDAO.instance.GetRequestedJournal(0);
					assertEquals(true,rjournal==null);
				} catch (OliverExceptionHandler E) {
					fail("Error book is not null");
				}
			}
			//Test No:08
				// Objective: Verify user cant request a non existing Journal by ID                
				// Inputs: book id = 0
				// Expected Output: true                                                    
				//                                                                          
				// ****************************************************************************

				public void testRequest008(){
					try {
						Journal rjournal = JournalDAO.instance.GetRequestedJournal(6);
						assertEquals(true,rjournal==null);
					} catch (OliverExceptionHandler E) {
						fail("Error book is not null");
					}
				}
}
