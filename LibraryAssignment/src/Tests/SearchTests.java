package Tests;

import java.util.ArrayList;

import DatabaseAccess.BookDAO;
import Model.Book;
import Model.DisplayModel;
import PageController.PageLoader;
import View.SearchPage;
import junit.framework.TestCase;

public class SearchTests extends TestCase {

	SearchPage searchPage;
	@Override
	public void setUp() {
		searchPage = new SearchPage(new PageLoader());
	}

	@Override
	public void tearDown() {
		searchPage = null;
	}
	
	
	/* @Author:Joseph 
	 * Test Number: 1 
	 * Test Objective: Test DisplayModel search title.
	 * Input(s): "Sword"; 
	 * Expected Output = ArrayList containing one DisplayModel that matches the book in database.
	 */
	public void testSearch001(){
		ArrayList<Book> t = BookDAO.instance.getAll();
		ArrayList<DisplayModel> casted = new ArrayList<DisplayModel>();
		casted.addAll(t);
		ArrayList<DisplayModel> res = searchPage.titleSearch(casted, "Sword");
		assertEquals("A Sword of Night & Day",res.get(0).getItemTitle());
	}
	
	
	/* @Author:Joseph 
	 * Test Number: 2 
	 * Test Objective: Test DisplayModel search author.
	 * Input(s): "Gemmell"; 
	 * Expected Output = ArrayList containing one DisplayModel that matches the book in database.
	 */
	
	public void testSearch002(){
		ArrayList<Book> t = BookDAO.instance.getAll();
		ArrayList<DisplayModel> casted = new ArrayList<DisplayModel>();
		casted.addAll(t);
		ArrayList<DisplayModel> res = searchPage.authorSearch(casted, "Gemmel");
		assertEquals("David Gemmell",res.get(0).getItemAuthor());
	}
	
	/* @Author:Joseph 
	 * Test Number: 3
	 * Test Objective: Test DisplayModel search genre.
	 * Input(s): "Fantasy"; 
	 * Expected Output = ArrayList containing one DisplayModel that matches the book in database.
	 */
	public void testSearch003(){
		ArrayList<Book> t = BookDAO.instance.getAll();
		ArrayList<DisplayModel> casted = new ArrayList<DisplayModel>();
		casted.addAll(t);
		ArrayList<DisplayModel> res = searchPage.genreSearch(casted, "fantasy");
		assertEquals("Heroic Fantasy",res.get(0).getItemGenre());
	}
	
	/* @Author:Joseph 
	 * Test Number: 4
	 * Test Objective: Test DisplayModel search library reference.
	 * Input(s): "E5432"; 
	 * Expected Output = ArrayList containing one DisplayModel that matches the book in database.
	 */
	public void testSearch004() {
		ArrayList<Book> t = BookDAO.instance.getAll();
		ArrayList<DisplayModel> casted = new ArrayList<DisplayModel>();
		casted.addAll(t);
		ArrayList<DisplayModel> res = searchPage.libraryReferenceSearch(casted, "E5432");
		assertEquals("E54321",res.get(0).getLibraryIdentifier());
	}
	/* @Author:Joseph 
	 * Test Number: 5
	 * Test Objective: Test DisplayModel search release date.
	 * Input(s): "2007"; 
	 * Expected Output = ArrayList containing one DisplayModel that matches the book in database.
	 */
	public void testSearch005(){
		ArrayList<Book> t = BookDAO.instance.getAll();
		ArrayList<DisplayModel> casted = new ArrayList<DisplayModel>();
		casted.addAll(t);
		ArrayList<DisplayModel> res = searchPage.releaseDateSearch(casted, "2007");
		assertEquals("2007",res.get(0).getItemReleaseDate());
	}
	/* @Author:Joseph 
	 * Test Number: 6
	 * Test Objective: Test DisplayModel search publisher id.
	 * Input(s): "9781212345015"; 
	 * Expected Output = ArrayList containing one DisplayModel that matches the book in database.
	 */
	public void testSearch006(){
		ArrayList<Book> t = BookDAO.instance.getAll();
		ArrayList<DisplayModel> casted = new ArrayList<DisplayModel>();
		casted.addAll(t);
		ArrayList<DisplayModel> res = searchPage.publisherIdSearch(casted, "9781212345015");
		assertEquals("9781212345015",res.get(0).getPublisherIdentifier());
	}
}
