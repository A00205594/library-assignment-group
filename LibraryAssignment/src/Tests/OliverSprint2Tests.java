package Tests;

import DatabaseAccess.UserDAO;
import Model.User;
import Utils.MailSender;
import junit.framework.TestCase;

public class OliverSprint2Tests extends TestCase {
	
	/*
	 * Test #: 1
	 * Test OBJ:  Test I can't send a notification with all fields null
	 * Input(s): to = null,subject = null, message = null
	 * Expected Output(s):
	 * 
	 * */
	
	public void testsendorderNotification001() {
		
		try{
		String to = null;
		String subject = null;
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}

	
	/*
	 * Test #: 2
	 * Test OBJ:  Test I can't send a notification with asubject and message fields null
	 * Input(s): to = "student@mail.com", subject = null, message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification002() {
		
		try{
		String to = "student@mail.com";
		String subject = null;
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}

	
	/*
	 * Test #: 3
	 * Test OBJ:  Test I can't send a notification with message field null
	 * Input(s): to = "student@mail.com", subject = "order", message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification003() {
		
		try{
		String to = "student@mail.com";
		String subject = "order";
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 4
	 * Test OBJ:  Test I can't send a notification with to field null
	 * Input(s): to = null, subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification004() {
		
		try{
		String to = null;
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 5
	 * Test OBJ:  Test I can't send a notification with message field null
	 * Input(s): to = "student@mail.com", subject = null, message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification005() {
		
		try{
		String to = "student@mail.com";
		String subject = null;
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 6
	 * Test OBJ:  Test I can send a notification with all fields not null
	 * Input(s): to = "student@mail.com", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification006() {
		
		
		String to = "student@mail.com";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		
		}
	
	/*
	 * Test #: 7
	 * Test OBJ:  Test I can't send a notification with a to field that doesnt contain an "@" symbol
	 * Input(s): to = "studentmail.com",subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */
	
	public void testsendorderNotification007() {
		
		try{
		String to = "studentmail.com";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
				fail();
	}
		catch(Exception e){
		
		}
		
		}

	
	/*
	 * Test #: 8
	 * Test OBJ:   Test I can't send a notification with an invalid extension
	 * Input(s): to = "student@mail.", subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification008() {
		
		try{
		String to = "student@mail.";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}

	
	/*
	 * Test #: 9
	 * Test OBJ:  Test I can't send a notification with a to field that doesnt contain a . symbol
	 * Input(s): to = "student@mailcom", subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification009() {
		
		try{
		String to = "student@mailcom";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 10
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "@" symbol
	 * Input(s): to = "@student@mailcom", subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification010() {
		
		try{
		String to = "@student@mailcom";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 11
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol
	 * Input(s): to = ".student@mail.com", subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification011() {
		
		try{
		String to = ".student@mail.com";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 12
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol
	 * 			and more than one "@" symbol
	 * Input(s): to = ".@student@mail.com", subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification012() {
		
		try{
		String to = ".@student@mail.com";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 13
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol
	 * 			and no "@" symbol
	 * Input(s): to = ".studentmail.com", subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification013() {
		
		try{
		String to = ".studentmail.com";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 14
	 * Test OBJ:  Test I can't send a notification with a to field that has no "." symbol
	 * 			and more than one "@" symbol
	 * Input(s): to = "@student@mailcom", subject = "order", message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification014() {
		
		
		try{
			
		String to = "@student@mailcom";
		String subject = "order";
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}
	
	/*
	 * Test #: 15
	 * Test OBJ:  Test I can't send a notification with a to field that has no "." symbol
	 * 			and a subject field null
	 * Input(s): to = "@student@mailcom", subject = null, message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification015() {
		
		
		try{
			
		String to = "@student@mailcom";
		String subject = null;
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}
	
	/*
	 * Test #: 16
	 * Test OBJ:  Test I can't send a notification with a to field that has no "@" symbol
	 * 			and a subject field null
	 * Input(s): to = "@student@mailcom", subject = null, message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification016() {
		
		
		try{
			
		String to = "studentmail.com";
		String subject = null;
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}
	
	/*
	 * Test #: 17
	 * Test OBJ:  Test I can't send a notification with a to field that has no "." symbol
	 * 			and a message field null
	 * Input(s): to = "@student@mailcom", subject = "order", message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification017() {
		
		
		try{
			
		String to = "@student@mailcom";
		String subject = "order";
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}
	
	/*
	 * Test #: 18
	 * Test OBJ:  Test I can't send a notification with a to field that has no "@" symbol
	 * 			and a message field null
	 * Input(s): to = "@student@mailcom", subject = "order", message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification018() {
		
		
		try{
			
		String to = "studentmail.com";
		String subject = "order";
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}
	
	/*
	 * Test #: 19
	 * Test OBJ:  Test I can't send a notification with a to field that has no "." symbol
	 * 			and a subject field null and a message field null
	 * Input(s): to = "@student@mailcom", subject = null, message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification019() {
		
		
		try{
			
		String to = "@student@mailcom";
		String subject = null;
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}
	
	/*
	 * Test #: 20
	 * Test OBJ:  Test I can't send a notification with a to field that has no "@" symbol
	 * 			and a subject field null and a message field null
	 * Input(s): to = "@student@mailcom", subject = null, message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification020() {
		
		
		try{
			
		String to = "studentmail.com";
		String subject = null;
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * Test #: 21
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "@" symbol
	 * 			  and a subject field null
	 * Input(s): to = "@student@mailcom", subject = null, message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification021() {
		
		try{
		String to = "@student@mailcom";
		String subject = null;
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 22
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol 
	 * 				and a subject field null
	 * Input(s): to = ".student@mail.com", subject = null, message = "The order status is ready" 
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification022() {
		
		try{
		String to = ".student@mail.com";
		String subject = null;
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 12
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol
	 * 			and more than one "@" symbol and a subject field null
	 * Input(s): to = ".@student@mail.com", subject = null, message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification023() {
		
		try{
		String to = ".@student@mail.com";
		String subject = null;
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
	
	/*
	 * Test #: 24
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "@" symbol
	 * 			  and a subject field null and a message field null
	 * Input(s): to = "@student@mailcom", subject = null, message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification024() {
		
		try{
		String to = "@student@mailcom";
		String subject = null;
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 25
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol 
	 * 				and a subject field null and a message field null
	 * Input(s): to = ".student@mail.com", subject = null, message = null 
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification025() {
		
		try{
		String to = ".student@mail.com";
		String subject = null;
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 26
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol
	 * 			and more than one "@" symbol and a subject field null and a message field null
	 * Input(s): to = ".@student@mail.com", subject = null, message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification026() {
		
		try{
		String to = ".@student@mail.com";
		String subject = null;
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/*
	 * Test #: 27
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol
	 * 			and no "@" symbol and a subject field null
	 * Input(s): to = ".studentmail.com", subject = null, message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification027() {
		
		try{
		String to = ".studentmail.com";
		String subject = null;
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 28
	 * Test OBJ:  Test I can't send a notification with a to field that has no "." symbol
	 * 			and more than one "@" symbol and a subject field null
	 * Input(s): to = "@student@mailcom", subject = null, message = "The order status is ready"
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification028() {
		
		
		try{
			
		String to = "@student@mailcom";
		String subject = null;
		String message = "The order status is ready";
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}

	/////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	/*
	 * Test #: 29
	 * Test OBJ:  Test I can't send a notification with a to field that has more than one "." symbol
	 * 			and no "@" symbol and a message field null
	 * Input(s): to = ".studentmail.com", subject = "order", message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification029() {
		
		try{
		String to = ".studentmail.com";
		String subject = "order";
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		}
		catch(Exception e){
			
		}
		
		}
	
	/*
	 * Test #: 30
	 * Test OBJ:  Test I can't send a notification with a to field that has no "." symbol
	 * 			and more than one "@" symbol and a message field null
	 * Input(s): to = "@student@mailcom", subject = "order", message = null
	 * Expected Output(s):
	 * 
	 * */

	public void testsendorderNotification030() {
		
		
		try{
			
		String to = "@student@mailcom";
		String subject = "order";
		String message = null;
		MailSender.instance.send(to, subject, message);
		fail();
		
		}
		catch(Exception e){
			
		}
		
	}
	
	
} // end test class
