package Tests;

import Model.Journal;
import Model.DisplayModel;
import Model.Book;
import junit.framework.TestCase;

public class ModelTests extends TestCase {
	Journal emptyConstructorJournal;
	Journal completedConstructorJournal;
	Journal noIdConstructorJournal;
	DisplayModel journalTest,bookTest;
	Book emptyConstructorBook;
	Book completedConstructorBook;
	Book noIdConstructorBook;

	@Override
	public void setUp() {
		emptyConstructorJournal = new Journal();

		completedConstructorJournal = new Journal(1, "test title 1", "test author 1", "test description 1", 1,
				"test ISSN 1", "test library reference 1", "test publisher 1", "test genres","test year 1");

		noIdConstructorJournal = new Journal("test title 2", "test author 2", "test description 2", 2, "test ISSN 2",
				"test library reference 2", "test publisher 2", "test 2 genres","test year 2");

		emptyConstructorBook = new Book();

		completedConstructorBook = new Book(1, "test title 1", "test author 1", "test description 1", 1, "test ISBN 1",
				"test library reference 1", "test publisher 1", "test genres","test year 1");

		noIdConstructorBook = new Book("test title 2", "test author 2", "test description 2", 2, "test ISBN 2",
				"test library reference 2", "test publisher 2", "test 2 genres","test year 2");
		
		journalTest = new Journal(2, "test title 1", "test author 1", "test description 1", 1,
				"test ISSN 1", "test library reference 1", "test publisher 1", "test genre 1","test year 1");
		
		bookTest = new Book(1,"test title 2", "test author 2", "test description 2", 2, "test ISBN 2",
				"test library reference 2", "test publisher 2", "test genre 2","test year 2");
	}

	@Override
	public void tearDown() {
		emptyConstructorJournal = null;
		completedConstructorJournal = null;
		noIdConstructorJournal = null;
		
		emptyConstructorBook = null;
		completedConstructorBook = null;
		noIdConstructorBook = null;
		
		journalTest = null;
		bookTest = null;
	}

//	      _   ____   _    _  _____   _   _            _        __  __   ____   _____   ______  _        _______  ______   _____  _______  _____ 
//	     | | / __ \ | |  | ||  __ \ | \ | |    /\    | |      |  \/  | / __ \ |  __ \ |  ____|| |      |__   __||  ____| / ____||__   __|/ ____|
//	     | || |  | || |  | || |__) ||  \| |   /  \   | |      | \  / || |  | || |  | || |__   | |         | |   | |__   | (___     | |  | (___  
//   _   | || |  | || |  | ||  _  / | . ` |  / /\ \  | |      | |\/| || |  | || |  | ||  __|  | |         | |   |  __|   \___ \    | |   \___ \ 
//  | |__| || |__| || |__| || | \ \ | |\  | / ____ \ | |____  | |  | || |__| || |__| || |____ | |____     | |   | |____  ____) |   | |   ____) |
//   \____/  \____/  \____/ |_|  \_\|_| \_|/_/    \_\|______| |_|  |_| \____/ |_____/ |______||______|    |_|   |______||_____/    |_|  |_____/ 
	                                                                                                                                          
	                                                                                                                                          

	
	/* @Author:Joseph 
	 * Test Number: 1 
	 * Test Objective: Test Journal getId method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output = 0,1,0
	 */
	public void testJournal001() {
		assertEquals(0,emptyConstructorJournal.getId());
		assertEquals(1,completedConstructorJournal.getId());
		assertEquals(0,noIdConstructorJournal.getId());
	}
	
	/* @Author:Joseph 
	 * Test Number: 2
	 * Test Objective: Test Journal setId method works when object is initiated with each constructor type.
	 * Input(s): 1,2,3; 
	 * Expected Output = 1,2,3;
	 */
	public void testJournal002() {
		emptyConstructorJournal.setId(1);
		completedConstructorJournal.setId(2);
		noIdConstructorJournal.setId(3);
		assertEquals(1,emptyConstructorJournal.getId());
		assertEquals(2,completedConstructorJournal.getId());
		assertEquals(3,noIdConstructorJournal.getId());
	}
	
	/* @Author:Joseph 
	 * Test Number: 3
	 * Test Objective: Test Journal getGenres method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output = "","test genres", "test 2 genres";
	 */
	public void testJournal003() {

		assertEquals("",emptyConstructorJournal.getGenres());
		assertEquals("test genres",completedConstructorJournal.getGenres());
		assertEquals("test 2 genres",noIdConstructorJournal.getGenres());
	}
	
	/* @Author:Joseph 
	 * Test Number: 4
	 * Test Objective: Test Journal setGenres method works when object is initiated with each constructor type.
	 * Input(s): "test genres 1","test genres 2","test genres 3"; 
	 * Expected Output = "test genres 1","test genres 2", "test genres 3";
	 */
	public void testJournal004() {
		emptyConstructorJournal.setGenres("test genres");
		completedConstructorJournal.setGenres("test genres 2");
		noIdConstructorJournal.setGenres("test genres 3");
		assertEquals("test genres",emptyConstructorJournal.getGenres());
		assertEquals("test genres 2",completedConstructorJournal.getGenres());
		assertEquals("test genres 3",noIdConstructorJournal.getGenres());
	}
	
	/* @Author:Joseph 
	 * Test Number: 5
	 * Test Objective: Test Journal getPublisher method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output = "","test publisher 1", "test publisher 2";
	 */
	public void testJournal005(){
		assertEquals("",emptyConstructorJournal.getPublisher());
		assertEquals("test publisher 1",completedConstructorJournal.getPublisher());
		assertEquals("test publisher 2",noIdConstructorJournal.getPublisher());
	}
	
	/* @Author:Joseph 
	 * Test Number: 6
	 * Test Objective: Test Journal setPublisher method works when object is initiated with each constructor type.
	 * Input(s): "test publisher 1","test publisher 2","test publisher"; 
	 * Expected Output = "","test publisher 1", "test publisher 2";
	 */
	public void testJournal006(){
		emptyConstructorJournal.setPublisher("test publisher 1");
		completedConstructorJournal.setPublisher("test publisher 2");
		noIdConstructorJournal.setPublisher("test publisher 3");
		assertEquals("test publisher 1",emptyConstructorJournal.getPublisher());
		assertEquals("test publisher 2",completedConstructorJournal.getPublisher());
		assertEquals("test publisher 3",noIdConstructorJournal.getPublisher());
	}
	
	/* @Author:Joseph 
	 * Test Number: 7
	 * Test Objective: Test Journal getStock method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output =0,1,2;
	 */
	public void testJournal007(){
		assertEquals(0,emptyConstructorJournal.getStock());
		assertEquals(1,completedConstructorJournal.getStock());
		assertEquals(2,noIdConstructorJournal.getStock());
	}
	
	/* @Author:Joseph 
	 * Test Number: 8
	 * Test Objective: Test Journal setStock method works when object is initiated with each constructor type.
	 * Input(s): 1,2,3; 
	 * Expected Output =1,2,3;
	 */
	public void testJournal008(){
		emptyConstructorJournal.setStock(1);
		completedConstructorJournal.setStock(2);
		noIdConstructorJournal.setStock(3);
		assertEquals(1,emptyConstructorJournal.getStock());
		assertEquals(2,completedConstructorJournal.getStock());
		assertEquals(3,noIdConstructorJournal.getStock());
	}
	
	/* @Author:Joseph 
	 * Test Number: 9
	 * Test Objective: Test Journal getISSN method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test ISSN 1","test ISSN 2";
	 */
	public void testJournal009(){
		assertEquals("",emptyConstructorJournal.getISSN());
		assertEquals("test ISSN 1",completedConstructorJournal.getISSN());
		assertEquals("test ISSN 2",noIdConstructorJournal.getISSN());
	}
	
	/* @Author:Joseph 
	 * Test Number: 10
	 * Test Objective: Test Journal setISSN method works when object is initiated with each constructor type.
	 * Input(s): "test ISSN 1" , "test ISSN 2", "test ISSN 3"; 
	 * Expected Output ="test ISSN 1" , "test ISSN 2", "test ISSN 3";
	 */
	public void testJournal010(){
		emptyConstructorJournal.setISSN("test ISSN 1");
		completedConstructorJournal.setISSN("test ISSN 2");
		noIdConstructorJournal.setISSN("test ISSN 3");
		assertEquals("test ISSN 1",emptyConstructorJournal.getISSN());
		assertEquals("test ISSN 2",completedConstructorJournal.getISSN());
		assertEquals("test ISSN 3",noIdConstructorJournal.getISSN());
	}

	/* @Author:Joseph 
	 * Test Number: 11
	 * Test Objective: Test Journal getLibraryReferenceNumber method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test library reference 1","test library reference 2";
	 */
	public void testJournal011(){
		assertEquals("",emptyConstructorJournal.getLibraryReferenceNumber());
		assertEquals("test library reference 1",completedConstructorJournal.getLibraryReferenceNumber());
		assertEquals("test library reference 2",noIdConstructorJournal.getLibraryReferenceNumber());
	}
	
	/* @Author:Joseph 
	 * Test Number: 12
	 * Test Objective: Test Journal setLibraryReferenceNumber method works when object is initiated with each constructor type.
	 * Input(s): "test library reference 1" , "test library reference 2", "test library reference 3"; 
	 * Expected Output ="test library reference 1" , "test library reference 2", "test library reference 3"; 
	 */
	public void testJournal012(){
		emptyConstructorJournal.setLibraryReferenceNumber("test library reference 1");
		completedConstructorJournal.setLibraryReferenceNumber("test library reference 2");
		noIdConstructorJournal.setLibraryReferenceNumber("test library reference 3");
		assertEquals("test library reference 1",emptyConstructorJournal.getLibraryReferenceNumber());
		assertEquals("test library reference 2",completedConstructorJournal.getLibraryReferenceNumber());
		assertEquals("test library reference 3",noIdConstructorJournal.getLibraryReferenceNumber());
	}
	
	/* @Author:Joseph 
	 * Test Number: 13
	 * Test Objective: Test Journal getTitle method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test title 1","test title 2";
	 */
	public void testJournal013(){
		assertEquals("",emptyConstructorJournal.getTitle());
		assertEquals("test title 1",completedConstructorJournal.getTitle());
		assertEquals("test title 2",noIdConstructorJournal.getTitle());
	}
	
	/* @Author:Joseph 
	 * Test Number: 14
	 * Test Objective: Test Journal setTitle method works when object is initiated with each constructor type.
	 * Input(s): "test title 1" , "test title 2", "test title 3"; 
	 * Expected Output = "test title 1" , "test title 2", "test title 3"; 
	 */
	public void testJournal014(){
		emptyConstructorJournal.setTitle("test title 1");
		completedConstructorJournal.setTitle("test title 2");
		noIdConstructorJournal.setTitle( "test title 3");
		assertEquals("test title 1",emptyConstructorJournal.getTitle());
		assertEquals("test title 2",completedConstructorJournal.getTitle());
		assertEquals("test title 3",noIdConstructorJournal.getTitle());
	}
	
	/* @Author:Joseph 
	 * Test Number: 15
	 * Test Objective: Test Journal getAuthor method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test author 1","test author 2";
	 */
	public void testJournal015(){
		assertEquals("",emptyConstructorJournal.getAuthor());
		assertEquals("test author 1",completedConstructorJournal.getAuthor());
		assertEquals("test author 2",noIdConstructorJournal.getAuthor());
	}
	
	/* @Author:Joseph 
	 * Test Number: 16
	 * Test Objective: Test Journal setAuthor method works when object is initiated with each constructor type.
	 * Input(s): "test author 1" , "test author 2", "test author 3"; 
	 * Expected Output = "test author 1" , "test author 2", "test author 3"; 
	 */
	public void testJournal016(){
		emptyConstructorJournal.setAuthor("test author 1");
		completedConstructorJournal.setAuthor("test author 2");
		noIdConstructorJournal.setAuthor( "test author 3");
		assertEquals("test author 1",emptyConstructorJournal.getAuthor());
		assertEquals("test author 2",completedConstructorJournal.getAuthor());
		assertEquals("test author 3",noIdConstructorJournal.getAuthor());
	}
	
	
	/* @Author:Joseph 
	 * Test Number: 17
	 * Test Objective: Test Journal getDescription method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test description 1","test description 2";
	 */
	public void testJournal017(){
		assertEquals("",emptyConstructorJournal.getDescription());
		assertEquals("test description 1",completedConstructorJournal.getDescription());
		assertEquals("test description 2",noIdConstructorJournal.getDescription());
	}
	
	/* @Author:Joseph 
	 * Test Number: 18
	 * Test Objective: Test Journal setDescription method works when object is initiated with each constructor type.
	 * Input(s): "test description 1" , "test description 2", "test description 3"; 
	 * Expected Output = "test description 1" , "test description 2", "test description 3"; 
	 */
	public void testJournal018(){
		emptyConstructorJournal.setDescription("test description 1");
		completedConstructorJournal.setDescription("test description 2");
		noIdConstructorJournal.setDescription( "test description 3");
		assertEquals("test description 1",emptyConstructorJournal.getDescription());
		assertEquals("test description 2",completedConstructorJournal.getDescription());
		assertEquals("test description 3",noIdConstructorJournal.getDescription());
	}
	
	/* @Author:Joseph 
	 * Test Number: 45
	 * Test Objective: Test Journal getReleaseDate method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output = "test year 1" , "test year 2"; 
	 */
	public void testJournal019(){
		assertEquals("",emptyConstructorJournal.getReleaseDate());
		assertEquals("test year 1",completedConstructorJournal.getReleaseDate());
		assertEquals("test year 2",noIdConstructorJournal.getReleaseDate());
	}
	/* @Author:Joseph 
	 * Test Number: 46
	 * Test Objective: Test Journal setReleaseDate method works when object is initiated with each constructor type.
	 * Input(s): "test year 1" , "test year 2", "test year 3"; 
	 * Expected Output = "test year 1" , "test year 2", "test year 3"; 
	 */
	public void testJournal020(){
		emptyConstructorJournal.setReleaseDate("test year 1");
		completedConstructorJournal.setReleaseDate("test year 2");
		noIdConstructorJournal.setReleaseDate( "test year 3");
		assertEquals("test year 1",emptyConstructorJournal.getReleaseDate());
		assertEquals("test year 2",completedConstructorJournal.getReleaseDate());
		assertEquals("test year 3",noIdConstructorJournal.getReleaseDate());
	}
	
	
//	   ____   ____   ____  _  __  __  __  ____  _____  ______ _        _______ ______  _____ _______ _____ 
//	  |  _ \ / __ \ / __ \| |/ / |  \/  |/ __ \|  __ \|  ____| |      |__   __|  ____|/ ____|__   __/ ____|
//	  | |_) | |  | | |  | | ' /  | \  / | |  | | |  | | |__  | |         | |  | |__  | (___    | | | (___  
//	  |  _ <| |  | | |  | |  <   | |\/| | |  | | |  | |  __| | |         | |  |  __|  \___ \   | |  \___ \ 
//	  | |_) | |__| | |__| | . \  | |  | | |__| | |__| | |____| |____     | |  | |____ ____) |  | |  ____) |
//	  |____/ \____/ \____/|_|\_\ |_|  |_|\____/|_____/|______|______|    |_|  |______|_____/   |_| |_____/ 
	                                                                                                       
	/* @Author:Joseph 
	 * Test Number: 19
	 * Test Objective: Test Book getId method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output = 0,1,0
	 */                                                                                               
	
	public void testBook001() {
		assertEquals(0,emptyConstructorBook.getId());
		assertEquals(1,completedConstructorBook.getId());
		assertEquals(0,noIdConstructorBook.getId());
	}
	
	/* @Author:Joseph 
	 * Test Number: 20
	 * Test Objective: Test Book setId method works when object is initiated with each constructor type.
	 * Input(s): 1,2,3; 
	 * Expected Output = 1,2,3;
	 */
	public void testBook002() {
		emptyConstructorBook.setId(1);
		completedConstructorBook.setId(2);
		noIdConstructorBook.setId(3);
		assertEquals(1,emptyConstructorBook.getId());
		assertEquals(2,completedConstructorBook.getId());
		assertEquals(3,noIdConstructorBook.getId());
	}
	
	/* @Author:Joseph 
	 * Test Number: 21
	 * Test Objective: Test Book getGenres method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output = "","test genres", "test 2 genres";
	 */
	public void testBook003() {

		assertEquals("",emptyConstructorBook.getGenres());
		assertEquals("test genres",completedConstructorBook.getGenres());
		assertEquals("test 2 genres",noIdConstructorBook.getGenres());
	}
	
	/* @Author:Joseph 
	 * Test Number: 22
	 * Test Objective: Test Book setGenres method works when object is initiated with each constructor type.
	 * Input(s): "test genres 1","test genres 2","test genres 3"; 
	 * Expected Output = "test genres 1","test genres 2", "test genres 3";
	 */
	public void testBook004() {
		emptyConstructorBook.setGenres("test genres");
		completedConstructorBook.setGenres("test genres 2");
		noIdConstructorBook.setGenres("test genres 3");
		assertEquals("test genres",emptyConstructorBook.getGenres());
		assertEquals("test genres 2",completedConstructorBook.getGenres());
		assertEquals("test genres 3",noIdConstructorBook.getGenres());
	}
	
	/* @Author:Joseph 
	 * Test Number: 23
	 * Test Objective: Test Book getPublisher method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output = "","test publisher 1", "test publisher 2";
	 */
	public void testBook005(){
		assertEquals("",emptyConstructorBook.getPublisher());
		assertEquals("test publisher 1",completedConstructorBook.getPublisher());
		assertEquals("test publisher 2",noIdConstructorBook.getPublisher());
	}
	
	/* @Author:Joseph 
	 * Test Number: 24
	 * Test Objective: Test Book setPublisher method works when object is initiated with each constructor type.
	 * Input(s): "test publisher 1","test publisher 2","test publisher"; 
	 * Expected Output = "","test publisher 1", "test publisher 2";
	 */
	public void testBook006(){
		emptyConstructorBook.setPublisher("test publisher 1");
		completedConstructorBook.setPublisher("test publisher 2");
		noIdConstructorBook.setPublisher("test publisher 3");
		assertEquals("test publisher 1",emptyConstructorBook.getPublisher());
		assertEquals("test publisher 2",completedConstructorBook.getPublisher());
		assertEquals("test publisher 3",noIdConstructorBook.getPublisher());
	}
	
	/* @Author:Joseph 
	 * Test Number: 25
	 * Test Objective: Test Book getStock method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output =0,1,2;
	 */
	public void testBook007(){
		assertEquals(0,emptyConstructorBook.getStock());
		assertEquals(1,completedConstructorBook.getStock());
		assertEquals(2,noIdConstructorBook.getStock());
	}
	
	/* @Author:Joseph 
	 * Test Number: 26
	 * Test Objective: Test Book setStock method works when object is initiated with each constructor type.
	 * Input(s): 1,2,3; 
	 * Expected Output =1,2,3;
	 */
	public void testBook008(){
		emptyConstructorBook.setStock(1);
		completedConstructorBook.setStock(2);
		noIdConstructorBook.setStock(3);
		assertEquals(1,emptyConstructorBook.getStock());
		assertEquals(2,completedConstructorBook.getStock());
		assertEquals(3,noIdConstructorBook.getStock());
	}
	
	/* @Author:Joseph 
	 * Test Number: 27
	 * Test Objective: Test Book getISBN method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test ISBN 1","test ISBN 2";
	 */
	public void testBook009(){
		assertEquals("",emptyConstructorBook.getISBN());
		assertEquals("test ISBN 1",completedConstructorBook.getISBN());
		assertEquals("test ISBN 2",noIdConstructorBook.getISBN());
	}
	
	/* @Author:Joseph 
	 * Test Number: 28
	 * Test Objective: Test Book setISBN method works when object is initiated with each constructor type.
	 * Input(s): "test ISBN 1" , "test ISBN 2", "test ISBN 3"; 
	 * Expected Output ="test ISBN 1" , "test ISBN 2", "test ISBN 3";
	 */
	public void testBook010(){
		emptyConstructorBook.setISBN("test ISBN 1");
		completedConstructorBook.setISBN("test ISBN 2");
		noIdConstructorBook.setISBN("test ISBN 3");
		assertEquals("test ISBN 1",emptyConstructorBook.getISBN());
		assertEquals("test ISBN 2",completedConstructorBook.getISBN());
		assertEquals("test ISBN 3",noIdConstructorBook.getISBN());
	}

	/* @Author:Joseph 
	 * Test Number: 29
	 * Test Objective: Test Book getLibraryReferenceNumber method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test library reference 1","test library reference 2";
	 */
	public void testBook011(){
		assertEquals("",emptyConstructorBook.getLibraryReferenceNumber());
		assertEquals("test library reference 1",completedConstructorBook.getLibraryReferenceNumber());
		assertEquals("test library reference 2",noIdConstructorBook.getLibraryReferenceNumber());
	}
	
	/* @Author:Joseph 
	 * Test Number: 30
	 * Test Objective: Test Book setLibraryReferenceNumber method works when object is initiated with each constructor type.
	 * Input(s): "test library reference 1" , "test library reference 2", "test library reference 3"; 
	 * Expected Output ="test library reference 1" , "test library reference 2", "test library reference 3"; 
	 */
	public void testBook012(){
		emptyConstructorBook.setLibraryReferenceNumber("test library reference 1");
		completedConstructorBook.setLibraryReferenceNumber("test library reference 2");
		noIdConstructorBook.setLibraryReferenceNumber("test library reference 3");
		assertEquals("test library reference 1",emptyConstructorBook.getLibraryReferenceNumber());
		assertEquals("test library reference 2",completedConstructorBook.getLibraryReferenceNumber());
		assertEquals("test library reference 3",noIdConstructorBook.getLibraryReferenceNumber());
	}
	
	/* @Author:Joseph 
	 * Test Number: 31
	 * Test Objective: Test Book getTitle method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test title 1","test title 2";
	 */
	public void testBook013(){
		assertEquals("",emptyConstructorBook.getTitle());
		assertEquals("test title 1",completedConstructorBook.getTitle());
		assertEquals("test title 2",noIdConstructorBook.getTitle());
	}
	
	/* @Author:Joseph 
	 * Test Number: 32
	 * Test Objective: Test Book setTitle method works when object is initiated with each constructor type.
	 * Input(s): "test title 1" , "test title 2", "test title 3"; 
	 * Expected Output = "test title 1" , "test title 2", "test title 3"; 
	 */
	public void testBook014(){
		emptyConstructorBook.setTitle("test title 1");
		completedConstructorBook.setTitle("test title 2");
		noIdConstructorBook.setTitle( "test title 3");
		assertEquals("test title 1",emptyConstructorBook.getTitle());
		assertEquals("test title 2",completedConstructorBook.getTitle());
		assertEquals("test title 3",noIdConstructorBook.getTitle());
	}
	
	/* @Author:Joseph 
	 * Test Number: 33
	 * Test Objective: Test Book getAuthor method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test author 1","test author 2";
	 */
	public void testBook015(){
		assertEquals("",emptyConstructorBook.getAuthor());
		assertEquals("test author 1",completedConstructorBook.getAuthor());
		assertEquals("test author 2",noIdConstructorBook.getAuthor());
	}
	
	/* @Author:Joseph 
	 * Test Number: 34
	 * Test Objective: Test Book setAuthor method works when object is initiated with each constructor type.
	 * Input(s): "test author 1" , "test author 2", "test author 3"; 
	 * Expected Output = "test author 1" , "test author 2", "test author 3"; 
	 */
	public void testBook016(){
		emptyConstructorBook.setAuthor("test author 1");
		completedConstructorBook.setAuthor("test author 2");
		noIdConstructorBook.setAuthor( "test author 3");
		assertEquals("test author 1",emptyConstructorBook.getAuthor());
		assertEquals("test author 2",completedConstructorBook.getAuthor());
		assertEquals("test author 3",noIdConstructorBook.getAuthor());
	}
	
	
	/* @Author:Joseph 
	 * Test Number: 35
	 * Test Objective: Test Book getDescription method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output ="","test description 1","test description 2";
	 */
	public void testBook017(){
		assertEquals("",emptyConstructorBook.getDescription());
		assertEquals("test description 1",completedConstructorBook.getDescription());
		assertEquals("test description 2",noIdConstructorBook.getDescription());
	}
	
	/* @Author:Joseph 
	 * Test Number: 36
	 * Test Objective: Test Book setDescription method works when object is initiated with each constructor type.
	 * Input(s): "test description 1" , "test description 2", "test description 3"; 
	 * Expected Output = "test description 1" , "test description 2", "test description 3"; 
	 */
	public void testBook018(){
		emptyConstructorBook.setDescription("test description 1");
		completedConstructorBook.setDescription("test description 2");
		noIdConstructorBook.setDescription( "test description 3");
		assertEquals("test description 1",emptyConstructorBook.getDescription());
		assertEquals("test description 2",completedConstructorBook.getDescription());
		assertEquals("test description 3",noIdConstructorBook.getDescription());
	}
	
	/* @Author:Joseph 
	 * Test Number: 47
	 * Test Objective: Test Book getReleaseDate method works when object is initiated with each constructor type.
	 * Input(s): none; 
	 * Expected Output = "test year 1" , "test year 2"; 
	 */
	public void testBook019(){
		assertEquals("",emptyConstructorBook.getReleaseDate());
		assertEquals("test year 1",completedConstructorBook.getReleaseDate());
		assertEquals("test year 2",noIdConstructorBook.getReleaseDate());
	}
	/* @Author:Joseph 
	 * Test Number: 48
	 * Test Objective: Test Book setReleaseDate method works when object is initiated with each constructor type.
	 * Input(s): "test year 1" , "test year 2", "test year 3"; 
	 * Expected Output = "test year 1" , "test year 2", "test year 3"; 
	 */
	public void testBook020(){
		emptyConstructorBook.setReleaseDate("test year 1");
		completedConstructorBook.setReleaseDate("test year 2");
		noIdConstructorBook.setReleaseDate( "test year 3");
		assertEquals("test year 1",emptyConstructorBook.getReleaseDate());
		assertEquals("test year 2",completedConstructorBook.getReleaseDate());
		assertEquals("test year 3",noIdConstructorBook.getReleaseDate());
	}
	
	
//	   _____ _____  _____ _____  _           __     ____  __  ____  _____  ______ _             _____ _   _ _______ ______ _____  ______      _____ ______       _______ ______  _____ _______ _____ 
//	  |  __ \_   _|/ ____|  __ \| |        /\\ \   / /  \/  |/ __ \|  __ \|  ____| |           |_   _| \ | |__   __|  ____|  __ \|  ____/\   / ____|  ____|     |__   __|  ____|/ ____|__   __/ ____|
//	  | |  | || | | (___ | |__) | |       /  \\ \_/ /| \  / | |  | | |  | | |__  | |             | | |  \| |  | |  | |__  | |__) | |__ /  \ | |    | |__           | |  | |__  | (___    | | | (___  
//	  | |  | || |  \___ \|  ___/| |      / /\ \\   / | |\/| | |  | | |  | |  __| | |             | | | . ` |  | |  |  __| |  _  /|  __/ /\ \| |    |  __|          | |  |  __|  \___ \   | |  \___ \ 
//	  | |__| || |_ ____) | |    | |____ / ____ \| |  | |  | | |__| | |__| | |____| |____        _| |_| |\  |  | |  | |____| | \ \| | / ____ \ |____| |____         | |  | |____ ____) |  | |  ____) |
//	  |_____/_____|_____/|_|    |______/_/    \_\_|  |_|  |_|\____/|_____/|______|______|      |_____|_| \_|  |_|  |______|_|  \_\_|/_/    \_\_____|______|        |_|  |______|_____/   |_| |_____/ 
	                                                                                                                                                                                                 
	                                                                                                                                                                                                 
	/* @Author:Joseph 
	 * Test Number: 37
	 * Test Objective: Test DisplayModel getItemTitle method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = "test title 1" , "test title 2"; 
	 */
	public void testDisplayModel001(){
		assertEquals("test title 1", journalTest.getItemTitle());
		assertEquals("test title 2", bookTest.getItemTitle());
	}
	
	/* @Author:Joseph 
	 * Test Number: 38
	 * Test Objective: Test DisplayModel getItemAuthor method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = "test author 1" , "test author 2"; 
	 */
	public void testDisplayModel002() {
		assertEquals("test author 1", journalTest.getItemAuthor());
		assertEquals("test author 2", bookTest.getItemAuthor());
	}
	
	/* @Author:Joseph 
	 * Test Number: 39
	 * Test Objective: Test DisplayModel getItemDescription method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = "test description 1" , "test description 2"; 
	 */
	public void testDisplayModel003() {
		assertEquals("test description 1", journalTest.getItemDescription());
		assertEquals("test description 2", bookTest.getItemDescription());
	}
	
	/* @Author:Joseph 
	 * Test Number: 40
	 * Test Objective: Test DisplayModel getCurrentStock method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = 1,2; 
	 */
	public void testDisplayModel004() {
		assertEquals("1", journalTest.getCurrentStock());
		assertEquals("2", bookTest.getCurrentStock());
	}
	/* @Author:Joseph 
	 * Test Number: 41
	 * Test Objective: Test DisplayModel getPublisherIdentifier method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = "test ISSN 1" , "test ISBN 2"; 
	 */
	public void testDisplayModel005() {
		assertEquals("test ISSN 1", journalTest.getPublisherIdentifier());
		assertEquals("test ISBN 2", bookTest.getPublisherIdentifier());
	}
	
	/* @Author:Joseph 
	 * Test Number: 41
	 * Test Objective: Test DisplayModel getLibraryIdentifier method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = "test library reference  1" , "test library reference 2"; 
	 */
	public void testDisplayModel006() {
		assertEquals("test library reference 1", journalTest.getLibraryIdentifier());
		assertEquals("test library reference 2", bookTest.getLibraryIdentifier());
	}
	/* @Author:Joseph 
	 * Test Number: 42
	 * Test Objective: Test DisplayModel getItemPublisher method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = "test publisher 1" , "test publisher 2"; 
	 */
	public void testDisplayModel007() {
		assertEquals("test publisher 1", journalTest.getItemPublisher());
		assertEquals("test publisher 2", bookTest.getItemPublisher());
	}
	
	/* @Author:Joseph 
	 * Test Number: 43
	 * Test Objective: Test DisplayModel getItemGenre method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = "test genre 1" , "test genre 2"; 
	 */
	public void testDisplayModel008() {
		assertEquals("test genre 1", journalTest.getItemGenre());
		assertEquals("test genre 2", bookTest.getItemGenre());
	}
	
	/* @Author:Joseph 
	 * Test Number: 49
	 * Test Objective: Test DisplayModel getReleaseDate method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = "test year 1" , "test year 2"; 
	 */
	public void testDisplayModel009() {
		assertEquals("test year 1", journalTest.getItemReleaseDate());
		assertEquals("test year 2", bookTest.getItemReleaseDate());
	}
	/* @Author:Joseph 
	 * Test Number: 50
	 * Test Objective: Test DisplayModel getItemId method works when object is initiated as book and as journal
	 * Input(s):  none;
	 * Expected Output = 1,2;
	 */
	public void testDisplayModel010() {
		assertEquals(2, journalTest.getItemId());
		assertEquals(1, bookTest.getItemId());
	}
	
	
	
	
}
