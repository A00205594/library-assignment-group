package Tests;

import DatabaseAccess.BookDAO;
import DatabaseAccess.JournalDAO;
import junit.framework.TestCase;

public class RemoveJournalTests extends TestCase {
	
	
//********************************************************************************************
//
//							Remove Journal Tests
//
	
	
		//	Test No: 1                                                       
		// 	Object: Check if Journal can be Removed from the table if reference number entered                 
		// 	Inputs: Reference_Number = E98765                                                     
		// 	Expected Output: True                                                    
		//  Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testRemoveJournal001() {
			try {
				assertTrue(JournalDAO.instance.Remove_Journal3("E98765"));
			} catch (Exception E) {
			}
		}
		
		
		//	Test No: 2                                                      
		// 	Object: Check if Journal can be Removed from the table if ISSN number entered                 
		// 	Inputs: ISSN = 978X1234                                                     
		// 	Expected Output: True                                                    
		//  Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testRemoveJournal002() {
			try {
				assertTrue(JournalDAO.instance.Remove_Journal2("978X1234"));
			} catch (Exception E) {
			}
		}
		 
		
		//	Test No: 3                                                     
		// 	Object: Check if Journal can be Removed from the table if a Title is entered                 
		// 	Inputs: ISBN = White Wolf                                                     
		// 	Expected Output: True                                                    
		//  Author: Patrick McLoughlin                                                                                                                                  
		// ****************************************************************************
		public void testRemoveBook003() {
			try {
				assertTrue(JournalDAO.instance.Remove_Journal("A Day and a Night"));
			} catch (Exception E) {
			}
		}
//End of Remove Book Tests*****************************************************************

	}
