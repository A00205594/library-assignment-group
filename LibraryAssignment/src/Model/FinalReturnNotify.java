package Model;

public class FinalReturnNotify implements DisplayModel {
	
		private int id,bookid,journalid,timeoverdue;
		private String firstname,lastname,email,booktitle,journaltitle;
		private float amountdue;
		
		public FinalReturnNotify() {
			id = 0;
			bookid=0;
			journalid=0;
			timeoverdue=0;
			firstname="";
			lastname="";
			email="";
			booktitle="";
			journaltitle="";
			amountdue=0;
		}
		
		

		public FinalReturnNotify(int id, int bookid, int journalid, int timeoverdue, String firstname, String lastname,
				String email, String booktitle, String journaltitle, float amountdue) {
			super();
			this.id = id;
			this.bookid = bookid;
			this.journalid = journalid;
			this.timeoverdue = timeoverdue;
			this.firstname = firstname;
			this.lastname = lastname;
			this.email = email;
			this.booktitle = booktitle;
			this.journaltitle = journaltitle;
			this.amountdue = amountdue;
		}



		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getBookid() {
			return bookid;
		}

		public void setBookid(int bookid) {
			this.bookid = bookid;
		}

		public int getJournalid() {
			return journalid;
		}

		public void setJournalid(int journalid) {
			this.journalid = journalid;
		}

		public int getTimeoverdue() {
			return timeoverdue;
		}

		public void setTimeoverdue(int timeoverdue) {
			this.timeoverdue = timeoverdue;
		}

		public String getFirstname() {
			return firstname;
		}

		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}

		public String getLastname() {
			return lastname;
		}

		public void setLastname(String lastname) {
			this.lastname = lastname;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getBooktitle() {
			return booktitle;
		}

		public void setBooktitle(String booktitle) {
			this.booktitle = booktitle;
		}

		public String getJournaltitle() {
			return journaltitle;
		}

		public void setJournaltitle(String journaltitle) {
			this.journaltitle = journaltitle;
		}

		public float getAmountdue() {
			return amountdue;
		}

		public void setAmountdue(float amountdue) {
			this.amountdue = amountdue;
		}


//////////////////////////////////////////////////////////////////////////////////////
		
//////////////////////////////////////////////////////////////////////////////////////
		@Override
		public String getItemTitle() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public String getItemAuthor() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public String getItemDescription() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public String getCurrentStock() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public String getPublisherIdentifier() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public String getLibraryIdentifier() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public String getItemPublisher() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public String getItemGenre() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public String getItemReleaseDate() {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public int getItemId() {
			// TODO Auto-generated method stub
			return 0;
		}

		
		
	}


