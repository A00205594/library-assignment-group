package Model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class JournalCart implements GenericJournalInterface 
{
	public List<Journal> JournalCart;
	
	public JournalCart() 
	{
		JournalCart = new ArrayList<Journal>();
	}
	
	//method to add journals to cart
	@Override
	public boolean addToCart(DisplayModel displayModel)
	{
		Journal journalToBeAddedToCart = (Journal) displayModel;
		boolean journalEntryDone = false;
		
		if(JournalCart.size() < maxItemsAllowedInCart)
		{

			//HANDLE DUPLICATE JOURNALS
			
			//if(!JournalCart.isEmpty())
			//{
			//	for (Journal journalCartEntry : JournalCart) 
			//	{
			//		if(journalCartEntry.getLibraryIdentifier().equalsIgnoreCase(journalToBeAddedToCart.getLibraryIdentifier()))
			//		{
			//			break;
			//		}
			//		else
			//		{
			//			JournalCart.add(journalToBeAddedToCart);						
			//			bookEntryDone = true;
			//		}
			//	}
			//}
			//else
			{
				JournalCart.add(journalToBeAddedToCart);
				journalEntryDone = true;
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Cart is already full (4 journals added)");
		}
		return journalEntryDone;
	}
	
	public boolean removeJournalFromCartByID(int JournalID)
	{
		boolean journalRemoved = false;
		
		//Journal journalToBeRemoved;
		//try 
		//{
			//journalToBeRemoved = JournalDAO.instance.GetRequestedJournal(JournalID);
			
			//if(JournalCart.contains(journalToBeRemoved))
			//{
			if(JournalCart.size() >= JournalID)
			{
				JournalCart.remove(JournalID -1);
				//JournalCart.remove(journalToBeRemoved);
				journalRemoved = true;
			}
			//}
		//}
		//catch (OliverExceptionHandler e)
		//{
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
				
		return journalRemoved;
	}
}
