package Model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import Tests.OliverExceptionHandler;
import DatabaseAccess.BookDAO;

public class BookCart implements GenericCartInterface 
{
	public List<Book> BookCart;
	
	public BookCart() 
	{
		BookCart = new ArrayList<Book>();
	}
	
	//method to add books to cart
	@Override
	public boolean addToCart(DisplayModel displayModel)
	{
		Book bookToBeAddedToCart = (Book) displayModel;
		boolean bookEntryDone = false;
		
		if(BookCart.size() < maxItemsAllowedInCart)
		{

			//HANDLE DUPLICATE BOOKS
			
			//if(!BookCart.isEmpty())
			//{
			//	for (Book bookCartEntry : BookCart) 
			//	{
			//		if(bookCartEntry.getLibraryIdentifier().equalsIgnoreCase(bookToBeAddedToCart.getLibraryIdentifier()))
			//		{
			//			break;
			//		}
			//		else
			//		{
			//			BookCart.add(bookToBeAddedToCart);						
			//			bookEntryDone = true;
			//		}
			//	}
			//}
			//else
			{
				BookCart.add(bookToBeAddedToCart);
				bookEntryDone = true;
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Cart is already full (5 books added)");
		}
		return bookEntryDone;
	}
	
	public boolean removeBookFromCartByID(int BookID)
	{
		boolean bookRemoved = false;
		
		//Book bookToBeRemoved;
		//try 
		//{
			//bookToBeRemoved = BookDAO.instance.GetRequestedBook(BookID);
			
			//if(BookCart.contains(bookToBeRemoved))
			//{
			if(BookCart.size() >= BookID)
			{
				BookCart.remove(BookID -1);
				//BookCart.remove(bookToBeRemoved);
				bookRemoved = true;
			}
			//}
		//}
		//catch (OliverExceptionHandler e)
		//{
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
				
		return bookRemoved;
	}
}
