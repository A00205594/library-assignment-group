package Model;

public class Journal implements DisplayModel {

	private int id;
	private String title, author, description;
	private int stock;
	private String ISSN, libraryReferenceNumber;
	private String publisher;
	private String genres;
	private String releaseDate;
	public Journal() {
		id = 0;
		title = "";
		author = "";
		description = "";
		stock = 0;
		ISSN = "";
		libraryReferenceNumber = "";
		publisher = "";
		genres = "";
		releaseDate = "";
	}

	public Journal(int id, String title, String author, String description, int stock, String ISSN,
			String libraryReferenceNumber, String publisher, String genres,String releaseDate) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.description = description;
		this.stock = stock;
		this.ISSN = ISSN;
		this.libraryReferenceNumber = libraryReferenceNumber;
		this.publisher = publisher;
		this.genres = genres;
		this.releaseDate=releaseDate;
	}

	public Journal(String title, String author, String description, int stock, String ISSN,
			String libraryReferenceNumber, String publisher, String genres,String releaseDate) {
		this.id = 0;
		this.title = title;
		this.author = author;
		this.description = description;
		this.stock = stock;
		this.ISSN = ISSN;
		this.libraryReferenceNumber = libraryReferenceNumber;
		this.publisher = publisher;
		this.genres = genres;
		this.releaseDate=releaseDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getGenres() {
		return genres;
	}

	public void setGenres(String genres) {
		this.genres = genres;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getISSN() {
		return ISSN;
	}

	public void setISSN(String ISSN) {
		this.ISSN = ISSN;
	}

	public String getLibraryReferenceNumber() {
		return libraryReferenceNumber;
	}

	public void setLibraryReferenceNumber(String libraryReferenceNumber) {
		this.libraryReferenceNumber = libraryReferenceNumber;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String getItemTitle() {

		return getTitle();
	}

	@Override
	public String getItemAuthor() {

		return getAuthor();
	}

	@Override
	public String getItemDescription() {

		return getDescription();
	}

	@Override
	public String getCurrentStock() {

		return getStock() + "";
	}

	@Override
	public String getPublisherIdentifier() {

		return getISSN();
	}

	@Override
	public String getLibraryIdentifier() {

		return getLibraryReferenceNumber();
	}

	@Override
	public String getItemPublisher() {
		return getPublisher();
	}

	@Override
	public String getItemGenre() {
		return getGenres();
	}
	@Override
	public String getItemReleaseDate() {
		return getReleaseDate();
	}

	@Override
	public int getItemId() {
		return getId();
	}
}
