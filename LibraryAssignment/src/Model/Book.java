package Model;

public class Book implements DisplayModel {
	private int id;
	private String title, author, description;
	private int stock;
	private String ISBN, libraryReferenceNumber;
	private String publisher;
	private String genres;
	private String releaseDate;

	public Book() {
		id = 0;
		title = "";
		author = "";
		description = "";
		stock = 0;
		ISBN = "";
		libraryReferenceNumber = "";
		publisher = "";
		genres = "";
		releaseDate = "";

	}

	public Book(int id, String title, String author, String description, int stock, String ISBN,
			String libraryReferenceNumber, String publisher, String genres,String releaseDate) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.description = description;
		this.stock = stock;
		this.ISBN = ISBN;
		this.libraryReferenceNumber = libraryReferenceNumber;
		this.publisher = publisher;
		this.genres = genres;
		this.releaseDate=releaseDate;
	}

	public Book(String title, String author, String description, int stock, String ISBN, String libraryReferenceNumber,
			String publisher, String genres,String releaseDate) {
		this.title = title;
		this.author = author;
		this.description = description;
		this.stock = stock;
		this.ISBN = ISBN;
		this.libraryReferenceNumber = libraryReferenceNumber;
		this.publisher = publisher;
		this.genres = genres;
		this.releaseDate=releaseDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getGenres() {
		return genres;
	}

	public void setGenres(String genres) {
		this.genres = genres;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}

	public String getLibraryReferenceNumber() {
		return libraryReferenceNumber;
	}

	public void setLibraryReferenceNumber(String libraryReferenceNumber) {
		this.libraryReferenceNumber = libraryReferenceNumber;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String getItemTitle() {
		return getTitle();
	}

	@Override
	public String getItemAuthor() {
		return getAuthor();
	}

	@Override
	public String getItemDescription() {
		return getDescription();
	}

	@Override
	public String getCurrentStock() {
		return getStock() + "";
	}

	@Override
	public String getPublisherIdentifier() {
		return getISBN();
	}

	@Override
	public String getLibraryIdentifier() {
		return getLibraryReferenceNumber();
	}

	@Override
	public String getItemPublisher() {
		return getPublisher();
	}

	@Override
	public String getItemGenre() {
		return getGenres();
	}
	@Override
	public String getItemReleaseDate() {
		return getReleaseDate();
	}

	@Override
	public int getItemId() {
		return getId();
	}
}