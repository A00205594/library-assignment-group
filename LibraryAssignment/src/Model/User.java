package Model;

public class User {

	private String fName;
	private String LName;
	private String email;
	private String PhoneNo;
	private String studentNum;
	private String password;
		
	public User(String fName, String lName, String email, String phoneNo, String studentNum, String password) {
		super();
		this.fName = fName;
		LName = lName;
		this.email = email;
		PhoneNo = phoneNo;
		this.studentNum = studentNum;
		this.password = password;
	}
	
	
	public User(String fName, String lName, String email, String phoneNo, String password) {
		super();
		this.fName = fName;
		LName = lName;
		this.email = email;
		PhoneNo = phoneNo;
		this.password = password;
	}
	

	public User(){
		
	}
	
	public User(String fName, String lName, String email) {
		super();
		this.fName = fName;
		LName = lName;
		this.email = email;
	}
	

	public boolean CheckUserType(){
		return false;
	}

	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getLName() {
		return LName;
	}
	public void setLName(String lName) {
		LName = lName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return PhoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		PhoneNo = phoneNo;
	}
	public String getStudentNum() {
		return studentNum;
	}
	public void setStudentNum(String studentNum) {
		this.studentNum = studentNum;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
}

