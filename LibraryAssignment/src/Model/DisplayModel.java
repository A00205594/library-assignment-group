package Model;

public interface DisplayModel {
	public int getItemId();
	public String getItemTitle();
	public String getItemAuthor();
	public String getItemDescription();
	public String getCurrentStock();
	public String getPublisherIdentifier();
	public String getLibraryIdentifier();
	public String getItemPublisher();
	public String getItemGenre();
	public String getItemReleaseDate();

}
