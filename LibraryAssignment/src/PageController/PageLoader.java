package PageController;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import Model.User;
import View.AddFines;
import View.AddJournalAddBookPage;
import View.BookRequestPage_chandna;
import View.DisplayPage;
import View.HomePage;
import View.LoginPage;
import View.RegisterPage;
import View.RequestBookPage;
import View.RequestJournalPage;
import View.SearchPage;
import View.SendFinalEmailPage;
import View.UpdateRemove_JournalBooks;
import View.UserUpdateDetails;
import View.ViewBookRequestPage;
import View.ViewJournalRequests;
import View.sendOrderNotifacationPage;
import View.DueDate;

/*
 * Created By Joseph O'Leary
 * 25/1/2017
 * 
 */
public class PageLoader extends JFrame { 

	public User user = null;
	public static boolean userAdmin = false;
	
	private static final long serialVersionUID = 1L;

	private JPanel mainPanel;
	
	private CardLayout pageSwapper;
	
	private JPanel loginPage;
	private JPanel registerPage;
	private JPanel searchPage;
	private JPanel SendFinalEmailPage;
	public RequestBookPage requestBookpage;
	public RequestJournalPage requestJournalpage;
	private JPanel viewJournalrequest;
	private JPanel viewBookrequest;
	public HomePage homePage;
	private JPanel UserUpdate;
	private JPanel editPage;
	private JPanel addJournal;
	private JPanel addFines;
	private JPanel dueDate;
	private sendOrderNotifacationPage sendordernotifacationpage;
	

	private BookRequestPage_chandna bookRequestPage_chandna;
	
	public DisplayPage displayItemPage;

	public final String LOGIN_PAGE = "A";
	public final String REGISTER_PAGE = "B";
	public final String SEARCH_PAGE = "C";
	public final String SEND_FINAL_EMAIL_PAGE = "D";
	public final String HOME_PAGE = "E";
	public final String UPDATE_REMOVE_JOURNAL = "UPRAJ";
	public final String REQUEST_BOOK_PAGE = "F";
	public final String REQUEST_JOURNAL_PAGE = "G";
	public final String VIEW_JOURNAL_REQUEST_PAGE = "H";
	public final String VIEW_BOOK_REQUEST_PAGE = "I";
	public final String DISPLAY_ITEM_PAGE = "J";
	public final String ADD_JOURNAL = "AJ";
	public final String BOOK_REQUEST_CHANDNA = "BC";
	public final String USER_UPDATE_DETAILS = "UU";
	public final String ADD_FINES = "AF";
	public final String DUE_DATE = "DD";
	public final String SEND_ORDER_NOTIFACATION_PAGE = "SO";

	public final Color DEFAULT_BACKGROUND_COLOUR = Color.LIGHT_GRAY;
	
	public User currentUser;
	
	public PageLoader() {
		super("Agile - Assignment");
		pageSwapper = new CardLayout();
		mainPanel = new JPanel(pageSwapper);
		loginPage = new LoginPage(this);
		registerPage = new RegisterPage(this);
		searchPage = new SearchPage(this);
		SendFinalEmailPage = new SendFinalEmailPage(this);
		displayItemPage = new DisplayPage(this);

		requestBookpage = new RequestBookPage(this);
		requestJournalpage = new RequestJournalPage(this);
		viewJournalrequest = new ViewJournalRequests(this);
		viewBookrequest = new ViewBookRequestPage(this);
		homePage = new HomePage(this);
		sendordernotifacationpage = new sendOrderNotifacationPage(this);

		addJournal = new AddJournalAddBookPage(this);
		dueDate = new DueDate(this);
		addFines = new AddFines(this);
		UserUpdate = new UserUpdateDetails(this);
		editPage = new UpdateRemove_JournalBooks(this);
		
		bookRequestPage_chandna = new BookRequestPage_chandna(this);

		mainPanel.add(loginPage, LOGIN_PAGE);
		mainPanel.add(registerPage, REGISTER_PAGE);
		mainPanel.add(searchPage, SEARCH_PAGE);
		mainPanel.add(SendFinalEmailPage, SEND_FINAL_EMAIL_PAGE);
		mainPanel.add(displayItemPage, DISPLAY_ITEM_PAGE);
		mainPanel.add(requestBookpage, REQUEST_BOOK_PAGE);
		mainPanel.add(requestJournalpage, REQUEST_JOURNAL_PAGE);
		mainPanel.add(viewJournalrequest, VIEW_JOURNAL_REQUEST_PAGE);
		mainPanel.add(viewBookrequest, VIEW_BOOK_REQUEST_PAGE);
		mainPanel.add(homePage, HOME_PAGE);
		mainPanel.add(UserUpdate, USER_UPDATE_DETAILS);
		mainPanel.add(editPage, UPDATE_REMOVE_JOURNAL);
		mainPanel.add(addJournal, ADD_JOURNAL);
		mainPanel.add(dueDate, DUE_DATE);
		mainPanel.add(addFines, ADD_FINES);
		mainPanel.add(bookRequestPage_chandna, BOOK_REQUEST_CHANDNA);
		mainPanel.add(sendordernotifacationpage, SEND_ORDER_NOTIFACATION_PAGE);

		// Going to start with the Welcome Page 
		showPage(LOGIN_PAGE);
		//showPage(DUE_DATE);
		 


		this.setContentPane(mainPanel);
		this.setSize(1280, 720);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE); 
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public void resizeWindow(int width, int height) {
		this.setSize(new Dimension(width, height));
	}

	public void showPage(String key) {
		pageSwapper.show(mainPanel, key);
	}

	public static void main(String[] args) {
		new PageLoader();

	}
	
	
}